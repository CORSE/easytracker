# pylint: disable=all
from sys import argv

import IPython

from easytracker.init_tracker import init_tracker

tracker = init_tracker("GDB")
print(tracker.load_program(argv[1]))
print(tracker.start())

go = True
while go:
    command = input()
    if command != "q":
        print(tracker.send_direct_command(command))
    else:
        go = False
tracker.terminate()

IPython.embed()
