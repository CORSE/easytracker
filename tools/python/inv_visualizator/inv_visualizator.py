#!/usr/bin/env python3

"""A visualization tool to illustrate the notion of invariant when looping over arrays"""

import argparse
from subprocess import run

from visualprimitives.source_image import generate_source_image
from visualprimitives.dot_utils import (
    dot_file,
    dot_table,
)
import easytracker

LIVE_SRC_IMG_NAME = "source"
LIVE_ARRAY_IMG_NAME = "array.svg"

PROCESSED_BACKGROUND = "chartreuse"
TO_BE_PROCESSED_BACKGROUND = "lightgray"

PORT_BASE = "cell"


def get_arguments():
    """Get the command line arguments."""
    parser = argparse.ArgumentParser(
        description="tool to visualize invariants when looping over arrays"
    )
    # Positional arguments
    parser.add_argument("prog_name", type=str, help="the program to visualize")
    parser.add_argument(
        "func_name", type=str, help="the function containing the loop to visualize"
    )
    parser.add_argument(
        "array_name",
        type=str,
        help="the name of the array iterated inside the loop",
    )
    # Optional arguments
    parser.add_argument(
        "-l",
        "--live",
        default=False,
        action="store_true",
        help=(
            "if true, execution is controlled by striking enter "
            "and a singe source image and a single array image are "
            "overwriten at each step for live visualisation."
            "if false, one dedicatde source image and one dedicated array image "
            "are generated after the execution of each line"
        ),
    )
    parser.add_argument(
        "--show",
        action="store_true",
        help="woks as for the live option, but display the generated image with eog",
    )
    parser.add_argument(
        "-li",
        "--leftidxname",
        dest="left_idx_name",
        help='the name of the index variable being the "left" invariant',
        type=str,
        default="",
    )
    parser.add_argument(
        "-ri",
        "--rightidxname",
        help='the name of the index variable being the "right" invariant',
        dest="right_idx_name",
        type=str,
        default="",
    )
    parser.add_argument(
        "--show-last",
        action="store_true",
        help="show last lineno in addition to next lineno",
    )

    return parser.parse_args()


def draw_array(array_name, array, left_idx_val, right_idx_val, dot_buf):
    """Draws the array in the given dot buffer"""
    with dot_table(
        dot_name="array", parent=dot_buf, border=0, cellspacing=0, port=array_name
    ) as array_buf:
        array_buf.add_single_data_line(
            content=array_name,
            colspan=len(array),
            align="center",
            border=0,
        )
        with array_buf.add_line() as line:
            for idx, array_elem in enumerate(array):
                background_color = (
                    PROCESSED_BACKGROUND
                    if (left_idx_val is not None and idx < left_idx_val)
                    or (right_idx_val is not None and idx > right_idx_val)
                    else TO_BE_PROCESSED_BACKGROUND
                )
                line.add_inline_data(
                    content=str(array_elem),
                    border=1,
                    bgcolor=background_color,
                    port=f"{PORT_BASE}{idx}",
                )


# pylint: disable=too-many-arguments
def draw_invs(
    array_name, left_idx_name, right_idx_name, left_idx_val, right_idx_val, dot_buf
):
    """Draws the array in the given dot buffer"""
    if left_idx_val is not None:
        dot_buf.write(f"{left_idx_name};")
        dot_buf.write(
            f"{array_name}:{PORT_BASE}{left_idx_val}->{left_idx_name}[dir=back];"
        )
    if right_idx_val is not None:
        dot_buf.write(f"{right_idx_name};")
        dot_buf.write(
            f"{array_name}:{PORT_BASE}{right_idx_val}->{right_idx_name}[dir=back];"
        )


def draw_array_with_invs(
    left_idx_name: str,
    right_idx_name: str,
    array_name: str,
    frame,
    array_image_name: str,
):
    """Draws the array and the invariant in the given image name"""
    array = frame.variables[array_name].value
    left_idx_val = (
        None
        if not left_idx_name or not left_idx_name in frame.variables
        else frame.variables[left_idx_name].value
    )
    right_idx_val = (
        None
        if not right_idx_name or not right_idx_name in frame.variables
        else frame.variables[right_idx_name].value
    )
    with dot_file(image_name=array_image_name) as dot_buf:
        draw_array(array_name, array, left_idx_val, right_idx_val, dot_buf)
        draw_invs(
            array_name,
            left_idx_name,
            right_idx_name,
            left_idx_val,
            right_idx_val,
            dot_buf,
        )


def execute_function_of_interest(args, tracker: easytracker.interface.EasyTrackerItf):
    """Executes the function of interest with visualization"""
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    image_count = 0
    while frame and frame.name == args.func_name:
        array_image_name, source_image_name = (
            (LIVE_ARRAY_IMG_NAME, LIVE_SRC_IMG_NAME)
            if args.live or args.show
            else (
                LIVE_ARRAY_IMG_NAME.replace(".", f"{image_count:02d}."),
                f"{LIVE_SRC_IMG_NAME}{image_count:02d}",
            )
        )
        draw_array_with_invs(
            args.left_idx_name,
            args.right_idx_name,
            args.array_name,
            frame,
            array_image_name=array_image_name,
        )
        generate_source_image(
            filename=args.prog_name,
            image_name=source_image_name,
            next_lineno=tracker.next_lineno,
            last_lineno=tracker.last_lineno if args.show_last else 0,
        )
        if args.show:
            run(f"eog {array_image_name} &", shell=True, check=True)
        elif args.live:
            input("Press enter to continue")
        else:
            image_count += 1
        tracker.next()
        frame = tracker.get_current_frame(as_raw_python_objects=True)


def main():
    """The tool's entry point"""
    args = get_arguments()

    # Init the tracker and run into the function of interest
    tracker = easytracker.init_tracker("python")
    tracker.load_program(args.prog_name)
    tracker.start()
    tracker.track_function(args.func_name)
    tracker.resume()
    tracker.next()

    # Visualize the function of interest
    execute_function_of_interest(args, tracker)


main()
