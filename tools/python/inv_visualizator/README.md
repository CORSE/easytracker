Visualize loop invariant
========================

Generates an image with an array and some invariants and the associated source code image.

In the given example, the invariants are:
- the `array` elements with index lower than `i` are all `0`,
- the `array` elements with index greater than `j` ara all `1`.

Run and generate all images for all steps with:

    ./inv_visualizator.py -li i -ri j inferiors/sort_array_of_binaries.py sort array

This will generate `sourcexx.jpg` and `arrayxx.jpg` file for each step.

Optionally display live the invariants update with the `eog` tool (must be installed):

    ./inv_visualizator.py --show -li i -ri j inferiors/sort_array_of_binaries.py sort array

This will update at each step the `array.jpg` image and display it.

Or proceed step-by-step with, optionally displaying aside the updated `source.jpg` and `array.jpg` image:

    ./inv_visualizator.py --live -li i -ri j inferiors/sort_array_of_binaries.py sort array
