#!/usr/bin/env python3
"""Sorting algorithm to illustrate the notion of loop invariants"""


def sort(array):
    """Sorts array containing only ones and zeros"""
    i = 0
    j = len(array) - 1
    while i != j:
        if array[i] == 0:
            i += 1
        else:
            array[i] = array[j]
            array[j] = 1
            j -= 1


def main():
    """Tests the above function"""
    array = [0, 1, 0, 1, 0, 1, 0, 1, 0]
    print("before :", array)
    sort(array)
    print("after :", array)


if __name__ == "__main__":
    main()
