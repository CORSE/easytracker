# Requirements

## kitty
* You need to have kitty installed. It is a terminal able to display images and to be split.
* On debian/ubuntu:
```bash
sudo apt install kitty
touch ~/.config/kitty/kitty.conf
echo "allow_remote_control yes" >> ~/.config/kitty/kitty.conf
```

## PYTHONPATH
* You need to have your PYTHONPATH set in your bashrc:
```bash
echo "export PYTHONPATH=/path/to/easytracker/" >> ~/.bashrc
source  ~/.bashrc
```

## VIM server
* You need to have vim in server mode. You need to compile it (probably not available in your distribution). You need libXt-dev.
```bash
sudo apt install libxt-dev
git clone https://github.com/vim/vim.git
cd vim
./configure
sudo make install
```
* vim should have the `--servername` option (check with --help that you have a `+` sign before `clientserver`)
* You then want to put this in your `~/.vimrc`:
```bash
touch ~/.vimrc
cat - >> ~/.vimrc <<EOF
" Turn syntax highlighting on.
syntax on

" Change colors
colorscheme slate
" colorscheme morning

" Show line numbers
set number

function! HighlightLine(lineno)
  call cursor(a:lineno, 0)
  mark l
  execute 'match Search /\%'.a:lineno.'l/'
  redraw
endfunction
EOF
```

# Usage
* launch kitty from your terminal
* In kitty: `./visualize_rec_func.sh examples/mystery.py m 12 x y`
* `./visualize_rec_func.sh examples/mergesort.py tri 25 tab`
* The first argument is the name of the recursive function. The second is the font size. The remaining is the list of parameters of the recursive function you want to show. It will then show those arguments in addition to the returned variable value.

