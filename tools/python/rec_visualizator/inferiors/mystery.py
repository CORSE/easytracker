#!/usr/bin/env python3
# pylint: disable=all


def m(x, elems):
    idx = len(elems) - 1 - x
    elems[idx] += 1
    if x == 0:
        return
    m(x - 1, elems)
    m(x - 1, elems)


def main():
    r = 3
    elems = [0] * (r + 1)
    m(r, elems)
    print(elems)


if __name__ == "__main__":
    main()
