#!/usr/bin/env python3
# pylint: disable=all

"""Example to test the behavior of the framework when controlling recursive algorithms."""


def fibo(n: int) -> int:
    """Computes the n-th term of the Fibonacci suite."""
    if n == 0 or n == 1:
        return n

    return fibo(n - 2) + fibo(n - 1)


def main():
    """Program entry point."""
    fibo(6)


if __name__ == "__main__":
    main()
