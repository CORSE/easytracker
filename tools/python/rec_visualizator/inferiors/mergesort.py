#!/usr/bin/env python3
# pylint: disable=all


def tri(tab):
    n = len(tab)
    m = n // 2
    if n == 1:
        return tab

    tab[0:m] = tri(tab=tab[0:m])
    tab[m : n + 1] = tri(tab=tab[m : n + 1])
    fused = list()
    (l, r) = (0, m)
    while l < m or r < n:
        if r == n or (l < m and tab[l] < tab[r]):
            fused.append(tab[l])
            l += 1
        else:
            fused.append(tab[r])
            r += 1
    return fused


def main():
    print(tri([22, 1, 4, 9, 4, 12, 6, 5, 0, 23]))


if __name__ == "__main__":
    main()
