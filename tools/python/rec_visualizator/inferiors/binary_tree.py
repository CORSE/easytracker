#!/usr/bin/env python3
"""Binary tree search example"""
# pylint: disable=all


class Noeud:
    """Un nœud d'un arbre binaire"""

    def __init__(self, val, fils_g, fils_d):
        self.val = val
        self.fils_g = fils_g
        self.fils_d = fils_d

    def __str__(self):
        return (
            f"val={self.val} "
            f"fg={None if self.fils_g is None else 'NotNone'} "
            f"fd={None if self.fils_d is None else 'NotNone'}"
        )


def abr_enonce():
    """Retourne l'arbre de l'énoncé"""
    return Noeud(
        8,
        Noeud(
            3,
            Noeud(1, None, None),
            Noeud(6, Noeud(4, None, None), Noeud(7, None, None)),
        ),
        Noeud(10, None, Noeud(14, Noeud(13, None, None), None)),
    )


def abr_est_present(val, abr):
    if abr is None:
        return False
    elif val == abr.val:
        return True
    elif val < abr.val:
        return abr_est_present(val, abr.fils_g)
    else:
        return abr_est_present(val, abr.fils_d)


def main():
    abr = abr_enonce()
    quatre_present = abr_est_present(4, abr)
    print(f"4 présent dans abr = {quatre_present}")


if __name__ == "__main__":
    main()
