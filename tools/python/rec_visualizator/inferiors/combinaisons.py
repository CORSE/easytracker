#!/usr/bin/env python3
# pylint: disable=all

"""Combinaisons en récursif."""


def c_rec(seq, k):
    def c_aux(st, k, pref):
        if k == 0:
            c = tuple(pref)
            cs.append(c)
            return
        else:
            up_inds = range(st, len(seq))
            for ind in up_inds:
                pref.append(seq[ind])
                c_aux(ind + 1, k - 1, pref)
                pref.pop()
            return

    cs = []
    c_aux(0, k, [])
    return cs


def main():
    print(*c_rec("ABC", 2), sep="\n")


if __name__ == "__main__":
    main()
