#!/bin/bash

if [[ $# -eq 0 || $1 == "-h" ]] ; then
  echo "Usage: $0 <path-to-python-prog> <func_name> <font_size> <func param to show>"
  exit 1
fi

# Parse params
full_path=$(realpath $0)
dir_path=$(dirname $full_path)
prog_name="$1"
func_name="$2"
font_size="$3"
shift 3
args="$@"

# Split the terminal
kitty @ goto-layout tall
kitty @ set-window-title code
kitty @ new-window --title control --keep-focus
kitty @ new-window --title calltree --keep-focus

# Grand écran
kitty @ resize-window -i -25
kitty @ resize-window --match title:control --axis vertical -i -50

# Petit écran
# kitty @ resize-window -i -36
# kitty @ resize-window --match title:control --axis vertical -i -50

# Set font size from parameter
kitty @ set-font-size $font_size

# Cd at proper place in calltree and stackheap window
kitty @ send-text --match title:calltree "cd $dir_path\n"

# Start the rec visualizator tool in the control window
kitty @ focus-window --match title:control
kitty @ send-text --match title:control "cd $dir_path\n"

kitty @ send-text --match title:control "./rec_visualizator.py $prog_name $func_name $args --live\n"

# Open vim in code window
# Must be done last otherwise it disapears, donow why :(
kitty @ send-text --match title:code "vim --servername rec $prog_name\n"

# Close every opened kitty windows
# kitty @ close-window --match title:control
# kitty @ close-window --match title:calltree
