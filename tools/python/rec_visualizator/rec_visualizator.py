#!/usr/bin/env python3

"""Tool to visualize execution of recursive functions."""

from __future__ import annotations

# Standard imports
import argparse
import copy
from dataclasses import dataclass
from enum import Enum
import subprocess
from typing import Optional, Iterable
import sys

# Own imports
from easytracker.init_tracker import init_tracker
from easytracker.interface import EasyTrackerItf
from easytracker import PauseReasonType


class CallState(Enum):
    """Enum type for the state of a call."""

    FUTURE = 0
    ALIVE = 1
    DEAD = 2


@dataclass
class Call:
    """A call to a recursive function."""

    arguments: list[object]
    # pylint: disable=invalid-name
    id: int
    children: list[Call]
    parent: Optional[Call]
    state: CallState
    return_value: object = float("inf")


CALL_STATE_TO_NODE_STYLE = {
    CallState.FUTURE: 'fillcolor="white" style="invis"',
    CallState.ALIVE: 'fillcolor="red" style="filled"',
    CallState.DEAD: 'fillcolor="gray" style="filled"',
}

CALL_STATE_TO_CALL_EDGE_STYLE = {
    CallState.FUTURE: '[color="white" style="invis"]',
    CallState.ALIVE: '[color="black"]',
    CallState.DEAD: '[color="gray"]',
}


def dump_call(calll: Call, dot_file, func_name: str):
    """Dump the given call and recursive call on children."""

    def get_return_edge_style_from_state(calll: Call):
        """Returns the call edge style."""
        style = ""
        color = "white"
        if calll.state == CallState.DEAD:
            if calll.parent:
                if calll.parent.state == CallState.ALIVE:
                    color = "black"
                elif calll.parent.state == CallState.DEAD:
                    color = "gray"
                else:
                    assert False, "Should not be here"
        else:
            style = 'style="invis"'
        return f"color={color} {style}"

    def get_arguments_string(arguments: list[object]):
        """Converts the arguments to a string."""
        arguments_s = ""
        for argument in arguments:
            if isinstance(argument, list):
                argument_s = "["
                argument_s += ",".join(str(elem) for elem in argument)
                argument_s += "]"
            else:
                argument_s = str(argument)
            arguments_s += f"{', ' if arguments_s else ''}{argument_s}"
        return arguments_s

    # The call node is always drawn,
    # invisible if needed
    arguments_s = get_arguments_string(calll.arguments)
    print(
        f'{id(calll)} [{CALL_STATE_TO_NODE_STYLE[calll.state]} shape="rect" margin="0" width="1"'
        f'label="{calll.id}.\\n{func_name}({arguments_s})"]',
        file=dot_file,
    )

    # Edges
    if calll.parent:
        # The edge from parent is always drawn,
        # invisible if needed
        print(
            f"{id(calll.parent)} -> {id(calll)} "
            f"{CALL_STATE_TO_CALL_EDGE_STYLE[calll.state]}",
            file=dot_file,
        )

        # The edge back to parent is always drawn,
        # invisible if needed
        return_edge_style_s = get_return_edge_style_from_state(calll)
        print(
            f"{id(calll)} -> {id(calll.parent)} [{return_edge_style_s} "
            f'label="{calll.return_value}"]',
            file=dot_file,
        )

    # Recursive call on children
    for child in calll.children:
        dump_call(child, dot_file, func_name)


def init_python_tracker(func_name: str, prog_name: str) -> EasyTrackerItf:
    """Inits and starts the python tracker."""
    tracker = init_tracker("python")
    tracker.track_function(func_name)
    tracker.load_program(prog_name)
    tracker.start()
    return tracker


def get_arguments():
    """Get the command line arguments."""
    parser = argparse.ArgumentParser(
        description="tool to visualize execution of " "recursive functions"
    )
    # Positional arguments
    parser.add_argument("prog_name", type=str, help="the program to visualize")
    parser.add_argument("func_name", type=str, help="the function to visualize")
    parser.add_argument(
        "args_names", type=str, help="the arguments of function to visualize", nargs="*"
    )
    # Optional arguments
    parser.add_argument(
        "--live",
        default=False,
        action="store_true",
        help="display nothing, just generate final tree",
    )
    # Optional arguments
    parser.add_argument(
        "--no-wide",
        default=False,
        action="store_true",
        help="Dont scale image up to width of terminal (for 'thin' recursive trees)",
    )
    return parser.parse_args()


def get_arguments_values(
    args_ids: Iterable[str], tracker: EasyTrackerItf
) -> list[object]:
    """Returns the values of the given arguments ids"""
    arguments = []
    for arg_id in args_ids:
        var_value = tracker.get_variable_value(name=arg_id, as_raw_python_objects=True)
        if var_value is None:
            raise ValueError(f"Argument {arg_id} not found")
        arguments.append(copy.deepcopy(var_value.value))
    return arguments


def build_complete_rec_tree(
    prog_name: str, func_name: str, args_ids: list[str]
) -> dict[int, Call]:
    """Builds the complete recursion tree.

    Done by tracking the function, and stopping
    only at fuction entry and exit.
    """
    tracker = init_python_tracker(prog_name=prog_name, func_name=func_name)
    current_call = None
    calls = {}
    next_call_id = 0
    while True:
        # Continue until the program is done
        tracker.resume()
        if tracker.exit_code is not None:
            break

        # If we break because the recursive function was called
        stop_reason = tracker.pause_reason
        if stop_reason.type == PauseReasonType.CALL:
            tracker.next()
            arguments = get_arguments_values(args_ids=args_ids, tracker=tracker)
            current_call = Call(
                arguments, next_call_id, [], current_call, CallState.ALIVE
            )
            calls[next_call_id] = current_call
            next_call_id += 1
            if current_call.parent:
                current_call.parent.children.append(current_call)

        # If we break because the recursive function returned
        elif stop_reason.type == PauseReasonType.RETURN and current_call:
            current_call.return_value = stop_reason.args[2]
            current_call.state = CallState.DEAD
            current_call = current_call.parent

        # Impossible cases:
        else:
            assert False, "We should not be here."
    tracker.terminate()

    return calls


def dump_call_tree_image(calls: dict[int, Call], func_name: str):
    """Dumps the call tree as image."""
    with open("rec.dot", "w", encoding="UTF-8") as dot_file:
        print("digraph rec {", file=dot_file)
        # pylint: disable=len-as-condition
        if len(calls):
            dump_call(calls[0], dot_file, func_name)
        print("}", file=dot_file)
    subprocess.run(["dot", "-Tpng", "rec.dot", "-o", "rec.png"], check=False)


def update_call_tree_image(calls, func_name, scale_up: str):
    """Update call tree image in kitty."""
    dump_call_tree_image(calls, func_name=func_name)
    with subprocess.Popen(
        "kitty @ send-text --match title:calltree clear\\\\n",
        shell=True,
        stdout=subprocess.DEVNULL,
    ) as process:
        process.wait()
    with subprocess.Popen(
        f"kitty @ send-text --match title:calltree "
        f"kitty +kitten icat {scale_up} rec.png\\\\n",
        shell=True,
        stdout=subprocess.DEVNULL,
    ) as process:
        process.wait()


def run_program_step_by_step(prog_name, func_name, calls, args_ids, scale_up: str):
    """Run the program step by step to see call tree evolution."""

    # Clear python terminal if something was displayed there.
    # os.system("clear")

    input("Taper entrée démarrer l'exécution :")

    tracker = init_python_tracker(prog_name=prog_name, func_name=func_name)
    next_call_id = 0
    update_call_tree = False
    resume = False
    while tracker.exit_code is None:
        # Update the call tree image if needed
        if update_call_tree:
            update_call_tree_image(calls, func_name, scale_up)

        # Update line number
        with subprocess.Popen(
            f"vim --servername rec --remote-expr "
            f"'HighlightLine({tracker.next_lineno})'",
            shell=True,
            stdout=subprocess.DEVNULL,
        ) as process:
            process.wait()

        # On avance ligne par ligne ou jusqu'au bout
        if not resume:
            cmd = input(
                "Taper 'entrée' pour executer 1 ligne ou 'c' pour tout exécuter ('q/x' pour quitter):"
            )
            if cmd == "c":
                resume = True
            if cmd in ("q", "x"):
                tracker.force_program_stop()
                sys.exit(0)

        # On exécute ligne par ligne même si on a le resume
        # pour voir un peu ce qu'il se passe.
        tracker.step()

        # If we are on a call to the watched recursive function
        # we update the call tree.
        stop_reason = tracker.pause_reason
        if stop_reason.type == PauseReasonType.CALL:
            current_call = calls[next_call_id]
            arguments = get_arguments_values(args_ids=args_ids, tracker=tracker)
            current_call.arguments = arguments
            current_call.state = CallState.ALIVE
            next_call_id += 1
            update_call_tree = True
        elif stop_reason.type == PauseReasonType.RETURN:
            current_call.state = CallState.DEAD
            current_call = current_call.parent
            update_call_tree = True
        else:
            update_call_tree = False
    tracker.terminate()


def main():
    """Program entry point"""

    # Arguments
    args = get_arguments()
    if args.no_wide:
        scale_up = ""
    else:
        scale_up = "--scale-up"

    # Clear the GUI, that is the image ;-)
    # if we are live.
    if args.live:
        dump_call_tree_image({}, args.func_name)

    # Build the complete call tree
    # with a first full run and
    # return if this is the only
    # thing the user wants.
    calls = build_complete_rec_tree(
        prog_name=args.prog_name, func_name=args.func_name, args_ids=args.args_names
    )
    if not args.live:
        dump_call_tree_image(calls, args.func_name)
        return

    # Starts a new execution step by step
    for calll in calls.values():
        calll.state = CallState.FUTURE
    run_program_step_by_step(
        prog_name=args.prog_name,
        func_name=args.func_name,
        calls=calls,
        args_ids=args.args_names,
        scale_up=scale_up,
    )


if __name__ == "__main__":
    main()
