# pylint: disable=all


def swap(tab, i, j):
    temp = tab[i]
    tab[i] = tab[j]
    tab[j] = temp


def main():
    tab = [2, 5, 8, 9, 4]
    for i in range(5):
        if tab[i] > tab[0]:
            swap(tab, i, 0)
