void swap(int* tab, int i, int j) {
    int temp = tab[i];
    tab[i] = tab[j];
    tab[j] = temp;
}

int main() {
    int tab[5] = {2, 5, 8, 9, 4};

    int i = 0;
    for (i = 0; i < 5; i++) {
        if (tab[i] > tab[0]) {
            swap(tab, i, 0);
        }
    }
}
