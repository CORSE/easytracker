fn swap(tab: &mut [i32], i: usize, j: usize) {
    let temp = tab[i];
    tab[i] = tab[j];
    tab[j] = temp;
}

fn main() {
    let mut tab = [2, 5, 8, 9, 4];

    for i in 0..5 {
        if tab[i] > tab[0] {
            swap(&mut tab, i, 0);
        }
    }
}
