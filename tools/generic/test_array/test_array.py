#!/usr/bin/env python3.9

"""Simple application to illustrate framework capabilities."""

# Standard imports first
import os
import sys

# Own imports last
from easytracker.init_tracker import init_tracker
from easytracker.types.variable import NameBinding

from visualprimitives.pygame.window import BasicWindow, TOP_RIGHT_ANCHORS
from visualprimitives.pygame.gui import source_code_gui, debugger_gui
from visualprimitives.pygame.structures.array import VisualArray

START_BUTTON_TEXT = "Start"
NEXT_BUTTON_TEXT = "Next"
STEP_BUTTON_TEXT = "Step"
NEXTI_BUTTON_TEXT = "Next i"

if len(sys.argv) != 2:
    print("Usage: ./test_array.py [python|C|rust]")
    sys.exit(1)

lang = sys.argv[1]
if lang == "python":
    TRACKER_NAME = "python"
    EXTENSION = ".py"
    PROG_FILE = os.path.join(os.path.dirname(__file__), "inferiors/sort.py")
elif lang == "C":
    TRACKER_NAME = "GDB"
    EXTENSION = ".c"
    PROG_FILE = os.path.join(os.path.dirname(__file__), "inferiors/sort")
elif lang == "rust":
    TRACKER_NAME = "GDB"
    EXTENSION = ".rs"
    PROG_FILE = os.path.join(os.path.dirname(__file__), "inferiors/sort_rs")
else:
    raise ValueError(
        f"Unknown language {lang}.\n" "Usage: ./test_array.py [python|C|rust]"
    )

SOURCE_FILENAME = os.path.join(
    os.path.dirname(__file__), "inferiors", "sort" + EXTENSION
)

UNDEFINED_VARIABLE_VALUE = None


# pylint: disable=too-many-statements
def main():
    """ "Application's entry point."""

    def update_gui():
        """Update current line arrow, tab and i in the GUI."""

        # Update arrow showing current line in the GUI
        source_code_gui.get_instance().update()

        # 'tab' has been defined ?
        # If yes draw it for the first time
        nonlocal tab_variable_value
        new_tab_value = debugger_itf.get_variable_value(tab_variable_id)
        if (
            tab_variable_value == UNDEFINED_VARIABLE_VALUE
            and new_tab_value != UNDEFINED_VARIABLE_VALUE
        ):
            visual_array.show_array()
        tab_variable_value = new_tab_value

        # 'i' has been defined ?
        # If yes draw it for the first time
        nonlocal i_variable_value
        new_i_value = debugger_itf.get_variable_value(i_variable_id)
        if (
            i_variable_value == UNDEFINED_VARIABLE_VALUE
            and new_i_value != UNDEFINED_VARIABLE_VALUE
        ):
            visual_array.show_index(i_variable_id.name)
        i_variable_value = new_i_value

        # 'tab' is defined, update GUI with its values, even
        # if it didn't changed since last time, who cares ??
        if tab_variable_value != UNDEFINED_VARIABLE_VALUE:
            visual_array.init_array(new_tab_value.value)

        # 'i' is defined, update GUI with its values, even
        # if it didn't changed since last time, who cares ??
        if i_variable_value != UNDEFINED_VARIABLE_VALUE:
            visual_array.set_index_value(i_variable_id.name, new_i_value.value)

    def start_button_pressed():
        """Start the program and updates the GUI."""
        debugger_itf.start()
        source_code_gui.get_instance().update()
        debugger_gui.get_instance().get_button_by_text(START_BUTTON_TEXT).disable()
        debugger_gui.get_instance().get_button_by_text(NEXT_BUTTON_TEXT).enable()
        debugger_gui.get_instance().get_button_by_text(STEP_BUTTON_TEXT).enable()
        debugger_gui.get_instance().get_button_by_text(NEXTI_BUTTON_TEXT).enable()

    def next_button_pressed():
        """Execute the next instruction, without stepping into functions.

        Also update the GUI once the next instruction has been executed.
        """
        debugger_itf.next()
        update_gui()

    def step_button_pressed():
        """Execute the next instruction, stepping into functions if needs be.

        Also update the GUI once the next instruction has been executed.
        """
        debugger_itf.step()
        update_gui()

    def nexti_button_pressed():
        """Continue to next i and updates the GUI."""
        debugger_itf.resume()
        update_gui()

    # Init the python tracker
    debugger_itf = init_tracker(TRACKER_NAME)

    # Register variables
    tab_variable_id = NameBinding(name="tab", function_name="main")
    i_variable_id = NameBinding(name="i", function_name="main")

    # Values for tab and i
    tab_variable_value = UNDEFINED_VARIABLE_VALUE
    i_variable_value = UNDEFINED_VARIABLE_VALUE

    # Add watchpoints
    debugger_itf.watch(tab_variable_id)
    debugger_itf.watch(i_variable_id)

    # Build window
    window = BasicWindow()
    _, height = window.window_surface.get_size()

    # Init the array gui in the window (at left)
    visual_array = VisualArray(window, ((50, 50), (500, 200)), 5, [i_variable_id])

    # Init source code gui in window (at right)
    source_code_gui.get_instance().init_gui(
        window.ui_manager, ((-500, 0), (500, height)), anchors=TOP_RIGHT_ANCHORS
    )
    source_code_gui.get_instance().set_source_file(SOURCE_FILENAME)

    # Init graphical debugger  in window (between array and source)
    debugger_gui.get_instance().init_gui(
        window.ui_manager, ((-850, 0), (350, height)), anchors=TOP_RIGHT_ANCHORS
    )

    debugger_gui.get_instance().add_callback_button(
        START_BUTTON_TEXT, (0, 0), callback=start_button_pressed
    )
    debugger_gui.get_instance().add_callback_button(
        NEXT_BUTTON_TEXT, (0, 50), callback=next_button_pressed
    )
    debugger_gui.get_instance().add_callback_button(
        NEXTI_BUTTON_TEXT, (0, 100), callback=nexti_button_pressed
    )
    debugger_gui.get_instance().add_callback_button(
        STEP_BUTTON_TEXT, (0, 150), callback=step_button_pressed
    )
    debugger_gui.get_instance().get_button_by_text(NEXT_BUTTON_TEXT).disable()
    debugger_gui.get_instance().get_button_by_text(STEP_BUTTON_TEXT).disable()
    debugger_gui.get_instance().get_button_by_text(NEXTI_BUTTON_TEXT).disable()
    # debugger_gui().add_callback_button("Restart", (0, 150),
    #                                    callback=debugger_itf.start)

    # Load test program
    debugger_itf.load_program(PROG_FILE)

    # Run the pygame loop until the window it is closed.
    window.run()

    # if the window has been closed, but the program
    # being debugged is still running, the program will
    # not exit. Hence we force the exit.
    debugger_itf.force_program_stop()
    debugger_itf.terminate()


if __name__ == "__main__":
    main()
