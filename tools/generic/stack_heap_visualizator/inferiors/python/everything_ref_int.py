#!/usr/bin/env python3
# pylint:disable = all

"""A small code for us, a big one for the interpreter"""


def main():
    i = 42
    j = i
    k = 41
    k += 1
    a = i is k
    b = i == k
    k = 17
    i = 6107899
    j = 6107898
    j += 1
    a = i is j
    b = i == j


if __name__ == "__main__":
    main()
