#!/usr/bin/env python3

"""What happens when we pass arguments to a function"""


def add_1(integer):
    """A function to add 1, super !!"""
    integer += 1
    return integer


def main():
    """Program entry point."""
    integer = 6
    add_1(integer)
    print(f"{integer = }")


if __name__ == "__main__":
    main()
