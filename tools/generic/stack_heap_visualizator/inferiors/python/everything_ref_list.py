#!/usr/bin/env python3
# pylint:disable = all

"""A small code for us, a big one for the interpreter"""


def main():
    list1 = [3, 2, 1, "ready", "study", "go"]
    list2 = list1
    list2[1] = 42
    list2[2] = 17


if __name__ == "__main__":
    main()
