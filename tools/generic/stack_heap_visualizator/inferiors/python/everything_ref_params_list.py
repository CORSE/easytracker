#!/usr/bin/env python3

"""Que se passe-t-il quand on passe des arguments à une fonction ?"""


def add_1(integers):
    """Une fonction qui ajoute 1, super !!"""
    integers.append(1)


def main():
    """Point d'entrée du programme."""
    i = 3
    integers = [i, 2]
    add_1(integers)
    print(f"{integers = }")


if __name__ == "__main__":
    main()
