#!/usr/bin/env python3
# pylint:disable = all
"""Illustrates stack diagram"""


def f(p1, p2):
    a = 3
    print(f"{p1 = }, {p2 = }, {a = }")


def main():
    a = 4
    l = [1, 2, 3]
    t = ("1", "2")
    f(a, t)
    print(f"{a = }, {l = }, {t = }")


if __name__ == "__main__":
    main()
