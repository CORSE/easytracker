#!/usr/bin/env python3
# pylint:disable = all

"""Example with a custom class to show STRUCT type"""

import sys


class CustomClass:
    def __init__(self):
        self.a = None
        self.b = None

    def __str__(self):
        return f"a={self.a}, b={self.b}"


def main():
    instance = CustomClass()
    instance.a = 17
    instance.b = "seven"
    print(instance)


if __name__ == "__main__":
    main()
