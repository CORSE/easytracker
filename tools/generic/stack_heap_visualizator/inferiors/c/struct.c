#include <stdlib.h>

struct MyStruct {
    int a;
    int b;
};

struct MyOtherStruct {
    struct MyStruct field_one;
    float field_two;
};

int main() {

    struct MyStruct s_stack;
    s_stack.a = 17;
    s_stack.b = 19;

    struct MyStruct* s_heap = malloc(sizeof(struct MyStruct));
    *s_heap = s_stack;

    struct MyOtherStruct other_s_stack;
    other_s_stack.field_one = s_stack;
    other_s_stack.field_two = 3.14;

    return 0;
}