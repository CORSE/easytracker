#include<stdlib.h>

void inc(int* a) {
    *a += 1;
}

int main() {
    int a = 5;
    int b = 2;

    a = 3;
    b = a + 2;

    int *c = &a;

    inc(&b);

    int* d = malloc(sizeof(int));
    int** e = malloc(sizeof(int*));
    int** f = malloc(sizeof(int*));

    int*** g = malloc(sizeof(int**));

    d = &b;
    *d += 2;

    *e = d;

    *f = &b;

    *g = f;

    return 0;
}
