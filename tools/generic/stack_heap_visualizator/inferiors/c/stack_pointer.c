void inc(int* a) {
    *a += 1;
}

int main() {
    int a = 5;
    int b = 2;

    a = 3;
    b = a + 2;

    int *c = &a;

    inc(&b);

    return 0;
}
