#!/usr/bin/env python3

"""Tool used to generate stack/heap images.

This tool can either be used in live with step by step execution
of the inferior or in a postmortem mode where a single image is
generated after execution of each line.
"""

# Standard imports first
from argparse import ArgumentTypeError as err
import argparse
from pathlib import Path
import os
import sys

# Third party imports
# None for now

# Own imports last
from easytracker import init_tracker
from easytracker import Frame
from visualprimitives.stack_heap_dot_display.stack_heap_display import (
    draw_stack_heap,
    draw_stack_only,
)
from visualprimitives.stack_heap_dot_display.utils import Language, DisplayParams
from visualprimitives.source_image import generate_source_image


# pylint: disable=too-many-branches
class PathType:
    """
    A class representing a path type for argparse.
    """

    def __init__(self, exists=True, path_type="file", dash_ok=True):
        """
        exists:
             True: a path that does exist
             False: a path that does not exist, in a valid parent directory
             None: don't care
        path_type: file, dir, symlink, None, or a function returning True for valid paths
             None: don't care
        dash_ok: whether to allow "-" as stdin/stdout
        """

        assert exists in (True, False, None)
        assert path_type in ("file", "dir", "symlink", None) or hasattr(
            type, "__call__"
        )

        self._exists = exists
        self._type = path_type
        self._dash_ok = dash_ok

    def __call__(self, string):
        if string == "-":
            # the special argument "-" means sys.std{in,out}
            if self._type == "dir":
                raise err("standard input/output (-) not allowed as directory path")
            if self._type == "symlink":
                raise err("standard input/output (-) not allowed as symlink path")
            if not self._dash_ok:
                raise err("standard input/output (-) not allowed")
        else:
            exists = os.path.exists(string)
            if self._exists:
                if not exists:
                    raise err(f"path does not exist: '{string}'")

                if self._type is None:
                    pass
                elif self._type == "file":
                    if not os.path.isfile(string):
                        raise err(f"path is not a file: '{string}'")
                elif self._type == "symlink":
                    if not os.path.islink(string):
                        raise err(f"path is not a symlink: '{string}'")
                elif self._type == "dir":
                    if not os.path.isdir(string):
                        raise err(f"path is not a directory: '{string}'")
                elif not self._type(string):
                    raise err(f"path not valid: '{string}'")
            else:
                if not self._exists and exists:
                    raise err(f"path exists: '{string}'")

                path = os.path.dirname(os.path.normpath(string)) or "."
                if not os.path.isdir(path):
                    raise err(f"parent path is not a directory: '{path}'")
                if not os.path.exists(path):
                    raise err(f"parent directory does not exist: '{path}'")

        return Path(string)


def get_args():
    """Get the program to track and other arguments."""

    parser = argparse.ArgumentParser(
        description="generate stack-and-heap-diagram after execution of each line"
    )
    parser.add_argument(
        "program",
        type=PathType(exists=True, path_type="file", dash_ok=False),
        help="the program to track",
    )
    parser.add_argument(
        "-f",
        "--functions",
        type=str,
        nargs="+",
        help="list of functions to trace, default to all functions",
    )
    parser.add_argument(
        "-in", "--image_name", type=str, help="the base name of generated images"
    )
    parser.set_defaults(image_name="stack_heap")
    parser.add_argument(
        "-o",
        "--outputdir",
        type=PathType(exists=True, path_type="dir", dash_ok=False),
        help="path of the directory where to generate images, default is current working directory",
    )
    parser.set_defaults(outputdir=".")
    parser.add_argument(
        "-gsi",
        "--generate_source_image",
        dest="generatesourceimage",
        action="store_true",
        help="generates source image with last and next lines highlighted in "
        "addition to stack-and-heap image after execution of each line, default is false",
    )
    parser.set_defaults(generatesourceimage=False)
    parser.add_argument(
        "-l",
        "--live",
        dest="live",
        action="store_true",
        help="waits for user input and update a single image instead of generating an "
        "image after the execution of each line, default is false",
    )
    parser.set_defaults(live=False)
    parser.add_argument(
        "-so",
        "--stackonly",
        dest="stackonly",
        action="store_true",
        help="displays only the stack (no heap)",
    )
    parser.set_defaults(stackonly=False)
    parser.add_argument(
        "-sa",
        "--showaddresses",
        dest="showaddresses",
        action="store_true",
        help="show addresses instead of dots, default is False",
    )
    parser.set_defaults(showaddresses=False)
    parser.add_argument(
        "-ini",
        "--inlinenil",
        dest="inlinenil",
        action="store_true",
        help="inline nil types into enclosing instances",
    )
    parser.set_defaults(inlineprimitives=False)
    parser.add_argument(
        "-ip",
        "--inlineprimitives",
        dest="inlineprimitives",
        action="store_true",
        help="inline primitive types into enclosing instances",
    )
    parser.set_defaults(inlineprimitives=False)
    args = parser.parse_args()
    return args


# pylint: disable=too-many-branches, too-many-statements, too-many-locals
def main():
    """ "Application's entry point."""

    # Init the python tracker then load and start
    # the program to be tracked
    args = get_args()
    prog: Path = args.program
    if prog.suffix == ".py":
        tracker_name = "python"
        language = Language.PYTHON
    else:
        tracker_name = "GDB"
        language = Language.C
    tracker = init_tracker(tracker_name)
    tracker.load_program(str(prog.resolve()))
    tracker.start()

    image_name = args.image_name
    if args.live:
        live_stack_heap_image_name = os.path.join(args.outputdir, image_name)
        print(f"the stack/heap image is in {live_stack_heap_image_name}")
        live_source_image_name = os.path.join(args.outputdir, "source.jpg")
        print(f"the source code image is in {live_source_image_name}")
        print("x for exit, any other key for continue")
    count = 1

    display_params = DisplayParams(
        show_address=args.showaddresses,
        inline_nil=args.inlinenil,
        inline_primitives=args.inlineprimitives,
        inline_all_in_stack=args.stackonly,
        language=language,
    )
    # source_file_name = (
    #     args.prog_name if args.prog_name.endswith(".py") else f"{args.prog_name}.c"
    # )
    while tracker.exit_code is None:
        last_lineno = tracker.last_lineno
        next_lineno = tracker.next_lineno

        if args.live:
            current_image = image_name
        else:
            current_image = f"{count:03}-{image_name}"

        current_image = os.path.join(args.outputdir, current_image)
        frame: Frame = tracker.get_current_frame(as_raw_python_objects=False)
        if args.stackonly:
            draw_stack_only(
                frame,
                image_name=current_image,
                display_params=display_params,
            )
        else:
            draw_stack_heap(
                frame,
                image_name=current_image,
                display_params=display_params,
            )

        last_source_file_path = tracker.last_source_file
        next_source_file_path = tracker.next_source_file
        last_lineno = (
            last_lineno if last_source_file_path == next_source_file_path else None
        )
        if args.generatesourceimage and not args.live:
            generate_source_image(
                image_name=os.path.join(args.outputdir, f"{count:03}-source"),
                filename=next_source_file_path,
                next_lineno=next_lineno,
                last_lineno=last_lineno,
            )

        if args.live:
            generate_source_image(
                image_name=live_source_image_name,
                filename=next_source_file_path,
                next_lineno=next_lineno,
                last_lineno=last_lineno,
            )
            print(f"last executed line = {last_lineno}")
            print(f"next line to be executed = {next_lineno}")
            key = input()
            if key == "x":
                print(f"Now exiting at line {next_lineno}")
                while tracker.exit_code is None:
                    tracker.resume()
                tracker.terminate()
                sys.exit(0)
            tracker.step()
        else:
            tracker.step()
        count += 1
    tracker.terminate()


if __name__ == "__main__":
    main()
