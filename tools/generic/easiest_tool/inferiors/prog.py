# pylint: disable=all


def fold_op(acc: int, elem: int):
    new_acc = acc + elem
    return new_acc


def main():
    acc = 0
    for i in range(5):
        acc = fold_op(acc, i)


if __name__ == "__main__":
    main()
