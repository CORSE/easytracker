int fold_op(int acc, int elem) {
    int new_acc = acc + elem;
    return new_acc;
}

int main() {
    int acc = 0;
    for (int i = 0; i < 5; i++) {
        acc = fold_op(acc, i);
    }

    return 0;
}
