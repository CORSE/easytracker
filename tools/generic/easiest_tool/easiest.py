#!/usr/bin/env python3

"""The simplest easytracker tool."""

# Standard imports
import sys
import os

# Own imports
from easytracker.init_tracker import init_tracker


def main():
    """Tool's entry point."""

    prog_file = sys.argv[1]

    tracker_name = None
    if prog_file == "prog.py":
        tracker_name = "python"
    elif prog_file == "prog":
        tracker_name = "GDB"

    tracker = init_tracker(tracker_name)
    tracker.load_program(os.path.join("inferiors", prog_file))
    tracker.start()

    while tracker.exit_code != 0:
        frame = tracker.get_current_frame(as_raw_python_objects=True)
        name = f"Frame name: {frame.name} "
        variables = ", ".join(
            [f"{var.name}: {var.value}" for var in frame.variables.values()]
        )
        print(name, variables)

        tracker.step()
    tracker.terminate()


if __name__ == "__main__":
    main()
