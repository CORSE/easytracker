#!/usr/bin/env python3.9

"""Command line frontend for EasyTracker."""

# Standard imports
import os

# Third party imports
from prompt_toolkit import PromptSession
from prompt_toolkit.history import FileHistory
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
from prompt_toolkit.completion import WordCompleter
from prompt_toolkit.validation import Validator, ValidationError
from prompt_toolkit.styles import Style

# Own imports
from easytracker import init_tracker

HISTORY_FILE = os.path.expanduser("~/.easytracker_history")
CMDS = ["load", "start", "resume", "next", "step", "quit", "where"]
CMD_COMPLETER = WordCompleter(CMDS)


style = Style.from_dict({"bottom-toolbar": "#ff0000"})


class CmdValidator(Validator):
    """Validate input is valid command"""

    @staticmethod
    def is_valid_cmd(cmd: str) -> bool:
        """Indicates if cmd is valid."""
        for ok_cmd in CMDS:
            if ok_cmd.startswith(cmd):
                return True
        return False

    def validate(self, document):
        if document.current_line:
            cmd = document.current_line.split()[0]
            if not CmdValidator.is_valid_cmd(cmd):
                raise ValidationError(
                    message="This input is not a valid EasyTracker command"
                )


def exec_where(tracker) -> str:
    """Returns string showing where is the tracker"""
    last_line = tracker.last_lineno
    next_line = tracker.next_lineno
    return f"{last_line = }, {next_line = }"


def run_cmd(cmd: str, args: list[str], tracker):
    """Runs the given command"""
    if tracker.inferior is None:
        if cmd == "load":
            tracker.load_program(args[0])
            tracker.start()
        else:
            print("No program loaded yet. Use 'load' command")
    else:
        if cmd == "step":
            tracker.step()
        elif cmd == "next":
            tracker.next()
        elif cmd == "where":
            print(exec_where(tracker))
        elif cmd == "resume":
            tracker.resume()


def toolbar(where):
    """Returns string with toolbar update with where"""
    return [("class:bottom-toolbar", where)]


def main():
    """Entry point of the program."""

    if not os.path.isfile(HISTORY_FILE):
        with open(HISTORY_FILE, mode="x", encoding="utf-8") as _:
            pass
    session = PromptSession(
        "easytracker> ",
        history=FileHistory(HISTORY_FILE),
        auto_suggest=AutoSuggestFromHistory(),
        validator=CmdValidator(),
    )

    tracker = init_tracker("python")
    while True:
        cmd, *args = session.prompt(
            bottom_toolbar=toolbar(exec_where(tracker)), style=style
        ).split()
        run_cmd(cmd, args, tracker)
        if cmd == "quit":
            break
    tracker.resume()
    tracker.terminate()


if __name__ == "__main__":
    main()
