#!/usr/bin/env python3.9

"""A tool to dump memory graphs from easytracker and from the dot visualization preprocessor
"""

# Standard imports first
import argparse
import os
import pickle

import pydot

# Own imports last
from easytracker import (
    init_tracker_from_prog_name,
    Variable,
    Frame,
    Value,
    InvalidMemory,
    AbstractType,
)
from easytracker.utils import filter_functions


SEEN: dict[str, pydot.Node] = {}
# this is just to ensure name unicity it is not a real counter sometimes it is incremented even if it is not a REF
REF_COUNTER: int = 0


def get_args():
    """Get the program to track and other arguments."""

    def dir_path(path):
        """Returns true if path is a valid directory path."""
        if os.path.isdir(path):
            return path
        os.makedirs(path)
        return path

    parser = argparse.ArgumentParser(
        description="debug tool to dump memory graphs in graphviz"
    )
    parser.add_argument("prog_name", type=str, help="the program to debug")
    parser.add_argument(
        "--outputdir",
        "-o",
        type=dir_path,
        help="path of the directory where to generate images",
    )
    parser.add_argument(
        "--as_python_raw_object",
        "-p",
        action="store_true",
        help="If False, decorate the memory with display information",
    )
    parser.add_argument(
        "--dump_ref",
        "-r",
        action="store_true",
        help="path of the directory where to dump pickle models",
    )
    parser.add_argument(
        "--dump_print",
        "-pp",
        action="store_true",
        help="Print in stdout the model",
    )
    parser.set_defaults(outputdir=".")

    args = parser.parse_args()
    return args


# pylint: disable=global-statement
def dot_name(obj) -> str:
    """This updates the REF_COUNTER so it should be called once per object"""
    global REF_COUNTER
    if isinstance(obj, Frame):
        name = f"frame_name_{obj.name}"
    elif isinstance(obj, Variable):
        name = f"var_{obj.name}_{obj.function_name}_{obj.depth}"
    elif isinstance(obj, Value):
        if obj.address is not None:
            name = f"val_{obj.address}"
        else:
            name = f"val_ref_{REF_COUNTER}"
            REF_COUNTER += 1
    else:
        name = str(id(obj))

    return name


def draw_value(graph, value: Value):
    """Draws the given value as a new node in the given graph"""
    computed_dot_name = dot_name(value)
    if computed_dot_name in SEEN:
        return SEEN[computed_dot_name]
    # invalid memory
    if isinstance(value, InvalidMemory):
        content = f"INVALID MEMORY {hex(value.address)}"  # type: ignore
        invalid_node = pydot.Node(computed_dot_name, label=content)
        graph.add_node(invalid_node)
        SEEN[computed_dot_name] = invalid_node
        return invalid_node

    # normal value
    if value.address is None:
        content_header = (
            f"type_enum={value.abstract_type.name} "
            f"type={value.language_type} "
            f"address=Python stack location={value.location.name}"
        )
    else:
        content_header = (
            f"type_enum={value.abstract_type.name} "
            f"type={value.language_type} "
            f"address={hex(value.address)} "
            f"location={value.location.name}"
        )

    if value.abstract_type == AbstractType.REF:
        content = value.content  # type: ignore
    elif value.abstract_type == AbstractType.LIST:
        content = "[ " + "\u25CF " * len(value.content) + "]"  # type: ignore
    else:
        content = str(value.content)

    value_node = pydot.Node(computed_dot_name, label=f"{content_header}\n{content}")
    graph.add_node(value_node)

    # test if there are printable children
    if value.abstract_type == AbstractType.REF:
        child_node = draw_value(graph, value.content)  # type: ignore
        graph.add_edge(pydot.Edge(value_node, child_node))
    elif value.abstract_type == AbstractType.LIST:
        for ref_child in value.content:  # type: ignore
            child_node = draw_value(graph, ref_child)  # type: ignore
            graph.add_edge(pydot.Edge(value_node, child_node))

    SEEN[computed_dot_name] = value_node
    return value_node


def draw_python(graph, python_obj):
    """Draws the given python instance as a new node in the given graph"""
    computed_dot_name = id(python_obj)
    if computed_dot_name in SEEN:
        return SEEN[computed_dot_name]

    value_node = pydot.Node(computed_dot_name, label=str(python_obj))
    graph.add_node(value_node)

    # test if there are printable children

    SEEN[computed_dot_name] = value_node
    return value_node


def draw_variable(graph, var: Variable):
    """Draws the given Variable as two new nodes in the given graph"""
    if var.function_name is not None:
        content = f"name={var.name} func_name={var.function_name} depth={var.depth}"
    else:
        content = f"global name={var.name}"
    var_node = pydot.Node(dot_name(var), label=content, shape="house")
    graph.add_node(var_node)

    # draw value
    # if this is a regular variable, print the node value
    if isinstance(var, Variable):
        value_node = draw_value(graph, var.value)
    else:
        # draw python
        value_node = draw_python(graph, var.value)

    # add the edge between variable and child value
    graph.add_edge(pydot.Edge(var_node, value_node))

    return var_node


def draw_globals(graph, glob_vars: list[Variable]):
    """Draws the given variables in the given graph"""
    for var in glob_vars:
        draw_variable(graph, var)


def draw_frame(graph, frame):
    """Draws the given frame in the given graph"""
    content = f"name={frame.name} depth={frame.depth} last_line={frame.last_line} next_line={frame.next_lineno}"
    frame_node = pydot.Node(dot_name(frame), label=content, shape="rectangle")
    graph.add_node(frame_node)

    # draw parent frame
    if frame.parent is not None:
        parent_node = draw_frame(graph, frame.parent)
        graph.add_edge(pydot.Edge(frame_node, parent_node))

    # draw variables
    for var in frame.variables.values():
        variable_node = draw_variable(graph, var)
        graph.add_edge(pydot.Edge(frame_node, variable_node))

    return frame_node


# pylint: disable=too-many-branches, too-many-statements
def main():
    """ "Application's entry point."""

    # Init the python tracker then load and start
    # the program to be tracked
    args = get_args()

    # cat program file
    if args.prog_name.endswith(".py"):
        os.system(f"cat {args.prog_name}")

    tracker = init_tracker_from_prog_name(args.prog_name)
    tracker.load_program(args.prog_name)
    tracker.start()

    count = 0

    while tracker.exit_code is None:
        # convert the memory graph to dot variable graph
        frame = tracker.get_current_frame(args.as_python_raw_object)
        global_vars = tracker.get_global_variables(args.as_python_raw_object)

        if args.dump_print:
            print(frame)
            print(global_vars)

        count += 1

        if args.dump_ref:
            if args.prog_name.endswith(".py"):
                global_vars = filter_functions(global_vars)

            pickle_frame_name = os.path.join(args.outputdir, f"frame-{count}.pickle")
            pickle_glob_name = os.path.join(args.outputdir, f"glob-{count}.pickle")

            with open(pickle_frame_name, "wb") as frame_file:
                pickle.dump(frame, frame_file)

            with open(pickle_glob_name, "wb") as glob_file:
                pickle.dump(global_vars, glob_file)
        else:
            image_name = os.path.join(args.outputdir, f"{count}-debug.png")

            graph = pydot.Dot("memory_graph", graph_type="digraph")
            SEEN.clear()

            # draw global variables
            draw_globals(graph, global_vars.values())
            # draw stack
            draw_frame(graph, frame)
            # pylint: disable=no-member
            # write_png does exist
            graph.write_png(image_name)

        print("Done", count)

        tracker.step()
    tracker.terminate()


if __name__ == "__main__":
    main()
