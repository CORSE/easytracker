"""A GDB plugin to track dlopen loaded libraries"""

# pylint: disable=import-error
import os
import re

import gdb

from dynamic_library_tracking import library_tracking  # type: ignore

DEBUG_LIBRARY_TRACKING = 0

LIBDLOPEN_PATH = os.path.join(os.path.dirname(__file__), "libdlopen.c")


def log_libs(message: str):
    """logs the given string in log.txt for debug"""
    # pylint: disable=unspecified-encoding
    if DEBUG_LIBRARY_TRACKING:
        with open("logs.txt", "a") as log_f:
            print(message, file=log_f)


class DlopenBreakpoint(gdb.Breakpoint):
    """Virtual automatic breakpoint on dlopen/dlmopen calls"""

    def stop(self):
        """Tracks the requested filename and the returned address"""
        filename = str(gdb.parse_and_eval("filename"))
        filename = re.sub(r'.*"(.*)"$', r"\1", filename)
        ret_pointer = int(gdb.parse_and_eval("ret_pointer"))

        log_libs(f"DLOPEN {ret_pointer} {filename}")

        if filename and ret_pointer != int(0):
            library_tracking.DLOPEN_LIBRARIES[ret_pointer] = filename

        return False  # continue execution


class DlcloseBreakpoint(gdb.Breakpoint):
    """Virtual automatic breakpoint on dlclose calls"""

    def stop(self):
        """Tracks the deallocated library handle"""

        handle = int(gdb.parse_and_eval("handle"))
        ret_code = int(gdb.parse_and_eval("ret_code"))

        log_libs(f"DLCLOSE {handle} {ret_code}")

        if handle != int(0) and ret_code == int(0):
            try:
                del library_tracking.DLOPEN_LIBRARIES[handle]
            except KeyError:
                log_libs(f"WARNING: Unexpected dlclose of handle {handle}")

        return False  # continue execution


class LibraryWrapperCommand(gdb.Command):
    """A command to place dynamic library wrappers and initialize"""

    def __init__(self):
        super().__init__("place-dynamic-library-wrappers", gdb.COMMAND_NONE)

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        # initialize dictionary of libraries
        library_tracking.initialize_dlopen_libraries()
        line_map = {
            "dlopen": [DlopenBreakpoint, 0],
            "dlmopen": [DlopenBreakpoint, 0],
            "dlclose": [DlcloseBreakpoint, 0],
        }
        # pylint: disable=unspecified-encoding
        with open(LIBDLOPEN_PATH) as cfile:
            for lineno, line in enumerate(cfile.readlines(), 1):
                for func, info in line_map.items():
                    if f"@@ BKPT:{func} @@" in line:
                        log_libs(f"BKPT:{func}: {LIBDLOPEN_PATH}:{lineno}")
                        info[1] = lineno
        for func, info in line_map.items():
            bkpt_cls, lineno = info
            if lineno == 0:
                # pylint: disable=broad-exception-raised
                raise Exception(
                    "Can't find @@ BKPT:{func} @@ marker in {LIBDLOPEN_PATH}"
                )
            bkpt_cls(f"{LIBDLOPEN_PATH}:{lineno}", internal=True)


LibraryWrapperCommand()
