/*
 * Wrapper library for capturing dlopen/dlclose calls.
 */

#define _GNU_SOURCE
#include <dlfcn.h>
#include <stddef.h>

void *dlopen(const char *filename, int flags)
{
    void *ret_pointer = NULL;
    void *(*fcn)(const char *, int) = dlsym(RTLD_NEXT, "dlopen");
    if (fcn != NULL)
        ret_pointer = fcn(filename, flags);
    /* @@ BKPT:dlopen @@ marker for bkpts_and_cmds.py */
    return ret_pointer;
}

void *dlmopen(Lmid_t lmid, const char *filename, int flags)
{
    void *ret_pointer = NULL;
    void *(*fcn)(Lmid_t, const char *, int) = dlsym(RTLD_NEXT, "dlmopen");
    if (fcn != NULL)
        ret_pointer = fcn(lmid, filename, flags);
    /* @@ BKPT:dlmopen @@ marker for bkpts_and_cmds.py */
    return ret_pointer;
}

int dlclose(void *handle)
{
    int ret_code = -1;
    int (*fcn)(void *) = dlsym(RTLD_NEXT, "dlclose");
    if (fcn != NULL)
        ret_code = fcn(handle);
    /* @@ BKPT:dlclose @@ marker for bkpts_and_cmds.py */
    return ret_code;
}
