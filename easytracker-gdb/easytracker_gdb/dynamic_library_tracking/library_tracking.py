"""A transfer module to common dictionary of libraies loaded with dlopen"""

# pylint: disable=global-statement

DLOPEN_LIBRARIES: dict[int, str] = {}


def initialize_dlopen_libraries():
    """Function to initialize the dictionary globally"""
    global DLOPEN_LIBRARIES  # dict[int, str]
    DLOPEN_LIBRARIES = {}
