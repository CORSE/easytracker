"""
Provides custom gdb user commands, events hooks,
custom breakpoints and watchpoints, and monitoring
tools that can be used inside a gdb instance
(in our case inside pygdbmi).
"""
# pylint: disable=import-error
# pylint: disable=wrong-import-position
# Our overriden BP and WP only overrides the stop method
# pylint: disable=too-few-public-methods

import os
import sys
from typing import Optional

import gdb

# Do not rely on default path as it does not work
# when the package is installed in a virtual env
# Done once for all plugins as control_plugin.py is loaded first
# install_dir: contains easytracker/
# current_dir: contains other gdb .py plugins
install_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if not os.path.exists(os.path.join(install_dir, "easytracker")):
    # When installed in dev mode, find it in source tree
    install_dir = os.path.dirname(install_dir)
sys.path.insert(0, install_dir)
current_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, current_dir)

from inspect_plugin import frame_depth, convert_to_primitive  # type: ignore
import memory_encoder  # type: ignore

DEBUG_CONTROL = 1

# map from virtual breakpoint number to GDB breakpoints
# this is used by a gdb command when we have a gdb breakpoint number that is
# hit and we want to find the bp number to return to the user
# /!\ for the return breakpoint of a track breakpoint the key is -bp_num instead of bp_num
BP_GDB_MAP: dict[int, gdb.Breakpoint] = {}


def log(message: str):
    """logs the given string in log.txt for debug"""
    if DEBUG_CONTROL:
        # pylint: disable=unspecified-encoding
        with open("logs.txt", "a") as log_f:
            print(message, file=log_f)


class DepthBP(gdb.Breakpoint):
    """Internal breakpoint that just has a depth limit for stopping"""

    def __init__(self, spec: str, maxdepth: Optional[int]):
        self.maxdepth = maxdepth

        super().__init__(spec, temporary=False)

    def stop(self):
        """On stop we just check if the depth is below maxdepth to stop.
        Otherwise we instantly resume"""
        if self.maxdepth is None:
            return True
        return frame_depth(gdb.newest_frame()) <= self.maxdepth


class TrackBP(DepthBP):
    """Internal breakpoint to track function enter and exit"""

    def __init__(
        self,
        funcname: str,
        virtual_number: Optional[int],
        maxdepth: Optional[int],
        internal=False,
    ):
        super().__init__(funcname, maxdepth)
        self.initialized = False
        self.funcname = funcname
        self.internal = internal
        self.virtual_bp_num = virtual_number
        self.return_bp = None
        if internal:
            self.silent = True

        log(
            f"Placed TrackBP for funcname {funcname} with number {virtual_number} and gdb number {self.number}"
        )

    def stop(self):
        """If not initialized place the return breakpoint.
        Then stops depending on depth"""
        if not self.initialized:
            self.__place_return_breakpoint()
            self.initialized = True
        if self.internal:
            # if the breakpoint is internal, notify standard output and don't stop
            log(f"<CALL>{self.funcname}")
            return False
        return super().stop()

    def __place_return_breakpoint(self):
        """Place a return breakpoint on the return instruction"""
        frame = gdb.newest_frame()
        block = frame.block()
        # Find the function block in case we are in an inner block.
        while block:
            if block.function:
                break
            block = block.superblock
        start = block.start
        end = block.end
        arch = frame.architecture()
        instructions = arch.disassemble(start, end - 1)
        for instruction in instructions:
            # takes ret and retq into account
            if instruction["asm"].startswith("ret"):
                address = instruction["addr"]
                self.return_bp = ReturnBP(
                    f"*{address}", self.maxdepth, self.funcname, self.internal
                )
                # the return breakpoint is saved in negative addresses to make the difference with the enter breakpoint
                if self.virtual_bp_num is not None:
                    BP_GDB_MAP[-self.virtual_bp_num] = self.return_bp

    def delete(self):
        """delete the associated return breakpoint before deletion"""
        if self.return_bp is not None:
            self.return_bp.delete()
        super().delete()


class ReturnBP(DepthBP):
    """A breakpoint place on return instructions"""

    def __init__(self, spec, maxdepth, funcname, internal):
        super().__init__(spec, maxdepth)
        self.internal = internal
        self.funcname = funcname
        if internal:
            self.silent = True

        log(f"Placed Return bp for function {funcname} with gdb number {self.number}")

    def stop(self):
        if self.internal:
            log(f"<RETURN>{self.funcname}")
            return False
        return super().stop()


class FunctionBP(gdb.Breakpoint):
    """
    Internal breakpoint to break at function call and place watchpoints
    for now it can only break on main file functions
    """

    def __init__(self, func_name, variable):
        super().__init__(func_name, internal=True, temporary=False)
        self.silent = True
        self.variable_name = variable
        self.func_name = func_name
        self.watchp = None
        self.watchp_number = None
        self.returnbp = None
        log(f"Placed Function BP for func {func_name} and var {variable}")

        # immediatly place a watchpoint if the breakpoint is created
        # if the current frame name is already corresponding to this
        try:
            if self.func_name == gdb.newest_frame().name():
                log("This BP placed a WP immediatly")
                self.enter_frame()
        except gdb.error:
            # no registers, the program has not started yet
            pass

    def enter_frame(self):
        """on frame entry setup the exit breakpoint and the watchpoint"""
        self.watchp = ValueWP(self.variable_name)
        self.watchp_number = self.watchp.number
        self.returnbp = FunctionBPFinish(parent=self)

    def exit_frame(self):
        """on frame exit remove the watchpoint, called from the exit breakpoint"""
        if self.watchp is not None and self.watchp.is_valid():
            self.watchp.delete()

    def stop(self):
        """on stop we create the watchpoint now that the variable is in function_name"""
        self.enter_frame()
        log("Placed a WP because entering scope")
        return False

    def delete(self):
        """delete the associated watchpoint before deletion"""
        if self.watchp is not None and self.watchp.is_valid():
            self.watchp.delete()
        if self.returnbp is not None and self.returnbp.is_valid():
            self.returnbp.delete()
        super().delete()


class FunctionBPFinish(gdb.FinishBreakpoint):
    """A breakpoint place on finish of the given frame for disabling function scoped watchpoints"""

    def __init__(self, parent):
        frame = gdb.newest_frame()
        super().__init__(frame, internal=True)
        self.silent = True
        self.parent = parent
        self.func_name = parent.func_name
        log(
            f"Placed FunctionBPFinish bp for function {self.func_name} with gdb number {self.number}"
        )

    def stop(self):
        """When stopping disable watchpoint"""
        if self.parent.is_valid():
            self.parent.exit_frame()
        return False


class ValueWP(gdb.Breakpoint):
    """
    Internal watchpoint that watches a specific variable.
    """

    def __init__(self, variable: str):
        """
        Initialisation of the break point as a silent watchpoint.
        The variable to watch is given as a string (spec).
        """
        super().__init__(
            variable, type=gdb.BP_WATCHPOINT, temporary=False, internal=True
        )
        self.variable_name = variable
        self.frame = gdb.newest_frame()

    def __is_current_frame_too_high(self):
        """Returns if the current frame is too high in the stack and shouldn't trigger update
        This is done to avoid the call just after the watchpoint is deleted"""
        current_frame = gdb.newest_frame()
        while current_frame is not None:
            if current_frame == self.frame:
                return False
            current_frame = current_frame.older()
        return True

    def stop(self):
        """
        On stop, the breakpoint add the new value of the variable
        to our status log.

        Returns False to tell to gdb not to stop.
        """
        if self.__is_current_frame_too_high():
            return False
        return True


class CustomWatch(gdb.Command):
    """A command to place our custom watchpoints.
    The input is parsed to place a FunctionBP
    The input looks like func:var, if no func is provided then the variable is global
    """

    def __init__(self):
        super().__init__("custom-watch", gdb.COMMAND_NONE)
        self.dont_repeat()

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        virtual_bp_num = int(args[0])
        if ":" in args[1]:
            func, var = args[1].split(":")
            breakp = FunctionBP(func, var)
        else:
            breakp = ValueWP(args[1])

        BP_GDB_MAP[virtual_bp_num] = breakp


custom_watch_command = CustomWatch()


class TrackFunctionCommand(gdb.Command):
    """A command to track function entry and exit."""

    def __init__(self):
        super().__init__("track-function", gdb.COMMAND_NONE)
        self.dont_repeat()

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        virtual_bp_num = int(args[0])
        funcname = args[1]
        try:
            maxdepth = int(args[2])
        except (IndexError, TypeError):
            maxdepth = None
        try:
            exit_only = bool(args[3])
        except IndexError:
            exit_only = False

        BP_GDB_MAP[virtual_bp_num] = TrackBPFinish(
            funcname=funcname,
            virtual_number=virtual_bp_num,
            maxdepth=maxdepth,
            exit_only=exit_only,
        )


TrackFunctionCommand()


class CustomDeleteCommand(gdb.Command):
    """A command to delete breakpoints using the tracker bp number instead of GDB number"""

    def __init__(self):
        super().__init__("custom-delete", gdb.COMMAND_NONE)
        self.dont_repeat()

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        virtual_bp_num = int(args[0])

        BP_GDB_MAP[virtual_bp_num].delete()


CustomDeleteCommand()


class CustomBreak(gdb.Command):
    """A command to place simple vanilla breakpoints but handling max stack depth."""

    def __init__(self):
        super().__init__("custom-break", gdb.COMMAND_NONE)
        self.dont_repeat()

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        virtual_bp_num = int(args[0])
        spec = args[1]
        try:
            maxdepth = int(args[2])
        except ValueError:
            maxdepth = None
        if maxdepth is None:
            breakp = gdb.Breakpoint(spec)
        else:
            breakp = DepthBP(spec, maxdepth)

        BP_GDB_MAP[virtual_bp_num] = breakp


CustomBreak()


class GetRealBPNumber(gdb.Command):
    """A command to get the underlying breakpoint number from a virtual number"""

    def __init__(self):
        super().__init__("get-real-bp-num", gdb.COMMAND_NONE)

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        virtual_bp_number = int(args[0])
        brk_number = -1
        try:
            brk = BP_GDB_MAP[virtual_bp_number]
            if brk.is_valid():
                # valid, i.e. not yet deleted
                brk_number = brk.number
        except KeyError:
            # cannot find virtual bp number
            pass
        print(brk_number)


GetRealBPNumber()


class GetVirtualBPNumberCommand(gdb.Command):
    """A command to get the virtual tracker bp number from the real gdb bp number"""

    def __init__(self):
        super().__init__("get-virtual-bp-num", gdb.COMMAND_NONE)

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        internal_bp_number = int(args[0])

        virtual_number = -1
        bp_type = "break"

        log(f"gdb bp map get {internal_bp_number} len {len(BP_GDB_MAP)}")

        for bp_key_num, bp_instance in BP_GDB_MAP.items():
            bp_key_num = int(bp_key_num)
            log(f"GDB bp {bp_instance} asked {internal_bp_number} key {bp_key_num}")
            if isinstance(bp_instance, FunctionBP):
                # for function BP that places watchpoints
                if (
                    bp_instance.is_valid()
                    and bp_instance.watchp_number == internal_bp_number
                ):
                    bp_type = "watch"
                    virtual_number = bp_key_num
                    break
            elif isinstance(bp_instance, TrackBPFinish):
                if bp_instance.is_valid() and (
                    bp_instance.number == internal_bp_number
                    or internal_bp_number in bp_instance.return_bps
                ):
                    virtual_number = bp_key_num
                    bp_type = bp_instance.state
                    break
            elif bp_instance.is_valid() and bp_instance.number == internal_bp_number:
                # if the key is negative, this is a return breakpoint
                # TODO: TrackBP and negative key are for obsolete implementation
                # of track function, though new implementation with TrackBPFinish
                # stops after return from function, i.e. frame is not available
                if isinstance(bp_instance, ReturnBP):
                    assert bp_key_num < 0
                    virtual_number = -bp_key_num
                    bp_type = "return"
                    break
                if isinstance(bp_instance, TrackBP):
                    bp_type = "call"
                virtual_number = bp_key_num
                break

        log(f"bp type {bp_type} number {virtual_number}")
        if virtual_number == -1:
            virtual_number = internal_bp_number
        log(f"bp type {bp_type} final number {virtual_number}")

        print(f"{bp_type}:{virtual_number}")


GetVirtualBPNumberCommand()


class GetVirtualBPAttrs(gdb.Command):
    """
    Get values of some breakpoint attributes.
    Return encoded dict of resulting value, if an attribute is not
    available the returned dict does not contain the attribute.
    For attributes wich are gdb.Value, convert to primitive type
    """

    def __init__(self):
        super().__init__("get-virtual-bp-attrs", gdb.COMMAND_NONE)

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        virtual_bp_number = int(args[0])
        attrs = args[1:]

        values = {}
        brk = BP_GDB_MAP.get(virtual_bp_number)
        if brk is not None and brk.is_valid():
            for attr in attrs:
                if hasattr(brk, attr):
                    value = getattr(brk, attr)
                    if isinstance(value, gdb.Value):
                        value = convert_to_primitive(value)
                    values[attr] = value
        print(memory_encoder.encode(values))


GetVirtualBPAttrs()


class TrackBPFinish(DepthBP):
    """
    Internal breakpoint to track function enter and exit on finish.
    Note that on exit, the function frame is already popped and
    thus only the returned value can be inspected.
    """

    # pylint: disable=too-many-instance-attributes,too-many-arguments
    def __init__(
        self,
        funcname: str,
        virtual_number: Optional[int],
        maxdepth: Optional[int],
        internal=False,
        exit_only=False,
    ):
        super().__init__(funcname, maxdepth)
        self.funcname = funcname
        self.internal = internal
        self.virtual_bp_num = virtual_number
        self.exit_only = exit_only
        self.return_bps: dict[int, gdb.Breakpoint] = {}
        self.state = "call"
        self.return_value = None
        if internal:
            self.silent = True

        log(
            f"Placed TrackBPFinish for funcname {funcname} with "
            f"exit_only = {exit_only}, "
            f"internal = {internal}, "
            f"virtual number = {virtual_number}, "
            f"gdb number = {self.number}"
        )

    def stop(self):
        """When stopping, place a temporary finish breakpoint for the current frame."""
        self.state = "call"
        stop = super().stop()
        log(f"TrackBPFinish STOPINGGGG {stop = }, {self.exit_only = }")
        if not stop:
            return False
        frame = gdb.newest_frame()
        return_bp = ReturnBPFinish(frame, self, self.internal)
        self.return_bps.update({return_bp.number: return_bp})
        if self.internal:
            # if the breakpoint is internal don't stop
            log(f"<CALL>{self.funcname}")
            return False
        return not self.exit_only

    def delete(self):
        """delete the associated return breakpoints before deletion"""
        for return_bp in self.return_bps.values():
            return_bp.delete()
        self.return_bps.clear()
        super().delete()


class ReturnBPFinish(gdb.FinishBreakpoint):
    """A breakpoint place on finish of the given frame"""

    def __init__(self, frame, parent, internal):
        super().__init__(frame, internal)
        self.internal = internal
        self.funcname = parent.funcname
        self.parent = parent
        if internal:
            self.silent = True
        log(
            f"Placed ReturnBPFinish bp for function {self.funcname} with gdb number {self.number}"
        )

    def stop(self):
        """When stopping report return value into parent breakpoint"""
        self.parent.return_value = self.return_value
        self.parent.state = "return"
        if self.internal:
            log(f"<RETURN>{self.funcname}")
            return False
        return super().stop()

    def delete(self):
        """delete the breakpoint from the parent breakpoints list"""
        del self.parent.return_bps[self.number]
        super().delete()


class InternalTrackCommand(gdb.Command):
    """A command to place simple vanilla breakpoints but handling max stack depth."""

    def __init__(self):
        super().__init__("internal-track", gdb.COMMAND_NONE)
        self.dont_repeat()

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        for funcname in args:
            TrackBPFinish(funcname, None, None, internal=True)


InternalTrackCommand()


class CustomContinue(gdb.Command):
    """A command to continue with a priority so we don't stop on all watchpoints
    Default priority is 0.
    -1 is a special value to stop nowhere
    Regular continue command is custom-continue 0"""

    def __init__(self):
        super().__init__("custom-continue", gdb.COMMAND_NONE)
        self.watchpoint_continue_priority = 0

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        if not args:
            args.append(0)
        self.watchpoint_continue_priority = int(args[0])

        output = gdb.execute("continue", to_string=True)
        log(output)

        self.watchpoint_continue_priority = 0


custom_continue_command = CustomContinue()
