"""
Build Cache utilities.

This module entry points are:
- build_shared_lib
"""
# pylint: disable=too-many-arguments

import os
from pathlib import Path
import hashlib
import subprocess


def build_shared_lib(so_name: str, sources: list[str], cwd: str) -> str:
    """
    Build a shared library, storing cached version.
    parameters:
      so_name: output .so name
      sources: list of c source files, must be relative to cwd
      cwd: working directory where source files are located
    returns:
      the actual output file name to use, located in cache dir
    """
    return BuildSharedLib(so_name, [str(Path(cwd) / src) for src in sources]).build()


class BuildCommand:
    """
    Generic implementation for a cached output build command.
    """

    def __init__(
        self,
        output: str,
        cmds: list[str],
        inputs: list[str],
        env: dict[str, str],
    ) -> None:
        assert not Path(output).is_absolute()
        for inp in inputs:
            assert Path(inp).is_absolute() and Path(inp).exists()
        self.output = output
        self.inputs = inputs
        self.env = env
        self.cmds = cmds

    def build(self) -> str:
        """
        Build to cache and returns the output file
        """
        out_hash = self._cache_cmd()
        cache_dir = self._cache_out_dir(out_hash)
        out = Path(cache_dir) / self.output
        return str(out)

    @classmethod
    def _home(cls) -> str:
        home = Path(os.environ.get("HOME", ""))
        assert (
            home.is_absolute() and home.is_dir()
        ), f"HOME environment variable must be defined: current HOME: '{home}'"
        return str(home)

    @classmethod
    def _cache_dir(cls) -> str:
        return str(Path(cls._home()) / ".cache" / "easytracker" / "builds")

    @classmethod
    def _cache_out_dir(cls, out_hash: str) -> str:
        return str(Path(cls._cache_dir()) / "outs" / out_hash[:2] / out_hash[2:])

    @classmethod
    def _hash_input(cls, input_fname: str) -> str:
        with open(input_fname, "rb") as fin:
            return hashlib.sha256(fin.read()).hexdigest()

    @classmethod
    def _hash_output(
        cls, cmds: list[str], inputs: dict[str, str], env: dict[str, str]
    ) -> str:
        sha = hashlib.sha256()
        sha.update("inputs:\n".encode())
        for in_fname, in_hash in inputs.items():
            sha.update(f"  {in_fname}: {in_hash}\n".encode())
        sha.update("envs:\n".encode())
        for key, val in env.items():
            sha.update(f"  {key}: {val}\n".encode())
        sha.update("cmds:\n".encode())
        for cmd in cmds:
            sha.update(f"  - {cmd}\n".encode())
        return sha.hexdigest()

    def _cache_cmd(self) -> str:
        inputs_hash = {inp: self._hash_input(inp) for inp in self.inputs}
        out_hash = self._hash_output(self.cmds, inputs_hash, self.env)
        cache_dir = self._cache_out_dir(out_hash)
        cached = Path(cache_dir) / self.output
        if not cached.exists():
            self._exec_cmd(cache_dir)
        return out_hash

    def _exec_cmd(self, cache_dir: str) -> None:
        Path(cache_dir).mkdir(parents=True, exist_ok=True)
        out = Path(cache_dir) / self.output
        for cmd in self.cmds:
            proc = subprocess.run(
                cmd,
                check=False,
                shell=True,
                text=True,
                capture_output=True,
                env=self.env,
                cwd=cache_dir,
            )
            assert proc.returncode == 0, (
                f"unable to compile from cache dir: {cache_dir}, cmd: {cmd}, env: {self.env}:\n"
                f"stdout:{proc.stdout}\n"
                f"stderr:{proc.stderr}\n"
            )
        assert out.exists(), f"missing output file in cache dir: {out}"


class BuildSharedLib(BuildCommand):
    """
    Implementation of a cached shared library build.
    """

    def __init__(self, so_name: str, sources: list[str]) -> None:
        super().__init__(
            output=so_name,
            inputs=sources,
            env={
                "PATH": os.environ.get("PATH", "/usr/local/bin:/usr/bin:/bin"),
                "HOME": os.environ.get("HOME", "/"),
                "USER": os.environ.get("USER", ""),
                "SHELL": os.environ.get("SHELL", "/bin/sh"),
                "CC": os.environ.get("EASYTRACKER_CC", "cc"),
                "CFLAGS": os.environ.get("EASYTRACKER_CFLAGS", "-g"),
                "LDFLAGS": os.environ.get("EASYTRACKER_LDFLAGS", ""),
            },
            cmds=[
                f"$CC $CFLAGS -fPIC -shared -Wl,-soname,{so_name} {' '.join(sources)} -o {so_name} $LDFLAGS -ldl"
            ],
        )
