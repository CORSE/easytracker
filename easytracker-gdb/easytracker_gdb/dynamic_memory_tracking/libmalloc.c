/*
 * Memory allocation wrappers using GLibc malloc functions.
 * Do not use GLibC malloc hooks which are deprecated and
 * do not use dlsym method which is not working due to recursive
 * calls to calloc() in particular.
 */

#include <stdlib.h>

extern void *__libc_malloc(size_t);
extern void *__libc_memalign(size_t, size_t);
extern void *__libc_calloc(size_t, size_t);
extern void *__libc_realloc(void *, size_t);
extern void *__libc_reallocarray(void *, size_t, size_t);
extern void __libc_free(void*);

void *malloc(size_t size)
{
    void* ret_pointer;
    ret_pointer = __libc_malloc(size);
    /* @@ BKPT:malloc @@ marker for bkpts_and_cmds.py */
    return ret_pointer;
}

void *memalign(size_t alignment, size_t size)
{
    void* ret_pointer;
    ret_pointer = __libc_memalign(alignment, size);
    /* @@ BKPT:memalign @@ marker for bkpts_and_cmds.py */
    return ret_pointer;
}

void *calloc(size_t nmemb, size_t size)
{
    void* ret_pointer;
    ret_pointer = __libc_calloc(nmemb, size);
    /* @@ BKPT:calloc @@ marker for bkpts_and_cmds.py */
    return ret_pointer;
}

void *realloc(void *ptr, size_t size)
{
    void* ret_pointer = NULL;
    ret_pointer = __libc_realloc(ptr, size);
    /* @@ BKPT:realloc @@ marker for bkpts_and_cmds.py */
    return ret_pointer;
}

void *reallocarray(void *ptr, size_t nmemb, size_t size)
{
    void* ret_pointer = NULL;
    ret_pointer = __libc_reallocarray(ptr, nmemb, size);
    /* @@ BKPT:reallocarray @@ marker for bkpts_and_cmds.py */
    return ret_pointer;
}

void free(void* ptr)
{
    /* @@ BKPT:free @@ marker for bkpts_and_cmds.py */
    __libc_free(ptr);
}
