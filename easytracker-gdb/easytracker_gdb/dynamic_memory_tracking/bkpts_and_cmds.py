"""A GDB plugin to track dynamic memory allocation"""

# pylint: disable=import-error
import os

import gdb

import memory_encoder  # type: ignore
import dynamic_memory_tracking.allocated_blocks as memory_tracking  # type: ignore

DEBUG_MEMORY_TRACKING = 0

LIBMALLOC_PATH = os.path.join(os.path.dirname(__file__), "libmalloc.c")


def log_alloc(message: str):
    """logs the given string in log.txt for debug"""
    # pylint: disable=unspecified-encoding
    if DEBUG_MEMORY_TRACKING:
        with open("logs.txt", "a") as log_f:
            print(message, file=log_f)


EASYTRACKER_HEAP_INIT_VALUE_VAR = "EASYTRACKER_HEAP_INIT_VALUE"
HEAP_INIT_VALUE_STR = os.environ.get(EASYTRACKER_HEAP_INIT_VALUE_VAR)
HEAP_INIT_VALUE = None
if HEAP_INIT_VALUE_STR is not None:
    try:
        if len(HEAP_INIT_VALUE_STR) > 2:
            log_alloc("len of EASYTRACKER_HEAP_INIT_VALUE is greater than 2")
        elif len(HEAP_INIT_VALUE_STR) == 0:
            log_alloc("len of EASYTRACKER_HEAP_INIT_VALUE is 0")
        else:
            HEAP_INIT_VALUE = int(HEAP_INIT_VALUE_STR, 16)
    except ValueError:
        log_alloc("EASYTRACKER_HEAP_INIT_VALUE is not a valid byte in hexadecimal")


class GetAllocatedBlocksCommand(gdb.Command):
    """A command to trigger a memory inspection
    This returns a pickled shadow memory of GDB Values"""

    def __init__(self):
        super().__init__("print-allocated-blocks", gdb.COMMAND_NONE)

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        print(memory_encoder.encode(memory_tracking.ALLOCATED_BLOCKS))
        if "--delete" in args:
            log_alloc("Deleted allocated block dictionary")
            memory_tracking.ALLOCATED_BLOCKS.clear()


GetAllocatedBlocksCommand()


class MallocBreakpoint(gdb.Breakpoint):
    """Virtual automatic breakpoint on malloc calls"""

    def stop(self):
        """Tracks the requested size and the returned address"""
        size = int(gdb.parse_and_eval("size"))
        return_address = int(gdb.parse_and_eval("ret_pointer"))

        log_alloc(f"MALLOC {return_address} {size}")

        memory_tracking.ALLOCATED_BLOCKS[return_address] = size

        if HEAP_INIT_VALUE is not None:
            memset_s = f"(void*)memset(ret_pointer, {HEAP_INIT_VALUE}, size)"
            log_alloc(f"MALLOC {memset_s}")
            log_alloc(gdb.parse_and_eval(memset_s))

        return False  # continue execution


class MemalignBreakpoint(gdb.Breakpoint):
    """Virtual automatic breakpoint on memalign calls"""

    def stop(self):
        """Tracks the requested size and the returned address"""
        size = int(gdb.parse_and_eval("size"))
        return_address = int(gdb.parse_and_eval("ret_pointer"))

        log_alloc(f"MEMALIGN {return_address} {size}")

        memory_tracking.ALLOCATED_BLOCKS[return_address] = size

        if HEAP_INIT_VALUE is not None:
            memset_s = f"(void*)memset(ret_pointer, {HEAP_INIT_VALUE}, size)"
            log_alloc(f"MEMALIGN {memset_s}")
            log_alloc(gdb.parse_and_eval(memset_s))

        return False  # continue execution


class CallocBreakpoint(gdb.Breakpoint):
    """Virtual automatic breakpoint on calloc calls"""

    def stop(self):
        """Tracks the requested size of an element, the number of elements
        and the returned address"""

        n_elem = int(gdb.parse_and_eval("nmemb"))
        size = int(gdb.parse_and_eval("size"))
        return_address = int(gdb.parse_and_eval("ret_pointer"))

        log_alloc(f"CALLOC {return_address} {n_elem} {size}")

        memory_tracking.ALLOCATED_BLOCKS[return_address] = size * n_elem

        return False  # continue execution


class ReallocBreakpoint(gdb.Breakpoint):
    """Virtual automatic breakpoint on realloc calls"""

    def stop(self):
        """Tracks the requested size, the returned address and if a free occured"""

        old_address = int(gdb.parse_and_eval("ptr"))
        size = int(gdb.parse_and_eval("size"))
        return_address = int(gdb.parse_and_eval("ret_pointer"))

        log_alloc(f"REALLOC {return_address} {old_address} {size}")

        if old_address != return_address:
            try:
                del memory_tracking.ALLOCATED_BLOCKS[old_address]
            except KeyError:
                log_alloc(f"WARNING: Double free block at address {int(old_address)}")

        memory_tracking.ALLOCATED_BLOCKS[return_address] = size

        return False  # continue execution


class ReallocarrayBreakpoint(gdb.Breakpoint):
    """Virtual automatic breakpoint on reallocarray calls"""

    def stop(self):
        """Tracks the requested size, the returned address and if a free occured"""

        old_address = int(gdb.parse_and_eval("ptr"))
        n_elem = int(gdb.parse_and_eval("nmemb"))
        size = int(gdb.parse_and_eval("size"))
        return_address = int(gdb.parse_and_eval("ret_pointer"))

        log_alloc(f"REALLOCARRAY {return_address} {old_address} {n_elem} {size}")

        if old_address != return_address:
            try:
                del memory_tracking.ALLOCATED_BLOCKS[old_address]
            except KeyError:
                log_alloc(f"WARNING: Double free block at address {int(old_address)}")

        memory_tracking.ALLOCATED_BLOCKS[return_address] = size * n_elem

        return False  # continue execution


class FreeBreakpoint(gdb.Breakpoint):
    """Virtual automatic breakpoint on free calls"""

    def stop(self):
        """Tracks the freed address"""

        free_address = int(gdb.parse_and_eval("ptr"))

        log_alloc(f"FREE {free_address}")

        if free_address != int(0):
            try:
                del memory_tracking.ALLOCATED_BLOCKS[free_address]
            except KeyError:
                log_alloc(f"WARNING: Double free block at address {free_address}")

        return False  # continue execution


class MallocWrapperCommand(gdb.Command):
    """A command to place dynamic allocation wrappers and initialize allocation blocks"""

    def __init__(self):
        super().__init__("place-dynamic-allocation-wrappers", gdb.COMMAND_NONE)

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        # initialize dictionary blocks
        memory_tracking.initialize_allocated_blocks()
        line_map = {
            "malloc": [MallocBreakpoint, 0],
            "memalign": [MemalignBreakpoint, 0],
            "realloc": [ReallocBreakpoint, 0],
            "reallocarray": [ReallocarrayBreakpoint, 0],
            "calloc": [CallocBreakpoint, 0],
            "free": [FreeBreakpoint, 0],
        }
        # pylint: disable=unspecified-encoding
        with open(LIBMALLOC_PATH) as cfile:
            for lineno, line in enumerate(cfile.readlines(), 1):
                for func, info in line_map.items():
                    if f"@@ BKPT:{func} @@" in line:
                        log_alloc(f"BKPT:{func}: {LIBMALLOC_PATH}:{lineno}")
                        info[1] = lineno
        for func, info in line_map.items():
            bkpt_cls, lineno = info
            if lineno == 0:
                # pylint: disable=broad-exception-raised
                raise Exception(
                    "Can't find @@ BKPT:{func} @@ marker in {LIBMALLOC_PATH}"
                )
            bkpt_cls(f"{LIBMALLOC_PATH}:{lineno}", internal=True)


MallocWrapperCommand()
