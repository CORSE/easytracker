"""A transfer module to import a common dictionary for allocated blocks"""

# pylint: disable=global-statement

ALLOCATED_BLOCKS: dict[int, int] = {}


# ALLOCATED_BLOCKS is a dictionary registering all the dynamically allocated objects
# It is a mapping from addresses (as integer) to allocated size (integer).
def initialize_allocated_blocks():
    """Function to initialize the dictionary globally"""
    global ALLOCATED_BLOCKS
    ALLOCATED_BLOCKS = {}
