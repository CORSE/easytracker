"""
GDB Tracker Module
"""

try:
    from importlib.metadata import version

    __version__ = version("easytracker-gdb")
except:  # pylint: disable=bare-except
    # only useful in no install mode (with PYTHONPATH)
    # change only for major rev M to: M.0.1+dYYYMMDD
    __version__ = "1.0.1+d20230704"

from .gdb_tracker import GdbTracker as Tracker
