""" Provides stack, variable and heap inspection commands """
# pylint: disable=too-few-public-methods
# pylint: disable=too-many-return-statements
# pylint: disable=import-error
# pylint: disable=global-statement

import re
import traceback
from typing import Optional, Union
import time

import gdb  # pylint: disable=import-error
import gdb.types  # pylint: disable=import-error

from easytracker.types.variable import (
    Variable,
    Value,
    InvalidMemory,
    AbstractType,
    Struct,
    Location,
)
from easytracker.types.frame import Frame
import memory_encoder  # type: ignore

# import memory tracking for allocated blocks
from dynamic_memory_tracking import allocated_blocks as memory_tracking  # type: ignore
from dynamic_library_tracking import library_tracking  # type: ignore

DEBUG_INSPECTION = 0
TIME_INSPECTION = 0

FUNC_NAME_RE = re.compile(r" (\w*)? \(")  # a word between " " and " ("
# a word between a " " or a "(" and a " " and an optional "="
VAR_NAME_RE = re.compile(r"(?:\(| )(\w*) ?=")
# match the output of symbols: <line>: <type_or_struct> var_name;
GLOBAL_NAME_RE = re.compile(r"^\d+:(?:.|\n)*? (\w+);\n(?=\d|$)", re.MULTILINE)
# match the output of symbols: <line>: <type_or_struct> *ptr_name;
GLOBAL_PTR_RE = re.compile(r"^\d+:(?:.|\n)*? *(\w+);\n(?=\d|$)", re.MULTILINE)
# match the output of symbols: <line>: <type_or_struct> array_name[<size>];
GLOBAL_ARRAY_RE = re.compile(r"^\d+:(?:.|\n)*? (\w+)[\d+];\n(?=\d|$)", re.MULTILINE)

# Maximum number of array value elements returned
# this is necessary as in some cases gdb does give absurdly large
# size for stack arrays of dynamic size, or more generally when
# inspecting large arrays. gdb itself puts a limit on values
# with the max-value-size parameter (65536 bytes).
# Set to a relatively low value as the returned object model can
# be heavy for thousands of elements.
MAX_ARRAY_VALUE_ELTS = 1024

START_TIMER = True
TIMER = 0.0


def log_insp(message: str, *args):
    """logs the given string in log.txt for debug"""
    # pylint: disable=unspecified-encoding
    if DEBUG_INSPECTION:
        with open("logs.txt", "a") as log_f:
            if len(args) > 0:
                message = message % tuple(args)
            print(message, file=log_f)


def log_insp_time(var_name: str):
    """Start/Stop the timer and write the profile in a file"""
    # pylint: disable=global-statement
    global TIMER, START_TIMER
    if not TIME_INSPECTION:
        return
    if START_TIMER:
        TIMER = time.time()
        START_TIMER = False
        return

    elapsed = time.time() - TIMER
    START_TIMER = True
    # pylint: disable=unspecified-encoding
    with open("time.txt", "a") as log_f:
        print(f"{var_name} {elapsed*1000}", file=log_f)


def transfer_memory_to_main_process(obj: object):
    """Prints the result in a serialized way so the main process can parse the memory"""
    log_insp("Transfer to main interpreter %s", obj)
    print(memory_encoder.encode(obj))


_find_global_names_cache: Optional[list[str]] = None
_find_global_names_libraries: Optional[dict[int, str]] = None


def find_global_names() -> list[str]:
    """Returns a list of global symbols for further inspection"""
    # Reload cache when dict of loaded libraries has canged
    global _find_global_names_cache
    global _find_global_names_libraries
    if (
        _find_global_names_cache is not None
        and _find_global_names_libraries == library_tracking.DLOPEN_LIBRARIES
    ):
        return _find_global_names_cache

    output = gdb.execute("info variables -n -q", True, True)
    log_insp("global variables output %s", output)
    variable_names = GLOBAL_NAME_RE.findall(output)
    variable_names.extend(GLOBAL_ARRAY_RE.findall(output))

    log_insp("global variables number: %s", len(variable_names))
    _find_global_names_libraries = {**library_tracking.DLOPEN_LIBRARIES}
    _find_global_names_cache = variable_names
    return variable_names


def frame_depth(frame):
    """Computes the frame depth in the stack, main is 0"""
    depth = 0
    while frame.older() is not None:
        frame = frame.older()
        depth += 1
    return depth


def get_main_frame():
    """Returns the outermost frame"""
    frame = gdb.newest_frame()
    if frame is None:
        return None
    while frame.older() is not None:
        frame = frame.older()
    return frame


def convert_to_primitive(value: gdb.Value) -> object:
    """checks the type of the value to return the according python object"""
    code = value.type.code
    str_v = str(value)
    if code == gdb.TYPE_CODE_INT:
        try:
            return int(str_v)
        except ValueError:
            try:
                # this is a char
                return chr(int(value))
            except ValueError:
                return int(value)

    if code == gdb.TYPE_CODE_FLT:
        return float(str_v)
    if code == gdb.TYPE_CODE_BOOL:
        return str_v in ["true", "True"]
    if code == gdb.TYPE_CODE_CHAR:
        return str(str_v)[0]
    return str_v


def convert_to_struct(
    struct_value: gdb.Value, fields: list[str], inspect_function
) -> Struct:
    """Inspect a value as a C struct. Cannot use namedtuple because they cannot be easily pickled"""

    struct = Struct()
    for field_name in fields:
        setattr(struct, field_name, inspect_function(struct_value[field_name]))

    return struct


class ModelInspector:
    """Build the model"""

    def __init__(self):
        # A set that knows the address of global variables from the inferior
        self.global_addresses: set[int] = set()
        self._compute_global_addresses()
        # seen address during an inspection to avoid cyclic dependencies (object can be values)
        self.seen_addr: dict[tuple[int, int], object] = {}

    def _compute_global_addresses(self):
        """Computes the global addresses for this inspector"""
        self.global_addresses.clear()
        frame = get_main_frame()
        for name in find_global_names():
            try:
                value = frame.read_var(name)
            except ValueError:
                continue
            if value.address:
                self.global_addresses.add(int(value.address))
            else:
                log_insp("Warning: found global %s without address", name)

    def _get_location(self, address: int) -> Location:
        """Returns the location of the given address"""
        if address in memory_tracking.ALLOCATED_BLOCKS:
            return Location.HEAP
        if address in self.global_addresses:
            return Location.GLOBAL
        for block_start, block_size in memory_tracking.ALLOCATED_BLOCKS.items():
            if block_start < address < block_start + block_size:
                return Location.HEAP
        return Location.STACK

    def _build_value(
        self,
        gdb_value: gdb.Value,
        content: object,
        abstract_type: AbstractType,
        location: Location = None,
    ) -> Value:
        """Build the return Value from the given content and abstract type"""
        address = int(gdb_value.address)
        basic_type = str(gdb.types.get_basic_type(gdb_value.type))
        location = self._get_location(address) if location is None else location

        # Do not print the original value as it may be non-printable and raise an exception
        log_insp("addr: %s type: %s location: %s", hex(address), basic_type, location)

        return Value(
            location,
            address,
            basic_type,
            abstract_type,
            content,  # type: ignore
        )

    def _add_to_cache(
        self, value: Union[Value, object], address: int, size: int
    ) -> Union[Value, object]:
        """add the given to the cache at the given address"""
        self.seen_addr[(address, size)] = value
        return value

    def _inspect_value(self, value: gdb.Value) -> Union[Value, object]:
        """Inspection dispatch that returns a Value"""
        # Do not print value directly which may be very large, and even send a gdb exception
        log_insp(f"_inspect_value: type={value.type}, address={value.address}")
        if not value.address:
            return None

        address = int(value.address)
        full_type = gdb.types.get_basic_type(value.type)
        type_size = full_type.sizeof

        # If we have already seen this object just return the cached value
        cached_value = self.seen_addr.get((address, type_size), None)
        if cached_value is not None:
            log_insp(f"  already seen (0x{address:0x}, {type_size}) --> {cached_value}")
            return self.seen_addr[(address, type_size)]

        # Pointer
        if full_type.code == gdb.TYPE_CODE_PTR:
            # Pointer to several elements into the heap
            value_as_addr = int(value)
            block_size = memory_tracking.ALLOCATED_BLOCKS.get(value_as_addr, None)
            if block_size is not None:
                log_insp(f"   {block_size=}")
                elem_size = full_type.target().sizeof
                nb_elements = block_size // elem_size
                if block_size != elem_size:
                    log_insp(f"   pointer to heap array with {nb_elements} elements")
                    return self._add_to_cache(
                        self._build_value(
                            value,
                            self._inspect_array(value, nb_elements, in_heap=True),
                            AbstractType.REF,
                        ),
                        address,
                        type_size,
                    )

            log_insp("   pointer to single element")
            return self._add_to_cache(self._inspect_pointer(value), address, type_size)

        # Static array type or stack array type
        if full_type.code == gdb.TYPE_CODE_ARRAY:
            elem_size = full_type.target().sizeof
            nb_elements = type_size // elem_size
            log_insp(f"   static or stack array with {nb_elements} elements")
            return self._add_to_cache(
                self._inspect_array(value, nb_elements, in_heap=False),
                address,
                type_size,
            )

        # Heap array

        # Struct type
        if full_type.code == gdb.TYPE_CODE_STRUCT:
            log_insp(f"   structure of size {type_size}")
            fields = [field.name for field in full_type.fields()]
            return self._add_to_cache(
                self._inspect_struct(value, fields), address, type_size
            )

        # If no special type is found, this is a primitive value
        log_insp(f"   primitive of type {full_type}")
        return self._add_to_cache(self._inspect_primitive(value), address, type_size)

    def _inspect_array_elem(self, array_value: gdb.Value, index: int):
        log_insp(f"_inspect_array_elem: {index=}")
        try:
            return self._inspect_value(array_value[index])
        except gdb.error:
            # TODO do something for generic pointer
            return None

    def _inspect_primitive(self, primitive_value: gdb.Value) -> object:
        """Inspect a value as a primitive type"""
        return self._build_value(
            primitive_value,
            convert_to_primitive(primitive_value),
            AbstractType.PRIMITIVE,
        )

    def _inspect_pointer(self, pointer_value: gdb.Value):
        try:
            deref_value = pointer_value.dereference()
            str(deref_value)  # trigger evaluation
            value = self._inspect_value(deref_value)
        except gdb.MemoryError:
            # we need the value of the pointer to know the value of the invalid address
            value = self._inspect_invalid_memory(pointer_value)
        except gdb.error:
            # TODO do something for generic pointer
            value = self._inspect_invalid_memory(pointer_value)

        return self._build_value(pointer_value, value, AbstractType.REF)

    def _inspect_invalid_memory(self, value: gdb.Value):
        return InvalidMemory(int(value))

    def _inspect_array(self, array_value: gdb.Value, nb_elements: int, in_heap: bool):
        log_insp(
            f"_inspect_array: address=0x{int(array_value.address):0x}, {nb_elements=}"
        )
        out_size = min(nb_elements, MAX_ARRAY_VALUE_ELTS)
        return self._build_value(
            gdb_value=array_value,
            content=[self._inspect_array_elem(array_value, i) for i in range(out_size)],
            abstract_type=AbstractType.LIST,
            location=Location.HEAP if in_heap else None,
        )

    def _inspect_struct(self, struct_value: gdb.Value, fields: list[str]):
        dict_content = {
            field_name: self._inspect_value(struct_value[field_name])
            for field_name in fields
        }
        log_insp("inspect struct with field %s", dict_content.keys())

        return self._build_value(struct_value, dict_content, AbstractType.STRUCT)

    def _build_variable(
        self, variable_name, processed_value, function_name=None, depth=None
    ) -> Variable:
        return Variable(
            variable_name,
            value=processed_value,
            function_name=function_name,
            depth=depth,
        )

    def inspect_frame_variable(
        self, frame: gdb.Frame, variable_name: str
    ) -> Optional[Variable]:
        """Build a shadow memory from the stack variable.
        If no frame is provided we take the newest frame and we assume this is a global symbol
        """
        log_insp("Inspection frame variable %s", variable_name)
        log_insp_time(variable_name)
        try:
            value = frame.read_var(variable_name)
        except ValueError:
            log_insp("Variable %s not found in frame %s", variable_name, frame.name())
            return None

        processed_value = self._inspect_value(value)

        function_name = frame.name()
        depth = frame_depth(frame)

        log_insp_time(variable_name)
        return self._build_variable(
            variable_name, processed_value, function_name, depth
        )

    def inspect_symbol(self, symbol: gdb.Symbol) -> Optional[Variable]:
        """Inspects a global symbol obtained from a Frame"""
        log_insp("Inspection global symbol %s", symbol.name)

        if symbol.is_function:
            log_insp("Symbol is a function")
            return None

        if symbol.name in ["stdin", "stdout", "stderr"]:
            log_insp("Skipping symbol %s", symbol)
            return None

        log_insp_time(symbol.name)
        try:
            processed_value = self._inspect_value(symbol.value())
        except TypeError:
            log_insp("Symbol don't have a value")
            # has to finish timing
            log_insp_time(symbol.name)

            return None
        log_insp_time(symbol.name)

        return self._build_variable(symbol.name, processed_value)


class GetMemoryCommand(gdb.Command):
    """A command to return the program stack preprocessed for easytracker output"""

    def __init__(self):
        super().__init__("get-memory", gdb.COMMAND_NONE)
        self.inspector = None

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        self.dont_repeat()
        self.inspector = ModelInspector()

        # get backtrace output for variable list
        try:
            output = gdb.execute("bt full", True, True)
        except gdb.error:
            transfer_memory_to_main_process(None)
            return
        backtrace_output = output.split("#")[1:]

        main_frame = get_main_frame()

        global_variables = self.get_global_variables(main_frame)
        stack = self.get_stack(main_frame, backtrace_output)

        transfer_memory_to_main_process(
            {"global_variables": global_variables, "stack": stack}
        )

    def get_stack(self, main_frame, backtrace_output):
        log_insp("get_stack: %s", main_frame)

        gdb_frame = main_frame
        parent = None
        frame_obj = None
        stack = []

        # we start from the main frame to end at the innermost frame
        for depth, frame_content in enumerate(reversed(backtrace_output)):
            name = FUNC_NAME_RE.findall(frame_content)[0]
            variable_names = VAR_NAME_RE.findall(frame_content)

            vars_dict = self.get_frame_variables(gdb_frame, variable_names)

            # build the final frame object and go to the next deeper frame in the stack
            frame_obj = Frame(
                name=name,
                depth=depth,
                next_source_file=gdb_frame.find_sal().symtab.fullname(),
                next_lineno=gdb_frame.find_sal().line,
                variables=vars_dict,
                parent=parent,
            )

            # update parent, stack and new frame
            stack.append(frame_obj)
            parent = frame_obj
            gdb_frame = gdb_frame.newer()

        # stack[0] is the main frame and stack[-1] the current frame
        return stack

    def get_frame_variables(self, gdb_frame: gdb.Frame, variable_names: list[str]):
        log_insp("get frame variables: %s", variable_names)

        variables = [
            self.inspector.inspect_frame_variable(gdb_frame, variable_name)
            for variable_name in variable_names
        ]

        vars_dict = {v.name: v for v in variables if v is not None}
        return vars_dict

    def get_global_variables(self, main_frame):
        if self.inspector is None:
            return None

        log_insp("get_global_variables in frame: %s", main_frame)

        try:
            # Probably missing debug information, assume no globals
            main_frame_block = main_frame.block()
        except RuntimeError:
            return {}

        global_variables = []
        # global variables
        for symbol in main_frame_block.global_block:
            global_variables.append(self.inspector.inspect_symbol(symbol))
        # static variables
        for symbol in main_frame_block.static_block:
            global_variables.append(self.inspector.inspect_symbol(symbol))

        return {v.name: v for v in global_variables if v is not None}


class GetVariableCommand(gdb.Command):
    """A command to access the value of a single variable in the current function_name (can be static or global)"""

    def __init__(self):
        super().__init__("get-variable", gdb.COMMAND_NONE)

    # pylint: disable=missing-function-docstring
    # pylint: disable=unused-argument
    def invoke(self, arg, from_tty):
        try:
            self.dont_repeat()
            args = gdb.string_to_argv(arg)
            if len(args) != 1:
                raise gdb.GdbError("get-variable takes one argument")
            var_name = args[0]
            inspector = ModelInspector()

            # look in the current frame
            inspection = inspector.inspect_frame_variable(gdb.newest_frame(), var_name)
            transfer_memory_to_main_process(inspection)
        # pylint: disable=broad-except
        except Exception as exc:
            log_insp("INSPECTION: %s", exc)
            log_insp(traceback.format_exc())
            print(exc)
            traceback.print_exc()


GetMemoryCommand()
GetVariableCommand()
