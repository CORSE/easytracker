"""This module defines the `IoManager` class
which manages I/O for file objects connected to an existing gdb process
or pty.
"""
import logging
import select
import time
import queue
import threading
from typing import IO, Any, Dict, List, Optional, Tuple, Union

__all__ = ["IoManager"]


logger = logging.getLogger(__name__)


def write_thread(stdin, stream_name, in_queue):
    while True:
        command = in_queue.get()
        # if we put a None in the command queue the thread needs to stop
        if command is None:
            break
        if not command.endswith("\n"):
            command += "\n"
        stdin.write(command)  # type: ignore
    # We close the stream, it will in turn terminate the underlying process
    stdin.close()


def read_thread(stream, stream_name, out_queue):
    while True:
        line = stream.readline()
        if line == "":
            break
        out_queue.put_nowait((stream_name, line))


class IoManager:
    def __init__(
        self,
        stdin: IO[str],
        stdout: IO[str],
        stderr: Optional[IO[str]],
    ) -> None:
        """
        Manage I/O for file objects created before calling this class
        This can be useful if the gdb process is managed elsewhere, or if a
        pty is used.
        """

        self.in_queue: queue.Queue = queue.Queue()
        self.out_queue: queue.Queue = queue.Queue()

        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr

        self.write_thread = threading.Thread(
            target=write_thread,
            name="IoManager-stdin",
            args=(self.stdin, "stdin", self.in_queue),
            daemon=True,
        )

        self.read_stdout_thread = threading.Thread(
            target=read_thread,
            name="IoManager-stdout",
            args=(self.stdout, "stdout", self.out_queue),
            daemon=True,
        )
        self.read_stderr_thread = (
            threading.Thread(
                target=read_thread,
                name="IoManager-stderr",
                args=(self.stderr, "stderr", self.out_queue),
                daemon=True,
            )
            if self.stderr is not None
            else None
        )

        self.write_thread.start()
        self.read_stdout_thread.start()
        if self.read_stderr_thread is not None:
            self.read_stderr_thread.start()
        self._terminated = False

    def read(self, block: bool = True, timeout: Optional[float] = None) -> tuple:
        """Read from the I/O stdout or stderr, returns a tuple (stream, bytes)
        or (None, None) when non blocking and empty."""
        try:
            res = self.out_queue.get(block, timeout)
        except queue.Empty:
            res = (None, None)
        return res

    def write(self, content: str) -> None:
        """Write to the I/O stdin stream."""
        self.in_queue.put_nowait(content)

    def terminate(self):
        """Terminate the manager and threads."""
        if not self._terminated:
            self._terminated = True
            self.in_queue.put_nowait(None)
            self.write_thread.join()
            self.read_stdout_thread.join()
            if self.stderr is not None:
                self.read_stderr_thread.join()

    def __del__(self):
        self.terminate()
