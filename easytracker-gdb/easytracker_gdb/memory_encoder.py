"""A module to encode python memory and transfer it between processes or through files"""

import pickle
import base64

DEBUG = 0


def log(message: str):
    """logs the given string in log.txt for debug"""
    if DEBUG:
        # pylint: disable=unspecified-encoding
        with open("logs.txt", "a") as log_f:
            print(message, file=log_f)


def encode(memory: object) -> str:
    """encodes the given bloc of memory into a string"""
    log(str(memory))

    pickled = pickle.dumps(memory)
    log(str(pickled))

    encoded = base64.b64encode(pickled)
    output = str(encoded)[2:-1]
    log(output)

    return output


def decode(code: str) -> object:
    """decode an encoded string to its original memory object"""
    log(f"output from gdb: {code}")

    if code.startswith("Python Exception"):
        raise ValueError(
            f"Python exception was raised in decode in easytracker: {code}"
        )

    try:
        b64_output = base64.b64decode(code)
        log(f"base64 decoded {b64_output!r}")

        python_obj = pickle.loads(b64_output)
        log(f"unpickled {python_obj}")
    except Exception as exc:
        raise ValueError(f"Cannot decode '{code}'") from exc

    return python_obj
