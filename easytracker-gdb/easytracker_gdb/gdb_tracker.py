"""
Tools that manipulate the pygdmi controller.
"""
import os
import tempfile
import re
import subprocess
import shutil
import typing
import queue
import signal
from typing import Optional, Union
from threading import Lock


from easytracker.abstract.abstract_tracker import AbstractEasyTracker
from easytracker.types.variable import (
    Variable,
    NameBinding,
    RawVariable,
)

from easytracker.types.pause_reason import PauseReason, PauseReasonType
from . import memory_encoder
from .utils import build_shared_lib

from .pygdbmi import gdbcontroller, gdbmiparser

DEBUG_MI = 0

lock = Lock()

CUR_DIR = os.path.dirname(__file__)
MEM_TRACK_DIR = os.path.join(CUR_DIR, "dynamic_memory_tracking")
LIB_TRACK_DIR = os.path.join(CUR_DIR, "dynamic_library_tracking")
IO_REDIRECT_DIR = os.path.join(CUR_DIR, "io_redirect")
CONTROL_PLUGIN_PATH = os.path.join(CUR_DIR, "control_plugin.py")
DYNAMIC_MEMORY_TRACKING_PLUGIN = os.path.join(MEM_TRACK_DIR, "bkpts_and_cmds.py")
DYNAMIC_LIBRARY_TRACKING_PLUGIN = os.path.join(LIB_TRACK_DIR, "bkpts_and_cmds.py")
INSPECT_PLUGIN_PATH = os.path.join(CUR_DIR, "inspect_plugin.py")

FUNC_NAME_RE = re.compile(r" (\w*)? \(")  # a word between " " and " ("

GDB_EXE = "gdb"
GDB_ARGUMENTS = ["--nx", "--quiet", "--interpreter=mi3"]
GDB_VERSION_MIN = (11, 2)
GDB_PYTHON_VERSION_MIN = (3, 9)


def log_mi(message: str, *args):
    """logs the given string in log.txt for debug"""
    if DEBUG_MI:
        log(message, *args)


def log(message: str, *args):
    """logs the given string in logs.txt for debug"""
    # pylint: disable=unspecified-encoding
    with open("logs.txt", "a") as log_f:
        print(message % args, file=log_f)


# Define strict Parsing for gdbmi, required for
# correctly managing stopped notifications
gdbmiparser.set_strict_parsing(True)


# pylint: disable=too-many-instance-attributes, too-many-public-methods
class GdbTracker(AbstractEasyTracker):
    """It loads our plugin and parse gdbmi output."""

    def __init__(self):
        super().__init__()

        self.stdout_queue = queue.Queue()
        self.fifo = None
        self.fifo_name = ""

        self._create_fifo()

        self._error_message: str = ""

        self._inferior_pid: int = 0

        self._inferior_running = False

        self._controller = None

        # Note that this may raise, hence we initialize all fields before this point
        self._check_gdb_compatibility(GDB_EXE)

        self._controller = gdbcontroller.GdbController([GDB_EXE] + GDB_ARGUMENTS)

        # load the python plugin
        self._internal_send_to_gdb("-gdb-set pagination off", raw=True)
        self._internal_send_to_gdb("-gdb-set can-use-hw-watchpoints 0", raw=True)

        malloc_wrapper_path = build_shared_lib(
            "libmalloc.so", ["libmalloc.c"], MEM_TRACK_DIR
        )
        io_buffering_path = build_shared_lib(
            "lib_io_buffering.so", ["lib_io_buffering.c"], IO_REDIRECT_DIR
        )
        dlopen_wrapper_path = build_shared_lib(
            "libdlopen.so", ["libdlopen.c"], LIB_TRACK_DIR
        )
        self._internal_send_to_gdb(
            f"-gdb-set environment LD_PRELOAD {malloc_wrapper_path}:{io_buffering_path}:{dlopen_wrapper_path}",
            raw=True,
        )

        # load the gdb plugins
        self._internal_send_to_gdb(f"source {CONTROL_PLUGIN_PATH}")
        self._internal_send_to_gdb(f"source {DYNAMIC_MEMORY_TRACKING_PLUGIN}")
        self._internal_send_to_gdb(f"source {DYNAMIC_LIBRARY_TRACKING_PLUGIN}")
        self._internal_send_to_gdb(f"source {INSPECT_PLUGIN_PATH}")

    # ============ Interface definition
    def load_program(self, prog_file_path: str):
        self.send_direct_command("directory")
        self.send_direct_command(f"file {prog_file_path}")

    def force_program_stop(self):
        if self._inferior_pid != 0:
            # send sigkill to the inferior
            subprocess.call(["kill", "-INT", str(self._inferior_pid)])

    # === Program control
    def next(self, count: int = 1) -> PauseReason:
        output = self.send_direct_command(f"next {count}")
        if self._has_to_finish_execution():
            return self.resume()
        return output

    def step(self, count: int = 1) -> PauseReason:
        # if we end up in malloc, realloc or free, we finish the function
        output = self.send_direct_command(f"step {count}")
        if (
            self.get_current_function_name()
            in [
                "malloc",
                "calloc",
                "realloc",
                "free",
            ]
            or self._has_to_finish_execution()
        ):
            self.send_direct_command("finish")
        return output

    def start(self, in_file: str = None) -> PauseReason:
        cmd = "start"
        if in_file:
            cmd += " < " + in_file
        output = self.send_direct_command(cmd)

        return output

    def resume(self) -> PauseReason:
        return self.send_direct_command("continue")

    def break_before_line(self, lineno: int, maxdepth: Optional[int] = None):
        bp_num = super().break_before_line(lineno, maxdepth)
        self._internal_send_to_gdb(f"custom-break {bp_num} {lineno} {maxdepth}")

        return bp_num

    def break_before_func(self, funcname: str, maxdepth: Optional[int] = None):
        bp_num = super().break_before_func(funcname, maxdepth)
        self._internal_send_to_gdb(f"custom-break {bp_num} {funcname} {maxdepth}")

        return bp_num

    def break_end_of_func(self, funcname, maxdepth: Optional[int] = None):
        """Adds a breakpoint at end of the given function"""
        bp_num = super().break_end_of_func(funcname, maxdepth)
        maxdepth_str = str(maxdepth)
        self._internal_send_to_gdb(
            f"track-function {bp_num} {funcname} {maxdepth_str} True"
        )
        return bp_num

    def add_condition(self, bp_num: int, condition: str):
        """Adds the given condition on the given break point"""
        # self._internal_send_to_gdb(f"cond {bp_num} {condition}")
        #
        real_bp = self.get_real_breakpoint_number(bp_num)
        output = self.send_direct_command(f"cond {real_bp} {condition}")
        return output

    def send_signal(self, signal_input):
        """Sends signal input number to gdb subprocess"""
        self._controller.send_signal_to_gdb(signal_input)

    def get_real_breakpoint_number(self, virtual_bp_num: int):
        """Returns the underlying breakpoint number from a virtual bp number"""
        output = self._internal_send_to_gdb(f"get-real-bp-num {virtual_bp_num}")
        return int(output)

    def watch(self, variable: Union[NameBinding, str]):
        """Add a new watchpoint on the given variable with priority"""
        bp_num = super().watch(variable)
        if isinstance(variable, str):
            variable = NameBinding(variable)
        if variable.function_name is None:
            self._internal_send_to_gdb(f"custom-watch {bp_num} {variable.name}")
        else:
            self._internal_send_to_gdb(
                f"custom-watch {bp_num} {variable.function_name}:{variable.name}"
            )
        return bp_num

    def delete_breakpoint(self, bp_num):
        super().delete_breakpoint(bp_num)
        # delete breakpoint inside GDB
        self._internal_send_to_gdb(f"custom-delete {bp_num}")

    def track_function(self, funcname: str, maxdepth: Optional[int] = None):
        bp_num = super().track_function(funcname, maxdepth)

        maxdepth_str = str(maxdepth) if maxdepth is not None else ""
        self._internal_send_to_gdb(f"track-function {bp_num} {funcname} {maxdepth_str}")

        return bp_num

    # === Program state
    @typing.no_type_check
    def get_program_memory(self, as_raw_python_objects: bool = False):
        """Returns the program memory : stack and globals"""
        # GDB handles the computation of the correct return type
        output = self._internal_send_to_gdb("get-memory")

        decoded_output = memory_encoder.decode(output)
        seen = {}
        if as_raw_python_objects:
            raw_frame = super()._as_raw_frame(decoded_output["stack"][-1], seen)
            raw_stack = []
            current_frame = raw_frame
            while current_frame is not None:
                raw_stack.append(current_frame)
                current_frame = current_frame.parent
            decoded_output["stack"] = list(reversed(raw_stack))
            decoded_output["global_variables"] = {
                vn: super()._as_raw_variable(v, seen)
                for vn, v in decoded_output["global_variables"].items()
            }
        return decoded_output

    # TODO finish refactor with interface instead of this ugly placeholders
    def get_current_frame(self, as_raw_python_objects: bool = False):
        """Returns the current frame"""
        return self.get_program_memory(as_raw_python_objects)["stack"][-1]

    def get_global_variables(self, as_raw_python_objects: bool = False):
        """Returns the global variables"""
        return self.get_program_memory(as_raw_python_objects)["global_variables"]

    def get_variable_value(
        self, name: str, as_raw_python_objects: bool = False
    ) -> Union[Variable, RawVariable, None]:
        memory = self.get_program_memory(as_raw_python_objects)
        for frame in reversed(memory["stack"]):
            if name in frame.variables:
                return frame.variables[name]
        return memory["global_variables"].get(name)

    def get_variable_value_as_str(
        self, name: str, type_name: str = None
    ) -> Union[str, int]:
        """Returns the variable value as a string or an int"""
        if type_name is None:
            output = self._internal_send_to_gdb(f"print {name}")
        else:
            output = self._internal_send_to_gdb(f"print (({type_name}) {name})")
        try:
            if type_name == "char":
                return output.split("'")[-2]
            if type_name == "char*":
                try:
                    return output.split('"')[-2]
                except IndexError:
                    return ""
            if type_name == "int":
                return int(output.split("= ")[1])
            return output

        except IndexError as idxe:
            print(f"Cannot find '{type_name}' value in '{output}'")
            raise idxe

    def is_running(self):
        """Returns wether or not the inferior is running"""
        return self._inferior_running

    # === Tracker specific stuff is accessed
    # through the following method.
    def send_direct_command(
        self, command: str, raise_exception_on_error: bool = False
    ) -> PauseReason:
        """sends a command to gdb and adds the console output to the console output stream"""
        command_name = command.split()[0]

        if command_name in ["start", "run", "r"]:
            command += f" < {self.fifo_name}"

        self._pause_reason = PauseReason(PauseReasonType.UNKNOWN)
        self.stdout_queue.put_nowait(("GDB", self._internal_send_to_gdb(command)))

        if raise_exception_on_error and self._error_message != "":
            raise RuntimeError(self._error_message)

        # manually search for new program loaded in gdb
        if command_name == "file":
            pass
            # self._place_internal_function_breakpoints()
        # exception for start to place breakpoints
        elif command_name == "start":
            self._internal_send_to_gdb("place-dynamic-library-wrappers")
            self._internal_send_to_gdb("place-dynamic-allocation-wrappers")
            self._internal_send_to_gdb("print-allocated-blocks --delete")
            self._pause_reason = PauseReason(PauseReasonType.START)

        return self._pause_reason

    def send_to_inferior_stdin(self, msg: str, append_new_line: bool = True):
        """Writes on the inferior stdin and not in GDB stdin.
        If the inferior does not consume the input it stays in the fifo.
        Note from branch 'unstable_refactor':
        You can clear it with the clear_fifo function.
        """
        if append_new_line:
            msg += "\n"
        self.fifo.write(msg)
        self.fifo.flush()

    def get_gdb_console_output(self) -> str:
        """Gets the current console output stream and flushes it"""
        output = ""
        # read all the elements of the queue
        while True:
            try:
                res = self.stdout_queue.get_nowait()
                output += res[1]
            except queue.Empty:
                break

        return output

    def get_allocated_blocks(self) -> dict[str, int]:
        """Returns a dictionary of allocated blocks maintained by the tracker"""
        code = self._internal_send_to_gdb("print-allocated-blocks")
        return memory_encoder.decode(code)  # type: ignore

    def get_current_function_name(self) -> Optional[str]:
        """Triggers a backtrace command and returns the current function name"""
        output = self._internal_send_to_gdb("bt")
        try:
            current_frame = output.split("#")[1]

            return FUNC_NAME_RE.findall(current_frame)[0]
        except IndexError:
            # list index out of range because we have no stack
            return None

    # ============ Internal methods
    def _has_to_finish_execution(self) -> bool:
        """Checks if the main frame is present in the stack.
        If not, finish the function"""
        if self._pause_reason.type == PauseReasonType.EXITED:
            return False
        output = self._internal_send_to_gdb("bt")
        try:
            output.split("#")[1]
        except IndexError:
            # list index out of range because we have no stack
            return True

        frames = FUNC_NAME_RE.findall(output)
        # log(f"frame names: {frames}")

        return "main" not in frames

    def _place_internal_function_breakpoints(self):
        """Place silent breakpoints for function enter and exit"""
        function_names = self._list_functions()
        func_args = " ".join(function_names)
        self._internal_send_to_gdb(f"internal-track {func_args}")

    def _list_functions(self) -> list[str]:
        """Lists all available functions in the program"""
        output = self._internal_send_to_gdb("info functions -n -q")
        function_names = FUNC_NAME_RE.findall(output)

        return function_names

    def _function_called_handler(self, message: str) -> None:
        """internal handler when a function is called"""

    def _function_returned_handler(self, message: str) -> None:
        """internal handler when a function returns"""

    def _parse_stoppped_payload(self, payload: list[tuple]) -> list[dict]:
        # We tansform list of (key, val) to a list of dict for each
        # encountered reason.
        reasons = []
        for key, val in payload:
            if key == "reason":
                reasons.append({"reason": val})
            else:
                assert len(reasons) >= 1
                reasons[-1].update({key: val})
        return reasons

    def _parse_gdb_response(self, message: dict) -> dict:
        # console output, gdb log, command result, inferior output, done
        if message["type"] in [
            "console",
            "log",
            "result",
            "output",
            "done",
        ] and isinstance(message["payload"], list):
            # The payload is a dict
            message["payload"] = dict(message["payload"])
        # notification
        elif message["type"] == "notify":
            if message["message"] == "stopped":
                # for stoppped message we have a reason list
                message["payload"] = {
                    "reasons": self._parse_stoppped_payload(message["payload"])
                }
            else:
                # Otherwise the payload is a dict
                message["payload"] = dict(message["payload"])
        log_mi("Parsed response: %s", message)
        return message

    # pylint: disable=too-many-branches
    def _internal_send_to_gdb(self, command: str, raw: bool = False) -> str:
        """Write the given command to subprocess GDB stdin.
        Returns the console output"""
        log_mi(f"GDB >> {command}")
        mi_command = command
        if not raw:
            mi_command = f'-interpreter-exec console "{command}"'

        # send command to gdb
        self._controller.write(mi_command)

        console_output = ""
        self._error_message = ""

        while True:
            message = self._controller.wait_gdb_response()
            log_mi("Raw pygdbmi response: %s", message)
            message = self._parse_gdb_response(message)

            # console output
            if message["type"] in ["console", "log"]:
                content = message["payload"]
                if "<CALL>" in console_output:
                    self._function_called_handler(content)
                elif "<RETURN>" in console_output:
                    self._function_returned_handler(content)
                else:
                    console_output += content

            # check result for error and running
            elif message["type"] == "result":
                self._parse_result_message(message)

                # manually check end of command
                if message["message"] in ["done", "error"]:
                    break

            # notifications (also check for stopping)
            elif message["type"] == "notify":
                self._parse_notification_message(message)

                # manually check program stop meaning command end
                if message["message"] == "stopped":
                    self._inferior_running = False
                    break

            # inferior stdout
            elif message["type"] == "output":
                self._parse_inferior_output(message)

            # ignore done tokens that are generated from gdb prompt
            elif message["type"] == "done":
                pass
            else:
                log_mi(f'Unknown MI message type {message["type"]}')

        log_mi(str(self._pause_reason))

        # evaluate escaped characters
        console_output = console_output.encode().decode("unicode_escape")
        return console_output

    def _start_run(self):
        """Starts "run" mode of easytracker"""
        self._inferior_running = True

        self._exit_code = None

    def _parse_inferior_output(self, message: dict):
        """Parse the inferior output in stdout or stderr"""
        if message["payload"] != "":
            self.stdout_queue.put_nowait((message["stream"], message["payload"]))

    def _parse_result_message(self, message: dict):
        """parse the result message and returns if the command should keep listening"""
        result = message["message"]
        # simple command end
        # catch error
        if result == "error":
            self._error_message = message["payload"]["msg"]
        # the program resumed running
        if result == "running":
            self._start_run()

    def _parse_notification_message(self, message: dict) -> None:
        """parse a notification message and returns if the command should keep listening"""
        notification = message["message"]
        # fetch exit code
        if notification == "thread-group-exited":
            self._update_exit_code(message)
        elif notification == "thread-group-started":
            self._inferior_pid = int(message["payload"]["pid"])
        elif notification == "stopped":
            # update pause reasons
            payload = message["payload"]["reasons"]
            self._update_pause_reasons(payload)

    def _update_line_from_reason(self, payload: dict) -> Optional[int]:
        """Update current line and file from reason payload"""
        try:
            self._next_lineno = int(payload["frame"]["line"])
            self._last_source_file = self._next_source_file
            self._next_source_file = payload["frame"]["file"]
        except KeyError:
            self._next_lineno = None
            self._next_source_file = None
        return self._next_lineno

    def _update_pause_reasons(self, payload: list[dict]) -> None:
        """Update the internal pause reason according to the 'reasons' field"""
        prev_line = self._next_lineno
        reasons = [
            r
            for r in [
                self._parse_single_pause_reason(reason["reason"], reason)
                for reason in payload
            ]
            if r is not None
        ]
        if len(reasons) == 0:
            self._error_message = "Unable to determine stopped reason"
            raise RuntimeError(self._error_message)
        self._pause_reason = min(reasons, key=lambda pause_reason: pause_reason.type)
        if self._next_lineno:
            self._last_lineno = prev_line

    def _get_gdb_bp_info(self, gdb_bp_num) -> typing.Tuple[str, int]:
        """Sends a command to GDB to get the virtual breakpoint number from the internal bp number.
        Returns -1 if the bp number is not found"""
        try:
            res = self._internal_send_to_gdb(f"get-virtual-bp-num {gdb_bp_num}")
            bp_type, bp_num = res.split(":")
        except ValueError as vale:
            print(f"Error splitting '{res}': too many values")
            raise vale

        return bp_type, int(bp_num)

    def _get_bp_return_value(self, virtual_num: int) -> object:
        """Sends a command to GDB to get the return value for the currently caught TrackBP.
        Returns None if not defined or if not a primitive type."""
        output = self._internal_send_to_gdb(
            f"get-virtual-bp-attrs {virtual_num} return_value"
        )
        decoded_output = memory_encoder.decode(output)
        return decoded_output.get("return_value")  # type: ignore

    def _parse_single_pause_reason(
        self, reason_string, payload: dict
    ) -> Optional[PauseReason]:
        """Parse a single pause reason string and returns the according object"""
        line = self._update_line_from_reason(payload)
        if reason_string == "breakpoint-hit":
            bp_num = payload["bkptno"]
            bp_type, virtual_num = self._get_gdb_bp_info(bp_num)
            if bp_type == "break":
                pause_reason = PauseReason(
                    PauseReasonType.BREAKPOINT, line, [virtual_num]
                )
            elif bp_type == "call":
                func_name = self.get_current_function_name()
                pause_reason = PauseReason(
                    PauseReasonType.CALL, line, [virtual_num, func_name]
                )
            elif bp_type == "return":
                func_name = self.get_current_function_name()
                return_value = self._get_bp_return_value(virtual_num)
                pause_reason = PauseReason(
                    PauseReasonType.RETURN, line, [virtual_num, func_name, return_value]
                )
            else:
                raise ValueError(f"Unknown breakpoint type {bp_type}")
        elif reason_string == "watchpoint-trigger":
            wpt_info = payload["wpt"]
            value = payload["value"]
            pause_reason = PauseReason(
                PauseReasonType.WATCHPOINT,
                line,
                [
                    self._get_gdb_bp_info(wpt_info["number"])[
                        1
                    ],  # gets only the bp number
                    wpt_info["exp"],
                    value["old"],
                    value["new"],
                ],
            )
        elif reason_string == "watchpoint-scope":
            # ignore watchpoint-scope
            pause_reason = None
        elif reason_string == "function-finished":
            pause_reason = PauseReason(PauseReasonType.FUNCTION_FINISHED, line)
        elif reason_string in ["exited-normally", "exited"]:
            pause_reason = PauseReason(PauseReasonType.EXITED, None, [self._exit_code])
        elif reason_string == "end-stepping-range":
            pause_reason = PauseReason(PauseReasonType.ENDSTEPPING_RANGE, line)
        elif reason_string in ["signal-received", "exited-signalled"]:
            sig_name = payload.get("signal-name", "")
            try:
                exit_code = -signal.Signals[sig_name].value
            except KeyError:
                exit_code = -1
            pause_reason = PauseReason(
                PauseReasonType.SIGNAL,
                None,
                [exit_code, sig_name, payload.get("signal-meaning", "")],
            )
        else:
            raise ValueError(f"Unknown pause reason type {reason_string}")

        return pause_reason

    def _update_exit_code(self, message: dict):
        """Update exit code state from a message with thread-group-exited' notification"""
        self._inferior_pid = 0
        try:
            # GDB MI returns exit code in octal base
            self._exit_code = int(message["payload"]["exit-code"], 8)
        except KeyError:
            # no exit is found means than the program just restarted
            self._exit_code = None

    def _create_fifo(self, name=None):
        if self.fifo is not None:
            return

        tmpdir = tempfile.mkdtemp(prefix="agdb_")
        name = os.path.join(tmpdir, "inferior_stdin_fifo")

        try:
            os.mkfifo(name)
        except OSError as ose:
            print(f"Failed to create FIFO: {ose}")
            raise ose

        read_fd = os.open(name, os.O_RDONLY | os.O_NONBLOCK)
        read_file = os.fdopen(read_fd)

        write_fd = os.open(name, os.O_WRONLY | os.O_NONBLOCK)
        write_file = os.fdopen(write_fd, "w")

        read_file.close()

        self.fifo = write_file
        self.fifo_dir = tmpdir
        self.fifo_name = name

    def _delete_fifo(self):
        with lock:
            if self.fifo is not None:
                self.fifo.close()
                self.fifo = None
                os.remove(self.fifo_name)
                os.rmdir(self.fifo_dir)

    def terminate(self):
        self._delete_fifo()
        if self._controller is not None:
            self._controller.exit()
        self.stdout_queue.put_nowait(None)

    def __del__(self):
        self.terminate()

    @classmethod
    def _check_gdb_executable(cls, gdb_exe: str) -> bool:
        gdb_path = shutil.which(gdb_exe)
        return gdb_path is not None

    @classmethod
    def _get_gdb_version(cls, gdb_exe: str) -> tuple[int, ...]:
        proc = subprocess.run(
            [gdb_exe, "--version"], text=True, capture_output=True, check=False
        )
        if proc.returncode != 0 or proc.stdout == "":
            raise RuntimeError(
                f"gdb executable issue, can't execute command: {gdb_exe} --version:\n{proc.stderr}"
            )
        version = None
        version_str = proc.stdout.splitlines()
        if len(version_str) > 0:
            version_str = version_str[0].split(" ")
            if len(version_str) > 0:
                try:
                    version = tuple(int(x) for x in version_str[-1].split("."))
                except ValueError:
                    pass
        if version is None or len(version) == 0:
            raise RuntimeError(
                f"gdb executable issue, can't infer version from: {gdb_exe} --version"
            )
        return version

    @classmethod
    def _get_gdb_python_version(cls, gdb_exe: str) -> tuple[int, ...]:
        py_version = None
        with tempfile.NamedTemporaryFile("w", suffix=".py", delete=False) as ftmp:
            ftmp.write(
                "import sys\nprint('.'.join([str(x) for x in sys.version_info[:3]]))\n"
            )
            ftmp.close()
            try:
                proc = subprocess.run(
                    [
                        gdb_exe,
                        "--nx",
                        "--batch",
                        "--quiet",
                        "-ex",
                        f"source {ftmp.name}",
                    ],
                    text=True,
                    capture_output=True,
                    check=False,
                )
                if proc.returncode != 0 or proc.stdout == "":
                    raise RuntimeError(
                        f"gdb issue, can't execute python plugin script, "
                        f"please ensure gdb python plugins are active\n{proc.stderr}"
                    )
            finally:
                os.unlink(ftmp.name)
            py_version_str = proc.stdout.splitlines()
            if len(py_version_str) > 0:
                try:
                    py_version = tuple(int(x) for x in py_version_str[0].split("."))
                except ValueError:
                    pass
        if py_version is None or len(py_version) == 0:
            raise RuntimeError(
                "gdb executable issue, can't infer python version from plugin script, "
                "please ensure gdb python plugins are active"
            )
        return py_version

    @classmethod
    def _check_gdb_compatibility(cls, gdb_exe: str) -> None:
        if not cls._check_gdb_executable(gdb_exe):
            raise RuntimeError(
                f"gdb executable ({gdb_exe}) not found in PATH, please install gdb first"
            )
        version = cls._get_gdb_version(gdb_exe)
        if version[: len(GDB_VERSION_MIN)] < GDB_VERSION_MIN:
            version_str = ".".join([str(x) for x in version])
            version_min = ".".join([str(x) for x in GDB_VERSION_MIN])
            raise RuntimeError(
                f"gdb current version is {version_str}, easytracker-gdb requires at least {version_min},"
                f"please install a more recent gdb version in PATH"
            )
        py_version = cls._get_gdb_python_version(gdb_exe)
        if py_version[: len(GDB_PYTHON_VERSION_MIN)] < GDB_PYTHON_VERSION_MIN:
            py_version_str = ".".join([str(x) for x in py_version])
            py_version_min = ".".join([str(x) for x in GDB_PYTHON_VERSION_MIN])
            raise RuntimeError(
                f"gdb python plugin current version is python {py_version_str}, "
                f"easytracker-gdb requires at least python {py_version_min}, "
                f"please install a more recent gdb python plugin version"
            )
