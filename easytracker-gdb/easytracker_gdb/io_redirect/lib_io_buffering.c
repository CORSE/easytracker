/*
 * Initialized default inferior standard streams buffering
 * - Keep stderr default, i.e. unbuffered,
 * - Force stdout to be line buffered,
 * - Keep stdin default, i.e. fuly buffered.
 */

#include <stdio.h>

static char _stdout_buffer[BUFSIZ];
static char _stdin_buffer[BUFSIZ];

static void __attribute__((constructor)) _set_stdio_buffering() {
    setvbuf(stdin, _stdin_buffer, _IOFBF, sizeof(_stdin_buffer));
    setvbuf(stdout, _stdout_buffer, _IOLBF, sizeof(_stdout_buffer));
    setvbuf(stderr, NULL, _IONBF, 0);
}
