EasyTracker GDB Tracker
=======================

Install with:

    pip install --extra-index-url https://gitlab.inria.fr/api/v4/groups/corse/-/packages/pypi/simple easytracker-gdb


For inplace development, install with:

    pip install -e '.[dev]'

