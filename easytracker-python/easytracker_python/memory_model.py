"""Module to convert python name bindings to Variable and Value"""

from types import ModuleType
from typing import Union

from easytracker.types.variable import (
    Variable,
    RawVariable,
    Value,
    Location,
    AbstractType,
)


def create_raw_variable(name: str, value: object, **kwargs) -> RawVariable:
    """Creates a raw variable with the direct python object as value"""
    return RawVariable(name, value=value, **kwargs)


def create_variable(
    name: str, value: object, in_stack: bool, seen: dict[int, Value], **kwargs
) -> Variable:
    """Creates a Variable object from a name and a value"""
    return Variable(
        name,
        value=_convert_python_to_value(value, in_stack=in_stack, seen=seen),
        **kwargs,
    )


def create_variable_option(
    name: str,
    value: object,
    as_raw_python_objects: bool,
    in_stack: bool,
    seen: dict[int, Value] = None,
    **kwargs,
) -> Union[Variable, RawVariable]:
    """Looks at the given option to know which type of variable to create"""
    if as_raw_python_objects:
        return create_raw_variable(name, value, **kwargs)
    if seen is None:
        seen = {}
    return create_variable(name, value, in_stack, seen, **kwargs)


def _encapsulate_in_ref_value(value: Value, in_stack: bool) -> Value:
    """Encapsulates the given value inside a REF"""
    location = Location.STACK if in_stack else Location.HEAP
    return Value(
        location,
        address=None,
        language_type=value.language_type,
        abstract_type=AbstractType.REF,
        content=value,  # (value.address, value),  # type: ignore
    )


# pylint: disable=too-many-branches
def _convert_python_to_value(
    python_value: object, in_stack: bool, seen: dict[int, Value]
) -> Value:
    """Takes a Python object and decorate it to become a Value"""
    address = id(python_value)
    # Check cycle in refs
    if address in seen:
        return _encapsulate_in_ref_value(seen[address], in_stack)

    content: object = None
    language_type = str(type(python_value))
    if isinstance(python_value, (list, tuple)):
        type_enum = AbstractType.LIST
        content = None
    elif python_value is None:
        type_enum = AbstractType.NONE
        content = None
    elif isinstance(python_value, (int, str, float)):
        type_enum = AbstractType.PRIMITIVE
        content = python_value
    elif type(python_value).__name__ == "function":
        type_enum = AbstractType.FUNCTION
        content = python_value.__name__  # type: ignore
    elif isinstance(python_value, dict):
        type_enum = AbstractType.DICT
        content = None
    elif isinstance(python_value, ModuleType):
        type_enum = AbstractType.UNKNOWN
        content = repr(python_value)
    else:
        try:
            vars(python_value)
            type_enum = AbstractType.STRUCT
            content = None
        except TypeError:
            type_enum = AbstractType.UNKNOWN
            content = repr(python_value)

    value = Value(
        Location.HEAP,
        address=address,
        language_type=language_type,
        abstract_type=type_enum,
        content=content,  # type: ignore
    )
    # store the reference to the value for cycle detection
    seen[address] = value

    # Force the update of the content after creation to avoid
    # infinite recursion for circular data structures
    # Use the default setattr method to bypass the frozen class own setattr
    if isinstance(python_value, (list, tuple)):
        values = tuple(
            _convert_python_to_value(e, in_stack=False, seen=seen) for e in python_value
        )
        object.__setattr__(value, "content", values)
    elif isinstance(python_value, dict):
        dict_values = {
            _convert_python_to_value(
                k, in_stack=False, seen=seen
            ): _convert_python_to_value(v, in_stack=False, seen=seen)
            for k, v in python_value.items()
        }
        object.__setattr__(value, "content", dict_values)
    elif value.abstract_type == AbstractType.STRUCT:
        attr_values = {
            key: _convert_python_to_value(val, in_stack=False, seen=seen)
            for key, val in vars(python_value).items()
        }
        object.__setattr__(value, "content", attr_values)

    return _encapsulate_in_ref_value(value, in_stack)
