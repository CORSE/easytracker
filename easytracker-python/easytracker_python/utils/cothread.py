"""
Implementation of a co-routine as a thread

Basically works as below:
- inherits the Thread() interface
- main is the main thread (i.e. a thread starting the CoThread())
- cothread is the thread created by the start() call
- main and cothread never execute at the same time

An example flow of control is given below:

main                             cothread
----                             --------
co = Cothread(...)
co.start()                       waiting # wait immediately on start
...                              waiting
r1 = co.yield_to_co(x)  --->     ...     # x not seen the first time
waiting                          ...
r1 is set to t          <---     c2 = co.yield_to_main(t)
...                              waiting
r2 = co.yield_to_co(y)  --->     c2 set to y
waiting                          ...
r2 is set to u          <---     c3 = co.yield_to_main(u)
...                              waiting
co.join()               --->     c3 set to None

"""

# pylint: disable=too-many-instance-attributes

import sys
from threading import Thread
from queue import Queue, Empty
import logging
from typing import Any, Callable, Optional

logger = logging.getLogger(__name__)


class CoThread(Thread):
    """Implementation of co-routine with a Thread"""

    def __init__(self, on_exit: Callable = None, **kwargs) -> None:
        target = kwargs.get("target", None)
        daemon = kwargs.get("daemon", None)
        kwargs["target"] = self.__start_in_thread
        kwargs["daemon"] = True if daemon is None else daemon
        super().__init__(**kwargs)
        self.__target = target
        self.__on_exit = on_exit
        self.__co_exited = False
        self.__co_joined = False
        self.__co_exception: Optional[Any] = None
        self.__val_co: Queue = Queue()
        self.__val_main: Queue = Queue()

    def __start_in_thread(self, *args, **kwargs) -> None:
        logger.debug("started co thread %s", self.name)
        self.__val_co.get()
        try:
            if self.__target is not None:
                self.__target(*args, **kwargs)
        except Exception as exc:  # pylint: disable=broad-exception-caught
            self.__co_exception = sys.exc_info()
            logger.debug("exception in co thread: %s", exc)
        finally:
            self.__co_exited = True
            logger.debug("exit of co thread")
            if self.__on_exit is not None:
                self.__on_exit()
            self.yield_to_main(to_main=None)

    def start(self) -> None:
        logger.debug("starting co thread")
        self.__co_joined = False
        self.__co_exited = False
        self.__co_exception = None
        while not self.__val_co.empty():
            self.__val_co.get()
        while not self.__val_main.empty():
            self.__val_main.get()
        super().start()

    def join(self, timeout: float = None) -> None:
        """Join the co thread, yield first a None if waiting"""
        logger.debug("joining co thread")
        if not self.__co_joined:
            self.__co_joined = True
            self.__val_co.put(None)
        super().join(timeout=timeout)

    def yield_to_main(self, to_main: Optional[Any] = None) -> Optional[Any]:
        """Block the cothread and pass execution and value to main"""
        logger.debug("yield to main: %s", to_main)
        self.__val_main.put(to_main)
        val_co = None
        if not self.__co_joined and not self.__co_exited:
            val_co = self.__val_co.get()
        logger.debug("exit yield to main: %s", val_co)
        return val_co

    def yield_to_co(self, to_co: Optional[Any] = None) -> Optional[Any]:
        """Block the main thread and pass execution and value to cothread"""
        logger.debug("yield to co: %s", to_co)
        self.__val_co.put(to_co)
        val_main = None
        while not self.__co_joined and not self.__co_exited:
            try:
                val_main = self.__val_main.get(timeout=0.1)
                break
            except Empty:
                pass
        logger.debug("exit yield to co: %s", val_main)
        return val_main

    @property
    def co_exited(self) -> bool:
        """Returns whether the cothread exited"""
        return self.__co_exited

    @property
    def co_exception(self) -> Optional[tuple[Any]]:
        """Returns the `exc_info` 3-tuple if cothread raised"""
        return self.__co_exception
