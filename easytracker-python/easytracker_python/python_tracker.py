"""THE python tracker."""

from __future__ import annotations

# Standard imports first
from contextlib import contextmanager
import os
import sys
import importlib.util
import copy
import logging
from typing import Union, Optional, cast
from enum import Enum
from types import ModuleType

from easytracker.types.pause_reason import PauseReason, PauseReasonType, DepthChange
from easytracker.abstract.abstract_tracker import AbstractEasyTracker
from easytracker.types.variable import Variable, NameBinding, RawVariable, Value
from easytracker.types.frame import Frame, RawFrame

# Own imports last
from .utils import CoThread
from .memory_model import (
    create_variable,
    create_raw_variable,
    create_variable_option,
)


class _ProgCtrlCmd(Enum):
    """Enum class to represent the program control commands."""

    START = 1
    NEXT = 2
    STEP = 3
    CONTINUE = 4
    CONTINUE_TO_VAR = 5
    CONTINUE_TO_PRIO = 6
    NONE = 99


def is_python_builtin(name: str) -> bool:
    """checks if the global symbol is a builtin from python or not"""
    return name.startswith("__") and name.endswith("__")


class _TrackerTerminate(Exception):
    """Internal Exception used for inferior early termination"""


# pylint: disable=too-many-instance-attributes
class PythonTracker(AbstractEasyTracker):
    """THE python tracker extends the abstract tracker."""

    def __init__(self):
        super().__init__()

        # The program under control
        self._inferior_file_path: Optional[str] = None
        self._inferior: CoThread = None

        # The dynamically loaded module
        # associated to self.file
        self._prog: Optional[ModuleType] = None

        # The last program control command
        # requested by the tool
        self._last_prog_ctrl_cmd: _ProgCtrlCmd = _ProgCtrlCmd.NONE
        self._next_depth: int = 0
        self._step_counter: int = 0

        self._last_depth_change: DepthChange = DepthChange.NONE
        self._next_depth_change: DepthChange = DepthChange.NONE

        # The program state
        self._current_depth: int = (
            -1
        )  # starts at -1 because always begin with call main
        self._trace_frame = None  # internal frame from python interpreter settrace
        self._has_to_pop_frame = False
        self._program_stack: list[RawFrame] = []

        # Per stack frame dictionary of watched variables.
        # The dictionary associates the variable name to its
        # value in the corresponding frame.
        #
        # self._watched_vars_function_names_stack[-1] always refers
        # to the current function_name.
        #
        # FIXME : what about 2 variables in different
        # function_names with the same name ?
        self._watched_vars_values_per_stack_frame: list[dict[str, object]] = []

        # Boolean set by the controller to request inferior termination on
        # force_program_exit()
        self._notify_terminate = False

        # Configure the generation of text traces
        # (replace logging.WARNING with logging.DEBUG
        # to enable traces)
        fmt = "[%(levelname)s] %(asctime)s - %(message)s"
        logging.basicConfig(level=logging.WARNING, format=fmt)

    # ========== Breakpoint and watchpoint management
    def _is_on_break_line(self) -> int:
        """Returns True if we reach a line breakpoint"""
        return self._is_on_break_internal(self._line_breakpoints, self._next_lineno)

    def _is_on_break_before_func(self) -> int:
        """Returns True if we reach a function breakpoint"""
        funcname = self._trace_frame.f_code.co_name
        return self._is_on_break_internal(self._before_function_breakpoints, funcname)

    def _is_on_break_internal(self, bp_dict, value) -> int:
        """Returns True if we reach a breakpoint on the given list"""
        if (value, self._next_source_file) in bp_dict:
            # check depth to see if we break
            hit_breakpoint = bp_dict[(value, self._next_source_file)]
            maxdepth = hit_breakpoint[1]
            # if no maxdepth is specified, break
            if maxdepth is None or self._current_depth <= maxdepth:
                return hit_breakpoint[0]
        # if the line is not in the list of breakpoints, don't break
        return -1

    def _is_entering_exiting_tracked_function(self, frame, event, arg) -> bool:
        """Check if we are on a call or return of watched function."""
        funcname = frame.f_code.co_name
        if funcname not in self._tracked_functions:
            return False
        bp_num = self._tracked_functions[funcname][0]
        max_depth = self._tracked_functions[funcname][1]
        if max_depth is None or max_depth >= self._current_depth:  # type: ignore
            bp_num = self._tracked_functions[funcname][0]
            bp_line = self._next_lineno
            if event == "call":
                self._pause_reason = PauseReason(
                    PauseReasonType.CALL,
                    line=bp_line,
                    args=[bp_num, funcname],
                )
                return True
            if event == "return":
                self._pause_reason = PauseReason(
                    PauseReasonType.RETURN, line=bp_line, args=[bp_num, funcname, arg]
                )
                return True
        return False

    def _get_watched_var_values_map(self) -> dict[NameBinding, RawVariable]:
        """
        Get values of watched variables from NameBindings in watchpoints.
        Note that when several bindings with the same var name match, only
        the deeper binding in the stack frame is preserved.
        """
        watched_var_values: dict[NameBinding, RawVariable] = {}
        for binding in self._watchpoints:
            maxup = self._get_binding_upward_max(binding)
            var, _ = self._get_upward_variable_value(
                binding.name, as_raw_python_objects=True, maxup=maxup
            )
            if var is not None:
                watched_var_values[binding] = cast(RawVariable, var)
        return watched_var_values

    def _has_watched_variable_changed(
        self,
        current_function_watched_values: dict[str, object],
        watched_var_updated_values: dict[NameBinding, RawVariable],
    ) -> bool:
        """checks if some watched variable have changed and returns them in a pause reason"""
        pause_reason = None
        for binding, var in watched_var_updated_values.items():
            wp_num = self._watchpoints.get(binding)
            var_value = var.value
            if wp_num is not None and var_value is not None:
                # if the variable has not been seen yet or is different from last time
                # the value is updated and a watchpoint is triggered
                if (
                    var.name not in current_function_watched_values
                    or current_function_watched_values[var.name] != var_value
                ):
                    # TODO: check frame before accessing dict
                    current_function_watched_values[var.name] = copy.deepcopy(
                        var_value
                    )  # beuark...

                    if pause_reason is None:
                        # Note that watched vars changes are detected after the line execution,
                        # hence the passed line is generally after the value assignment
                        pause_reason = PauseReason(
                            PauseReasonType.WATCHPOINT,
                            line=self._next_lineno,
                            args=[var, wp_num],
                        )
                    else:
                        pause_reason.args.append(var)
                        pause_reason.args.append(wp_num)

        if pause_reason is not None:
            self._last_prog_ctrl_cmd = _ProgCtrlCmd.NONE
            self._pause_reason = pause_reason
            return True

        return False

    # ========= Step control
    def _handle_next(self):
        """We must break if next has been asked.

        This is true as long as we are still in the same
        function_name (depth) or above as when next has been called.
        """
        if self._current_depth <= self._next_depth:
            self._step_counter -= 1
            if self._step_counter == 0:
                self._last_prog_ctrl_cmd = _ProgCtrlCmd.NONE
                self._next_depth = 0
                self._inferior.yield_to_main()

    def _handle_step(self):
        """We must break if step been asked."""
        self._step_counter -= 1
        if self._step_counter == 0:
            # We must break if step has been asked
            self._last_prog_ctrl_cmd = _ProgCtrlCmd.NONE
            self._inferior.yield_to_main()

    # ========= Internal state management
    def _update_last_state(self):
        """updates all the internal states that need to be saved"""
        self._last_lineno = self._next_lineno
        self._last_depth_change = self._next_depth_change

        # update source file we are stopped at
        self._next_source_file = self._trace_frame.f_code.co_filename

    # pylint: disable=too-many-branches, too-many-statements
    def _trace_function(self, frame, event, arg):
        """Local trace function which returns itself.

        We track the execution of every single line of python code.
        """

        # Record the current frame object to be inspected when
        # looking for variable changes
        # TAKE care that FRAME object is the same in a function !
        self._trace_frame = frame

        self._update_last_state()
        self._next_lineno = frame.f_lineno
        self._next_depth_change = DepthChange.STEP

        # if the last event was a return we pop the top of the stack frame
        if self._has_to_pop_frame:
            self._program_stack.pop()
            self._has_to_pop_frame = False

        # If force_program_stop has been called
        # before exit we stop tracing and let the inferior complete.
        if self._notify_terminate:
            self._notify_terminate = False
            raise _TrackerTerminate()

        # Activate per-line tracing: could this be done only once ?
        frame.f_trace_lines = True

        # Update the depth when entering a function scope
        if event == "call":
            self._next_depth_change = DepthChange.CALL
            self._current_depth += 1
            self._watched_vars_values_per_stack_frame.append({})

            parent = None
            if self._program_stack:
                parent = self._program_stack[-1]

            new_frame = RawFrame(
                name=frame.f_code.co_name,
                depth=self._current_depth,
                next_source_file=frame.f_code.co_filename,
                next_lineno=frame.f_lineno,
                variables={},  # variables will be set afterward
                parent=parent,
            )
            self._program_stack.append(new_frame)

        elif event == "return":
            self._next_depth_change = DepthChange.RETURN

        # updates the variables of the last frame
        if self._program_stack:
            # variables are created as simple python objects by defaults and are decorated with Value if requested later
            variables = {
                var_name: create_raw_variable(
                    var_name,
                    frame.f_locals[var_name],
                    function_name=frame.f_code.co_name,
                    depth=self._current_depth - 1,
                )
                for var_name in frame.f_locals
            }
            self._program_stack[-1].variables = variables

            self._program_stack[-1].next_lineno = self._next_lineno

        # Get the watched var values map for this stack frame
        watched_var_updated_values = self._get_watched_var_values_map()

        # Check if we enter or exit a tracked function
        if self._is_entering_exiting_tracked_function(frame, event, arg):
            self._inferior.yield_to_main()

        # Check if we are on a breakpoint
        bp_num_func = self._is_on_break_before_func()
        bp_num_line = self._is_on_break_line()
        bp_line = self._next_lineno

        if event == "call" and frame.f_code.co_name == "main":
            # We must break on main while the prog is not started
            self._inferior.yield_to_main()

        elif event == "call" and bp_num_func != -1:
            self._pause_reason = PauseReason(
                PauseReasonType.BREAKPOINT, line=bp_line, args=[bp_num_func]
            )
            self._inferior.yield_to_main()

        elif bp_num_line != -1:
            # We must break if we reached a breakpoint
            self._pause_reason = PauseReason(
                PauseReasonType.BREAKPOINT, line=bp_line, args=[bp_num_line]
            )
            self._inferior.yield_to_main()

        elif self._last_prog_ctrl_cmd is _ProgCtrlCmd.NEXT:
            self._handle_next()

        elif self._last_prog_ctrl_cmd is _ProgCtrlCmd.STEP:
            self._handle_step()

        # Check for watchpoint changes and update the value of watched variable on the current function_name
        if (
            self._watched_vars_values_per_stack_frame
            and self._has_watched_variable_changed(
                self._watched_vars_values_per_stack_frame[-1],
                watched_var_updated_values,
            )
        ):
            self._inferior.yield_to_main()

        # Update the depth when leaving a function function_name
        if event == "return":
            self._current_depth -= 1
            self._watched_vars_values_per_stack_frame.pop()

            self._has_to_pop_frame = True

        # We must indicate end of program
        if event == "return" and frame.f_code.co_name == "main":
            # The program is ended
            self._exit_code = int(arg) if arg is not None else 0
            self._pause_reason = PauseReason(
                PauseReasonType.EXITED, None, [self._exit_code]
            )

            # Deactivate tracing and continue executing to completion
            return None
        # The tracer function returns itself to indicate it stills
        # wants to be notified.
        return self._trace_function

    # ======= Interface implementation
    def load_program(self, prog_file_path: str):
        @contextmanager
        def add_to_path(path):
            old_path = sys.path
            sys.path = sys.path[:]
            sys.path.insert(0, path)
            try:
                yield
            finally:
                sys.path = old_path

        assert self._inferior is None
        self._inferior_file_path = prog_file_path
        file_name = os.path.basename(prog_file_path)
        dir_name = os.path.dirname(prog_file_path)
        with add_to_path(dir_name):
            module = os.path.splitext(file_name)[0]
            spec = importlib.util.spec_from_file_location(module, prog_file_path)
            self._prog = importlib.util.module_from_spec(spec)  # type:ignore
            spec.loader.exec_module(self._prog)  # type:ignore
            self._exit_code = None

    def force_program_stop(self):
        """Stop inferior, optionally early if not exited yet"""
        if self._inferior is not None:
            if self._exit_code is None:
                self._notify_terminate = True
                self._inferior.yield_to_co()
            self._inferior.join()
            self._inferior = None

    def terminate(self):
        """Terminate inferior if any"""
        if self._inferior is not None:
            self._inferior.join()
            self._inferior = None

    def _update_line_if_end_step(self):
        if self._pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE:
            # Force change with base object setattr instead of the frozen setattr
            object.__setattr__(self._pause_reason, "line", self._next_lineno)

    # control
    def next(self, count: int = 1):
        self._last_source_file = self._next_source_file
        self._last_prog_ctrl_cmd = _ProgCtrlCmd.NEXT
        self._next_depth = self._current_depth
        self._step_counter = count
        self._pause_reason = PauseReason(PauseReasonType.ENDSTEPPING_RANGE)
        self._inferior.yield_to_co()
        self._update_line_if_end_step()
        return self._pause_reason

    def step(self, count: int = 1):
        self._last_source_file = self._next_source_file
        self._last_prog_ctrl_cmd = _ProgCtrlCmd.STEP
        self._step_counter = count
        self._pause_reason = PauseReason(PauseReasonType.ENDSTEPPING_RANGE)
        self._inferior.yield_to_co()
        self._update_line_if_end_step()
        return self._pause_reason

    def _start_in_coroutine(self):
        # Set the tracer up before entering user program
        sys.settrace(self._trace_function)
        # call the main function
        try:
            self._prog.main()
        except _TrackerTerminate:
            pass
        finally:
            sys.settrace(None)
            if self._exit_code is None:
                self._exit_code = -1

    def start(self, in_file: str = None):
        assert self._inferior is None
        assert self._notify_terminate is False
        self._exit_code = None

        # Start the program in the tracker thread
        self._inferior = CoThread(
            target=self._start_in_coroutine,
            name="inferior",
            daemon=True,
        )
        self._inferior.start()

        self._pause_reason = PauseReason(PauseReasonType.START)
        self._inferior.yield_to_co()
        self._update_line_if_end_step()
        return self._pause_reason

    def resume(self):
        self._last_source_file = self._next_source_file
        self._last_prog_ctrl_cmd = _ProgCtrlCmd.CONTINUE
        self._inferior.yield_to_co()
        return self._pause_reason

    # inspection
    def get_current_frame(
        self, as_raw_python_objects: bool = False
    ) -> Union[RawFrame, Frame, None]:
        if self._program_stack:
            # Create the object model
            parent = None
            seen: dict[int, Value] = {}
            for frame in self._program_stack:
                variables = {
                    var_name: create_variable(
                        var_name,
                        var.value,
                        in_stack=True,
                        seen=seen,
                        function_name=frame.name,
                        depth=frame.depth,
                    )
                    for var_name, var in frame.variables.items()
                }
                new_frame = Frame(
                    name=frame.name,
                    depth=frame.depth,
                    next_source_file=frame.next_source_file,
                    next_lineno=frame.next_lineno,
                    variables=variables,
                    parent=parent,
                )
                parent = new_frame
            if not as_raw_python_objects:
                return new_frame
            return super()._as_raw_frame(new_frame, {})
        return None

    def get_global_variables(
        self, as_raw_python_objects: bool = False
    ) -> dict[str, Union[Variable, RawVariable]]:
        seen: dict[int, Value] = {}
        return {
            name: create_variable_option(
                name=name,
                value=val,
                as_raw_python_objects=as_raw_python_objects,
                in_stack=False,
                seen=seen,
                function_name=None,
                depth=None,
            )
            for name, val in self._trace_frame.f_globals.items()
            if not is_python_builtin(name)
        }

    def _get_binding_upward_max(self, binding: NameBinding) -> Optional[int]:
        """
        Returns where the given variable name binding should be searched:
        - -1 if the binding does not match the whole stack frame;
        - 0 if the binding may match only the current frame;
        - n > 0 if the binding may match the current frame up to current + n
        - None if the binding may match any upward frame
        Note: that frames counts include the top level global frame
        Note: as of now if may match only the current frame, or any frame upward
        Note: depth is only used if function_name is specified, as per interface
        """
        if binding.function_name:
            if binding.function_name != self._trace_frame.f_code.co_name:
                return -1
            if (
                binding.depth is not None
                and binding.depth != self._program_stack[-1].depth
            ):
                return -1
            return 0
        return None

    def _get_upward_variable_value(
        self,
        name: str,
        as_raw_python_objects: bool = False,
        maxup: Optional[int] = None,
    ) -> tuple[Union[Variable, RawVariable, None], Optional[int]]:
        """
        Get variable value and upward frame index (0 is latest frame)
        or None if not found. Walk frames up to the current frame + maxup.
        Ref to _get_binding_upward_max() for the value of maxup
        """
        maxup = 2**32 if maxup is None else maxup
        value = None
        upward = 0
        # walk stack frames up
        frame = self._trace_frame
        while frame is not None and upward <= maxup:
            if name in frame.f_locals:
                value = frame.f_locals[name]
                break
            frame = frame.f_back
            upward += 1
        # then global frame
        if value is None and upward <= maxup:
            value = self._trace_frame.f_globals.get(name)
        if value is not None:
            return (
                create_variable_option(
                    name=name,
                    value=value,
                    as_raw_python_objects=as_raw_python_objects,
                    in_stack=False,
                ),
                upward,
            )
        return None, None

    def get_variable_value(
        self, name: str, as_raw_python_objects: bool = False
    ) -> Union[Variable, RawVariable, None]:
        """
        Get variable value or None upto the current frame + maxup.
        Ref to _get_binding_upward_max() for the value of maxup
        """
        var, _ = self._get_upward_variable_value(name, as_raw_python_objects)
        return var

    # tracker specific
    def send_direct_command(self, command: str) -> PauseReason:
        """This has no meaning YET in the python world."""
        raise RuntimeError("Should not be there !")
