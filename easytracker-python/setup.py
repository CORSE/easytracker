#!/usr/bin/env python3
"""
Legacy setup.py, all configuration should be in pyproject.toml.
Provided to support editable installation with pip<23.
"""

import setuptools

setuptools.setup()
