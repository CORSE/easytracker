EasyTracker Python Tracker
==========================

Install with:

    pip install --extra-index-url https://gitlab.inria.fr/api/v4/groups/corse/-/packages/pypi/simple easytracker-python


For inplace development, install with:

    pip install -e '.[dev]'

