"""Utility functions to create dot file"""

import io
import os
import shutil
import subprocess
import time
from typing import Optional, TextIO
from contextlib import contextmanager
from tempfile import NamedTemporaryFile


class DotBuffer(io.StringIO):
    """A simple wrapper to get str value from a StringIO"""

    def add_node(self, name: str, content: str, **kwargs):
        """Adds a node to this buffer"""
        args = " ".join(f'{k.upper()}="{str(v).upper()}"' for k, v in kwargs.items())
        self.write(f'{name} [label="{content}" {args}]\n')

    def add_edge(self, source: str, dest: str):
        """Draw an edge in this buffer"""
        self.write(f"{source} -> {dest};\n")

    def __repr__(self):
        return self.getvalue()


@contextmanager
def dot_file(image_name: Optional[str] = None):
    """Returns a buffer for a dot file and generates an image after closing if specified"""
    buffer = DotBuffer()

    buffer.write("digraph structs {\n")
    buffer.write("node [shape=plaintext]\n")
    buffer.write("nodesep=0;\n")

    try:
        yield buffer
    finally:
        buffer.write("}")

        if image_name is not None:
            # put dot buffer in temporary file
            with NamedTemporaryFile(mode="w", delete=False, suffix=".dot") as dot_f:
                dot_f.write(buffer.getvalue())
                # print(file_buffer.getvalue())
            generate_image(False, image_name, dot_f)


@contextmanager
def dot_cluster(name: str, parent: Optional[TextIO] = None):
    """Buffer for dot cluster with a given name"""
    cluster_buffer = DotBuffer()

    cluster_buffer.write(f"subgraph cluster_{name}")
    cluster_buffer.write("{\n")
    # cluser.buffer.writet("graph [peripheries=0]\n")
    cluster_buffer.write(f"label = {name.upper()};\n")

    try:
        yield cluster_buffer
    finally:
        cluster_buffer.write("}\n")
        if parent:
            parent.write(str(cluster_buffer))


@contextmanager
def dot_table(dot_name, parent: Optional[TextIO] = None, **kwargs):
    """A context manager to wrap the content in an html table.
    Arguments to graphviz can be given in kwargs."""
    table_buffer = DotTable()
    args = " ".join(f'{k.upper()}="{str(v).upper()}"' for k, v in kwargs.items())
    table_buffer.write(f"{dot_name} [label=<\n")
    table_buffer.write(f"<TABLE {args}>\n")
    try:
        yield table_buffer
    finally:
        table_buffer.write("</TABLE>>]\n")
        if parent:
            parent.write(str(table_buffer))


class LineBuffer(DotBuffer):
    """A line buffer to add table data"""

    @contextmanager
    def add_data(self, **kwargs):
        """Adds a new line to the table"""
        data_buffer = DotBuffer()
        args = " ".join(f'{k.upper()}="{str(v)}"' for k, v in kwargs.items())
        data_buffer.write(f"<TD {args}>")
        try:
            yield data_buffer
        finally:
            data_buffer.write("</TD>\n")
            self.write(str(data_buffer))

    def add_inline_data(self, content: str, **kwargs):
        """Adds a new line without context management"""
        with self.add_data(**kwargs) as buffer:
            buffer.write(content)


class DotTable(DotBuffer):
    """A buffer that can be converted to a string later"""

    @contextmanager
    def add_line(self):
        """Adds a new line to the table"""
        line_buffer = LineBuffer()
        line_buffer.write("<TR>\n")
        try:
            yield line_buffer
        finally:
            line_buffer.write("</TR>\n")
            self.write(str(line_buffer))

    def add_single_data_line(self, content: str, **kwargs):
        """Adds the content in a single line in the table"""
        with self.add_line() as line_buffer:
            line_buffer.add_inline_data(content, **kwargs)


def generate_image(visualize: bool, image_name: Optional[str], dot_f):  #: typing.TextIO
    """Generates the svg image for the given dot file."""

    # We know what we do here : we check if we have an extension, so
    # we tell it to pylint and mypy
    try:
        extension = os.path.splitext(image_name)[1][1:]  # type:ignore
    except:  # pylint: disable=bare-except
        extension = None
    if not extension:
        extension = "svg"
        ext_provided = False
    else:
        ext_provided = True

    # If extension is dot, just copy and we are done
    if extension == "dot":
        shutil.copyfile(dot_f.name, image_name)  # type:ignore

    # Else convert the dot to an image
    else:
        dot_name = dot_f.name
        tmp_img_name = f"{dot_name}.{extension}"
        subprocess.check_call(f"dot -T{extension} {dot_name} -o {tmp_img_name}".split())

        # Open image using tycat
        if visualize:
            os.system(f"tycat {tmp_img_name}")
            time.sleep(1)

        # Or copy the image to the current directory
        else:
            if not image_name:
                dot_basename = os.path.basename(dot_f.name)
                dot_basename_no_ext = os.path.splitext(dot_basename)[0][1:]
                target_file = f"./{dot_basename_no_ext}.svg"
                print(f"image generated as {target_file}")
            else:
                target_file = f"{image_name}{'' if ext_provided else '.svg'}"
            shutil.copyfile(tmp_img_name, target_file)
