"""Module generating image of source file with lines highlighting."""

# Standard imports first
from typing import Optional

# Non standard imports then
from PIL import Image, ImageDraw
from pygments import highlight
from pygments.lexers.python import PythonLexer
from pygments.formatters import ImageFormatter  # pylint: disable=no-name-in-module


class CustomImgFormatter(ImageFormatter):
    """Custom formatter able to hilight lines using different colors."""

    def format(self, tokensource, outfile):
        """Code copypasted from parent class with minor modification."""
        # pylint: disable=too-many-locals
        self._create_drawables(tokensource)
        self._draw_line_numbers()
        img = Image.new(
            "RGB",
            self._get_image_size(self.maxlinelength, self.maxlineno),
            self.background_color,
        )
        self._paint_line_number_bg(img)
        draw = ImageDraw.Draw(img)

        # Highlight
        # pylint: disable=invalid-name
        if self.hl_lines:
            x = self.image_pad + self.line_number_width - self.line_number_pad + 1
            recth = self._get_line_height()
            rectw = img.size[0] - x
            for i, linenumber in enumerate(self.hl_lines):
                y = self._get_line_y(linenumber - 1)
                draw.rectangle([(x, y), (x + rectw, y + recth)], fill=self.hl_color[i])
        for pos, value, font, text_fg, text_bg in self.drawables:
            if text_bg:
                text_size = draw.textsize(text=value, font=font)
                draw.rectangle(
                    [pos[0], pos[1], pos[0] + text_size[0], pos[1] + text_size[1]],
                    fill=text_bg,
                )
            draw.text(pos, value, font=font, fill=text_fg)
        img.save(outfile, self.image_format.upper())


def generate_source_image(
    filename: str,
    image_name: str = "sourceimage",
    next_lineno: Optional[int] = None,
    last_lineno: Optional[int] = None,
):
    """Generates an image showing source code with next and last lines highlighted."""
    hl_lines = [l for l in (last_lineno, next_lineno) if l]
    with open(f"{image_name}.jpg", "wb") as jpg_f, open(
        filename, "r", encoding="utf-8"
    ) as code_f:
        jpg = highlight(
            code_f.read(),
            PythonLexer(),
            CustomImgFormatter(
                image_format="jpeg",
                linenos=True,
                line_numbers=True,
                hl_lines=hl_lines,
                hl_color=["lightblue", "red"],
                font_size=40,
            ),
        )
        jpg_f.write(jpg)
