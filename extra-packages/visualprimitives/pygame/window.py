""" A basic window module to be subclassed
    This runs in a separated thread """

# Standard imports
from threading import Thread
from typing import Optional

# Non standard imports
import pygame
import pygame_gui

# Own imports
from visualprimitives.pygame.gui import debugger_gui, console

BOTTOM_RIGHT_ANCHORS = {
    "left": "right",
    "right": "right",
    "top": "bottom",
    "bottom": "bottom",
}
TOP_RIGHT_ANCHORS = {"left": "right", "right": "right", "top": "top", "bottom": "top"}


class BasicWindow(Thread):
    """The main class if no size is provided we go fullscreen"""

    def __init__(self, size: Optional[tuple[int, int]] = None, pygame_handler=None):
        super().__init__()
        # pygame initialisation
        pygame.init()
        pygame.font.init()
        if size is None:
            info = (
                pygame.display.Info()
            )  # You have to call this before pygame.display.set_mode()
            screen_width, screen_height = info.current_w, info.current_h
            self.window_surface = pygame.display.set_mode(
                (screen_width - 10, screen_height - 200), pygame.HWSURFACE
            )
            size = self.window_surface.get_size()
            self.width, self.height = size
        else:
            self.width, self.height = size
            self.window_surface = pygame.display.set_mode(
                (self.width, self.height), pygame.HWSURFACE
            )

        self.ui_manager = pygame_gui.UIManager(size)
        self.structure_elements: list[object] = []

        self.handle_pygame_event = pygame_handler
        self.main_loop_callback = None

    def add(self, elem):
        """Register a new graphical element to be drawn"""
        self.structure_elements.append(elem)

    def run(self):
        """Main loop"""
        is_running = True
        clock = pygame.time.Clock()
        while is_running:
            time_delta = clock.tick(10) / 1000.0
            self.window_surface.fill((200, 200, 200))

            # Handle events
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    is_running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        is_running = False

                self.ui_manager.process_events(event)
                # dispatch events to gui elements
                debugger_gui.get_instance().process_event(event)
                console.get_instance().process_event(event)

                if self.handle_pygame_event is not None:
                    self.handle_pygame_event(event)

            # Draw
            for elem in self.structure_elements:
                elem.draw(self.window_surface)

            if self.main_loop_callback is not None:
                self.main_loop_callback()  # pylint: disable=not-callable

            self.ui_manager.update(time_delta)
            self.ui_manager.draw_ui(self.window_surface)

            # Render
            pygame.display.update()

        pygame.quit()
