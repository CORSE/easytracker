"""
Module that provides graphical gdb shortcuts (buttons)
through pygame.
The debugger GUI is a panel inside the pygame window.
Buttons are configurable by application.
"""
# pylint: disable=attribute-defined-outside-init

# Standard imports
from typing import Callable, Iterator
from itertools import chain

# Non standard imports
import pygame
import pygame_gui
from pygame_gui.elements import UIPanel, UIButton
from pygame_gui.windows import UIMessageWindow

# Own imports
from visualprimitives.pygame.gui import console, primitive

BUTTON_WIDTH = 150
BUTTON_HEIGHT = 50


class DebuggerGUI(primitive.UIPrimitive):
    """
    Provides an implementation of graphical gdb buttons for pygame.
    """

    def init_gui(self, ui_manager, dimensions, anchors=None):
        """
        Initialisation of the buttons and reference the GDB controller for UI actions
        This has to be called in the main window initialization once we have an UIManager instance
        """
        super().init_gui(ui_manager, dimensions, anchors)
        # init UI
        self.panel = UIPanel(
            self.area_rect,
            starting_layer_height=1,
            manager=self.ui_manager,
            anchors=self.anchors,
        )

        self.popup_window = None

        self.callbacks = []
        self.callback_buttons = []
        # dict command -> button
        self.command_buttons = {}

    def new_button_internal(self, text: str, pos: tuple[int, int]) -> UIButton:
        """Instantiates a new button without effect"""
        dim = pygame.Rect(pos, (BUTTON_WIDTH, BUTTON_HEIGHT))
        return UIButton(dim, text, self.ui_manager, container=self.panel)

    def add_callback_button(
        self, text: str, pos: tuple[int, int], callback: Callable[[], None]
    ):
        """Create a new button in self"""
        new_button = self.new_button_internal(text, pos)
        self.callback_buttons.append(new_button)
        self.callbacks.append(callback)

    def add_command_button(self, text: str, pos: tuple[int, int], command: str):
        """Creates a new command button, the button just triggers the given command"""
        new_button = self.new_button_internal(text, pos)
        self.command_buttons[command] = new_button

    def get_button_by_text(self, text: str) -> UIButton:
        """Search for a buttons that has the given text and returns a reference"""
        for button, _ in self.command_buttons.values():
            if button.text == text:
                return button
        for button in self.callback_buttons:
            if button.text == text:
                return button
        raise ValueError(f"No button with text {text}")

    def get_all_buttons(self) -> Iterator[UIButton]:
        """Returns an iterator over all buttons"""
        return chain(self.callback_buttons, self.command_buttons.values())

    def process_event(self, event):
        """Process the given pygame event"""
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                self.process_button_event(event.ui_element)
            elif (
                event.user_type == pygame_gui.UI_WINDOW_CLOSE
                and event.ui_element == self.popup_window
            ):
                self.popup_window = None  # we free the popup_window variable
                # forward popup closing to set callbacks
                # TODO
                # level_controller.instance.current_level.on_popup_closed(event.ui_object_id)

    def process_button_event(self, ui_element):
        """Process the given button pressed event"""
        for command, (button) in self.command_buttons.items():
            if ui_element == button:
                console.get_instance().trigger_command(command)
        for i, button in enumerate(self.callback_buttons):
            if ui_element == button:
                self.callbacks[i]()

    def clear_buttons(self):
        """Clears all the declared buttons"""
        self.command_buttons.clear()
        self.callback_buttons.clear()
        self.callbacks.clear()

    def create_text_popup(
        self, title: str, text: str, dismiss_button_text: str, ui_object_id: str
    ):
        """Occupy the main popup with the given text"""
        # if self.popup_window is not None:
        # raise Exception("Can only have one popup")
        position = pygame.Rect((0, 0), self.ui_manager.window_resolution)
        clean_html_text = text.replace("\n", "<br />")  # fixes the weird character
        # artifacts in pop-ups...
        self.popup_window = UIMessageWindow(
            rect=position,
            html_message=clean_html_text,
            manager=self.ui_manager,
            window_title=title,
            object_id=ui_object_id,
        )
        self.popup_window.dismiss_button.set_text(dismiss_button_text)
        self.popup_window.close_window_button.hide()


instance = DebuggerGUI()


def get_instance():
    """A getter for the global instance"""
    return instance
