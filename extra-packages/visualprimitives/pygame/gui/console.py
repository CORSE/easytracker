"""
Emulation of the gdb command line using the readline lib.
"""
# pylint: disable=attribute-defined-outside-init

# Standard imports
import readline

# Non standard imports
import pygame
import pygame_gui
from pygame_gui.elements import UIPanel, UILabel, UIButton, UITextEntryLine, UITextBox

# Own imports
from visualprimitives.pygame.gui.primitive import UIPrimitive
from easytracker import tracker_interface
from easytracker_gdb.gdb_tracker import GdbTracker

# default tab-completion commands
_GDB_OPTIONS = ("list", "run", "start", "break", "step", "stop")


class Console(UIPrimitive):
    # maybe put console gui in another class ?
    # pylint: disable=too-many-instance-attributes
    """
    Class that provides a usable console.

    The available commands are given as a list in
    argument.
    """

    def __init__(
        self, options: tuple[str, ...] = _GDB_OPTIONS, prompt="(agdbentures) "
    ):
        """
        Initialisation of a console.

        options: the list of know commands. This list
        will be used to give a tab auto-complete.

        message_handler: command that will process each
        command received.

        signal_handler: the function that is called when
        a ctrl + c signal (or system equivalent) is
        received.
        """
        self.prompt = prompt
        self.options = tuple(sorted(options))
        self.matches: list[str] = []

        # Set auto-complete
        readline.parse_and_bind("tab: complete")
        readline.set_completer(self.complete)

        # console display
        self.console_text = ""
        self.console_display = None

    def init_gui(self, ui_manager, dimensions, anchors=None):
        """Graphical initialization of the console"""
        super().init_gui(ui_manager, dimensions, anchors)
        self.ui_manager = ui_manager

        # panel
        self.panel = UIPanel(
            self.area_rect,
            starting_layer_height=1,
            manager=self.ui_manager,
            anchors=self.anchors,
        )

        # prompt
        label_anchor = {
            "left": "left",
            "right": "left",
            "top": "bottom",
            "bottom": "bottom",
        }
        pos = pygame.Rect((0, -20), (120, 20))
        self.prompt_label = UILabel(
            pos,
            self.prompt,
            self.ui_manager,
            container=self.panel,
            anchors=label_anchor,
        )

        # send_button
        button_anchor = {
            "left": "right",
            "right": "right",
            "top": "bottom",
            "bottom": "bottom",
        }
        pos = pygame.Rect((-20, -20), (20, 20))
        self.console_send_button = UIButton(
            pos, ">", self.ui_manager, container=self.panel, anchors=button_anchor
        )

        # command entry
        entry_anchor = label_anchor
        pos = pygame.Rect((115, -22), (self.width - 120 - 20, 20))
        self.command_entry = UITextEntryLine(
            pos, self.ui_manager, container=self.panel, anchors=entry_anchor
        )

        self.redraw_console_display()

    def redraw_console_display(self):
        """Reparse the console text and draw it again if console gui is activated"""
        width, height = self.area_rect.size
        pos = pygame.Rect((0, 0), (width, height - 30))
        if self.console_display is not None:
            self.console_display.kill()
        self.console_display = UITextBox(
            self.console_text, pos, self.ui_manager, container=self.panel
        )
        if self.console_display.scroll_bar is not None:
            # sets scroll bar to the bottom of the console if there is a scrollbar
            self.console_display.scroll_bar.scroll_position = (
                self.console_display.scroll_bar.bottom_limit
            )
            # This function doesn't actually force redraw
            # so we trigger a fake button event to perform redraw
            # Thank you Python
            # self.console_display.redraw_from_text_block()
            self.console_display.scroll_bar.bottom_button.held = True
            self.console_display.scroll_bar.update(0)
            self.console_display.scroll_bar.bottom_button.held = False

    def print_console(self, message: str, prompt=False):
        """
        Prints a message, given as a string in argument,
        on stdout.

        This function handles the prompt redraw since
        readline only redraws the prompt when a command
        is sent through the keyboard.

        Note: Don't call print yourself. Even if print just
        works, the prompt won't be redrawn, the user may
        think that the software is buggy.

        unlock: if set to True, unlocks terminal after the
        message. This will allow the user to send commands.
        If not, keeps the terminal in the previous state
        (locked or unlocked)
        """
        print(message)
        pre_cmd = f"<font color='#00FF00'>{self.prompt}</font>" if prompt else ""
        message = message.replace("\n", "<br>")
        message = message.replace("\t", " " * 4)
        self.console_text += f"{pre_cmd}{message}<br>"
        if self.console_display is not None:
            self.redraw_console_display()

    def trigger_command(self, command: str):
        """Programmatically trigger a console command"""
        readline.add_history(command)

        self.print_console(command, prompt=True)
        tracker = tracker_interface()
        tracker.send_direct_command(command)
        pause_reason = tracker.pause_reason
        if isinstance(tracker, GdbTracker):
            output = tracker.get_gdb_console_output()
            self.print_console(output)

        self.triggered_command_callback(command, pause_reason)

    def triggered_command_callback(self, command, pause_reason):
        """A placeholder callback after a command is sent to the tracker"""

    def process_event(self, event) -> None:
        """
        Gets all pygame events
        Filter if the clicked button is the send button from the console
        """
        # if the console is not graphically initialized, just return
        if self.console_display is None:
            return
        if (
            event.type == pygame.USEREVENT
            and event.user_type == pygame_gui.UI_BUTTON_PRESSED
            and event.ui_element == self.console_send_button
        ) or (
            event.type == pygame.KEYDOWN
            and self.is_keyboard_event_triggering_command(event)
        ):
            line = self.command_entry.get_text()
            self.command_entry.set_text("")

            # case the user exits
            if line in ["q", "quit"]:
                # clean up pygame and wait
                pygame.event.post(pygame.event.Event(pygame.QUIT))
                return

            # if the current command is empty,
            # sends the previous command and
            # add it to the history
            if not line:
                hist_size = readline.get_current_history_length()
                line = readline.get_history_item(hist_size)

                if line is None:
                    return
            # send the command without blackslashes
            # slashes could escape the black list
            self.trigger_command(line.replace("\\", " "))

    def is_keyboard_event_triggering_command(self, event):
        """Returns wether there is not something in the event
        that should prevent command fireing"""
        # pylint: disable=no-member
        if event.key != pygame.K_RETURN:
            return False
        if not self.command_entry.get_text():
            return False
        return self.panel.get_abs_rect().collidepoint(
            self.ui_manager.get_mouse_position()
        )

    def complete(self, text, state):
        """
        Function that is triggered when the auto-complete
        key (in our case TAB) is hit.

        This function will be called by the readline lib
        with two arguments text and state, where state is
        the number of times the complete key has been hit.

        docs:
        - https://docs.python.org/3/library/readline.html
        - https://pymotw.com/3/readline/
        """
        response = None
        if state == 0:
            # first try
            if text:
                self.matches = [s for s in self.options if s and s.startswith(text)]
            else:
                self.matches = self.options[:]

        # Return the state'th item from the match list,
        # if we have that many.
        try:
            response = self.matches[state]
        except IndexError:
            response = None
        return response


instance = Console()


def get_instance():
    """A getter for the global instance"""
    return instance
