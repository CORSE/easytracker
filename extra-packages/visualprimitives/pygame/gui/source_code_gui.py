""" A module to display the source code in the main window """

# Standard imports

# Non standard imports
from pygame_gui.elements import UITextBox
import pygments.lexers

# Own imports
from visualprimitives.pygame.gui.primitive import UIPrimitive
from easytracker.init_tracker import get_tracker_instance

INCLUDE_COLOR = "#FF0000"
TRAILING_INCLUDE_COLOR = "#00DDDD"
KEYWORD_COLOR = "#FFA500"
ARROW_COLOR = "#FF0000"


class SourceGUI(UIPrimitive):
    """A singleton widget to display the level source code"""

    def __init__(self):
        super().__init__()
        self.text = ""

        self._source_file = None
        self.source_line = 0

        self.source_display = None
        self.lexer = None
        self.keywords = None

    def init_gui(self, ui_manager, dimensions, anchors=None):
        """Instanciate the initial text box with the given ui manager"""
        super().init_gui(ui_manager, dimensions, anchors)

        self.clear_text_box()
        self._source_file = None

    def clear_text_box(self):
        """Clear the source text"""
        self.text = ""
        self.redraw_text_box()

    def color_and_convert(self, line) -> str:
        """Takes a single source line as input and convert it to html
        with coloration according to the current lexer."""
        # escape < and >
        line = line.replace("<", "&lt;")
        line = line.replace(">", "&gt;")

        if "#include" in line:
            # color #include in red
            # color includes in light blue
            line_end = line[8:-1]
            return (
                f"<font color='{INCLUDE_COLOR}'>#include</font>"
                f"<font color='{TRAILING_INCLUDE_COLOR}'>{line_end}</font>"
            )

        # color keyword in orange
        for keyword in self.keywords:
            if keyword in line:
                line = line.replace(
                    keyword, f"<font color='{KEYWORD_COLOR}'>{keyword}</font>"
                )

        # removes the trailing \n
        return line[:-1]

    def redraw_text_box(self):
        """Redraw the text box with a text update"""
        formated_content = [
            self.line_prefix(i + 1) + self.color_and_convert(line)
            for i, line in enumerate(self.text)
        ]
        formated_content = "<br>".join(formated_content)

        if self.source_display is not None:
            self.source_display.kill()

        self.source_display = UITextBox(
            formated_content, self.area_rect, self.ui_manager, anchors=self.anchors
        )

    def line_prefix(self, lino):
        """Computes the line prefix to be printed before the source line"""
        prefix = " " * (5 - len(str())) + str(lino) + " " * 2
        if lino == self.source_line:
            prefix = "<font color='{ARROW_COLOR}'>-&gt</font>" + prefix
        else:
            prefix = "  " + prefix
        return prefix

    def set_source_file(self, filename):
        """Redraws the text box with a new source file"""
        self._source_file = filename
        # We let Pygments guessing the lexer to use according to
        # the source file extension.
        self.lexer = pygments.lexers.get_lexer_for_filename(self._source_file)

        with open(filename, "r", encoding="utf-8") as source_file:
            self.text = source_file.readlines()
        self.keywords = []
        for typee, name in self.lexer.get_tokens("".join(self.text)):
            # We use Pygments' lexer to detect the keywords in
            # the text of the source file we want to display.
            if "Keyword" in str(typee) or "Word" in str(typee):
                # We found a keyword, but we don't want to
                # colour words that contain these keywords
                # (ex: the 'in' in 'main' shouldn't be displayed
                # in colour). Adding a blank character next to the
                # keyword appears to do the trick, but it's quite ugly...
                self.keywords.append(f"{name} ")
        self.redraw_text_box()

    def set_line_number(self, lineno):
        """Set the line number."""
        self.source_line = int(lineno)
        self.redraw_text_box()

    def update(self):
        """Get current tracker file pos and update visual"""
        if self._source_file is not None:
            self.source_line = get_tracker_instance().next_lineno
        self.redraw_text_box()


instance = SourceGUI()


def get_instance():
    """A getter for the global instance"""
    return instance
