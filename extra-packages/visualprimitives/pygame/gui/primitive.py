""" Two abstract classes for visual primitive construction """

import pygame


# pylint: disable=too-few-public-methods
# pylint: disable=attribute-defined-outside-init
class UIPrimitive:
    """A UI primitive with a UI manager"""

    def init_gui(self, ui_manager, dimensions, anchors=None):
        """stores UI parameters"""
        self.ui_manager = ui_manager
        self.pos, self.size = dimensions
        if anchors is not None:
            self.anchors = anchors
        else:
            self.anchors = {
                "left": "left",
                "right": "left",
                "top": "top",
                "bottom": "bottom",
            }

        self.area_rect = pygame.Rect(self.pos, self.size)
        self.width, self.height = self.size
