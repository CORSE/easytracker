""" A module for a graphical representation of a 1D array """

# Non standard imports
import pygame

# Own imports
from visualprimitives.pygame.window import BasicWindow

DEBUG_INTERNAL = 0

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)


# pylint: disable=too-many-instance-attributes
class VisualArray:
    """A visual array that keeps an internal array and display it on screen"""

    def __init__(
        self,
        window: BasicWindow,
        dimensions: tuple[tuple[int, int], tuple[int, int]],
        array_length: int,
        indices: list[str],
    ):
        self.pos, self.size = dimensions

        self.array = ["" for _ in range(array_length)]
        self.array_visible = False
        self.indices = dict(zip([str(v) for v in indices], [0 for _ in indices]))
        self.indices_visible = dict(
            zip([str(v) for v in indices], [False for _ in indices])
        )

        # init visual elements
        self.line_height = self.size[1] / (len(indices) + 1)
        self.elem_width = self.size[0] // len(self.array)
        # draw grid
        self.grid_surface = pygame.Surface((self.size[0] + 1, self.line_height + 1))
        self.grid_surface.fill(WHITE)

        points = [
            (0, 0),
            (self.size[0], 0),
            (self.size[0], self.line_height),
            (0, self.line_height),
        ]
        pygame.draw.lines(self.grid_surface, BLACK, closed=True, points=points)
        for i, _ in enumerate(self.array):
            start_pos = (i * self.elem_width, 0)
            end_pos = (i * self.elem_width, self.line_height)
            pygame.draw.line(self.grid_surface, BLACK, start_pos, end_pos)

        # init black and red index arrow
        self.black_arrow = pygame.Surface((10, 20))
        self.red_arrow = pygame.Surface((10, 20))
        self.black_arrow.fill(WHITE)
        self.red_arrow.fill(WHITE)
        for arrow, color in zip([self.black_arrow, self.red_arrow], [BLACK, RED]):
            pygame.draw.line(arrow, color, (5, 0), (5, 20))
            pygame.draw.line(arrow, color, (5, 0), (0, 5))
            pygame.draw.line(arrow, color, (5, 0), (10, 5))

        self.font = pygame.font.SysFont("droidsans", 30)

        window.add(self)

    def show_array(self):
        """Make the array visible"""
        self.array_visible = True

    def hide_array(self):
        """Make the array not visible anymore"""
        self.array_visible = False

    def show_index(self, index: str):
        """Make an index visible"""
        self.indices_visible[index] = True

    def hide_index(self, index: str):
        """Make an index not visible anymore"""
        self.indices_visible[index] = False

    def init_array(self, value: list[str]):
        """Callback for the controller to set the whole array at once"""
        self.array = value

    def set_array_element(self, index: int, value: str):
        """Callback for the controller to changer a specific element"""
        self.array[index] = value
        if DEBUG_INTERNAL:
            print(f"changed position {index} to {value}")
            print(self.array)

    def set_index_value(self, name: str, value: str):
        """Callback for the controller to change the value of an index"""
        self.indices[name] = int(value)
        if DEBUG_INTERNAL:
            print(f"changed indice {name} to {value}")

    def draw(self, screen: pygame.Surface):
        """Draws the different elements on screen"""
        if self.array_visible:
            # grid
            screen.blit(self.grid_surface, self.pos)
        # indices
        for i, (index_variable, index_val) in enumerate(self.indices.items()):
            if self.indices_visible[str(index_variable)]:
                self.draw_arrow(screen, self.black_arrow, index_val, i)
        if self.array_visible:
            # content
            for i, elem in enumerate(self.array):
                text_surface = self.font.render(str(elem), False, BLACK)
                self.place_content(screen, text_surface, i)  # type:ignore

    def draw_arrow(
        self, screen: pygame.Surface, arrow: pygame.Surface, index: int, line: int
    ):
        """Draws the given arrow at the according index position"""
        height = self.pos[1] + (line + 1) * self.line_height + 5
        width = self.pos[0] + index * self.elem_width + self.elem_width // 2 - 5
        screen.blit(arrow, (width, height))

    def place_content(self, screen: pygame.Surface, text: pygame.Surface, index: int):
        """Places the given text in the according index position"""
        height = self.pos[1] + (self.line_height - text.get_height()) // 2
        width = (
            self.pos[0]
            + index * self.elem_width
            + (self.elem_width - text.get_width()) // 2
        )
        screen.blit(text, (width, height))
