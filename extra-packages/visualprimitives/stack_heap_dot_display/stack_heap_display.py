"""Main module for dot display.

Provides two functions:

- draw_stack_heap
- draw_stack_only
"""

# Third party imports
from typing import Optional
from easytracker import Value, Frame
from easytracker.types.variable import Location, AbstractType, Variable

# Our own imports
from .utils import (
    get_value_dotname,
    get_var_dotname,
    is_valid_ref,
    point_in_heap,
    point_in_stack,
    stack_cluster,
    DisplayParams,
    STACK_CLUSTER_NAME,
    HEAP_CLUSTER_NAME,
)
from ..dot_utils import (
    DotBuffer,
    dot_file,
    dot_cluster,
)
from .stack_display import draw_stack
from .heap_display import draw_heap, PointersOutHeap


def draw_stack_heap(
    frame: Frame,
    image_name: str,
    display_params: DisplayParams = DisplayParams(),
):
    """Draws the stack and the heap."""
    with dot_file(image_name) as file_buffer:
        with stack_cluster(file_buffer) as stack_table_buffer:
            draw_stack(
                stack_table_buffer,
                frame,
                display_params=display_params,
            )
        with dot_cluster(HEAP_CLUSTER_NAME, file_buffer) as heap_buffer:
            pointers_out_heap = draw_heap(
                heap_buffer,
                frame,
                display_params=display_params,
            )
        addresses_to_vars: dict[int, Variable] = _get_addresses_to_var(frame)
        _draw_link_starting_in_stack(
            frame, display_params, file_buffer, addresses_to_vars
        )
        _draw_link_out_of_heap(pointers_out_heap, file_buffer, addresses_to_vars)


def draw_stack_only(
    frame: Frame,
    image_name: str,
    display_params: DisplayParams = DisplayParams(),
):
    """Same as draw_stack_heap but only for the stack."""
    with dot_file(image_name) as file_buffer:
        with stack_cluster(file_buffer) as stack_table_buffer:
            draw_stack(
                stack_table_buffer,
                frame,
                display_params=display_params,
            )


def _draw_link_starting_in_stack(
    frame: Frame,
    display_params: DisplayParams,
    file_buffer: DotBuffer,
    addresses_to_vars: dict[int, Variable],
):
    """Draw all the links starting in stack.

    These links always end in the heap in python,
    and can end in the heap and in the stack.
    """
    file_buffer.write("edge [constraint=false]")
    current_frame: Optional[Frame] = frame
    while current_frame is not None:
        for var in current_frame.variables.values():
            var_value: Value = var.value
            if not var_value.abstract_type == AbstractType.REF:
                continue
            if is_valid_ref(var_value):
                var_dot_name = get_var_dotname(var)
                pointed_value: Value = var_value.content  # type: ignore
                if point_in_heap(var_value):
                    if (
                        display_params.inline_primitives
                        and pointed_value.abstract_type == AbstractType.PRIMITIVE
                        or display_params.inline_nil
                        and pointed_value.abstract_type == AbstractType.NONE
                    ):
                        continue
                    pointed_value_dot_name = get_value_dotname(pointed_value)
                    file_buffer.add_edge(
                        f"{STACK_CLUSTER_NAME}:{var_dot_name}",
                        pointed_value_dot_name,
                    )
                elif point_in_stack(var_value):
                    _add_edge_pointing_stack(
                        file_buffer,
                        addresses_to_vars,
                        pointed_value,
                        f"{STACK_CLUSTER_NAME}:{var_dot_name}",
                    )
                else:
                    raise NotImplementedError("Variable pointing somewhere")
        current_frame = current_frame.parent


def _draw_link_out_of_heap(
    pointers_out_heap: PointersOutHeap,
    file_buffer: DotBuffer,
    addresses_to_vars: dict[int, Variable],
):
    """Draw all the links starting in heap and ending outside."""
    for pointer_out_heap in pointers_out_heap:
        src_port = pointer_out_heap[0]
        dst_value = pointer_out_heap[1]
        if dst_value.location == Location.STACK:
            _add_edge_pointing_stack(
                file_buffer, addresses_to_vars, dst_value, src_port
            )


def _get_addresses_to_var(frame: Frame) -> dict[int, Variable]:
    """Returns a dictionnary mapping addresses to variables."""
    current_frame: Optional[Frame] = frame
    addresses_to_vars: dict[int, Variable] = {}
    while current_frame is not None:
        for var in current_frame.variables.values():
            addr = var.value.address
            if addr:
                addresses_to_vars[addr] = var
        current_frame = current_frame.parent
    return addresses_to_vars


def _add_edge_pointing_stack(
    file_buffer: DotBuffer,
    addresses_to_vars: dict[int, Variable],
    stack_value: Value,
    src_port: str,
):
    assert stack_value.location == Location.STACK
    pointed_var = addresses_to_vars.get(stack_value.address, None)  # type: ignore
    if pointed_var:
        pointed_var_dot_name = get_var_dotname(pointed_var)
        file_buffer.add_edge(
            src_port,
            f"{STACK_CLUSTER_NAME}:{pointed_var_dot_name}",
        )
