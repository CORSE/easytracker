"""A module to draw instances on the heap"""

# Standard imports
from dataclasses import dataclass
from typing import Optional, Union

# Third party imports
from easytracker import Frame, Location, Value, Variable, AbstractType

# Own imports
from .utils import (
    DisplayParams,
    Value,
    get_value_dotname,
    get_primitive_display_str,
    get_ref_display_str,
    is_valid_ref,
    point_in_heap,
    Language,
    INSTANCE_BACKGROUND,
    NONE_BACKGROUND,
    get_simplified_language_type,
)
from ..dot_utils import (
    dot_table,
    DotTable,
    DotBuffer,
)

# Types definition
PointerOutHeap = tuple[str, Value]
PointersOutHeap = list[PointerOutHeap]


@dataclass
class _ChildToDraw:
    value: Value
    in_parent_port_name: str


# Set to 1 to enable DEBUG
DEBUG = 0


def draw_heap(
    heap_buffer: DotBuffer,
    frame: Frame,
    display_params: DisplayParams = DisplayParams(),
) -> PointersOutHeap:
    """Draws the heap instances recursively in the given buffer"""
    seen_insts: set[str] = set()
    variables: list[Variable] = []
    current_frame: Optional[Frame] = frame
    while current_frame is not None:
        variables.extend(var for var in current_frame.variables.values())
        current_frame = current_frame.parent
    pointers_going_out_of_heap: PointersOutHeap = []
    for variable in variables:
        # If the variable is a valid pointer into the heap
        if (
            variable.value.abstract_type == AbstractType.REF
            and is_valid_ref(variable.value)
            and point_in_heap(variable.value)
        ):
            pointed_value: Value = variable.value.content  # type: ignore
            pointers_going_out_of_heap.extend(
                draw_instance(
                    pointed_value,
                    buffer=heap_buffer,
                    seen_insts=seen_insts,
                    display_params=display_params,
                )
            )
    return pointers_going_out_of_heap


def draw_instance(
    value: Value,
    buffer,  #: typing.TextIO,
    seen_insts: set[str],
    display_params: DisplayParams,
) -> PointersOutHeap:
    """Draws an instance of a value on the heap.

    And recursively draws all children.
    Returns the pointers going out of the heap.
    """
    if value.abstract_type == AbstractType.PRIMITIVE:
        if not display_params.inline_primitives:
            _draw_primitive(
                value=value,
                buffer=buffer,
                seen_insts=seen_insts,
                display_params=display_params,
            )
        return []

    if value.abstract_type == AbstractType.NONE:
        if not display_params.inline_nil:
            _draw_none(
                value=value,
                buffer=buffer,
                seen_insts=seen_insts,
                display_params=display_params,
            )
        return []

    if value.abstract_type == AbstractType.LIST:
        return _draw_list(
            value=value,
            buffer=buffer,
            seen_insts=seen_insts,
            display_params=display_params,
        )

    if value.abstract_type == AbstractType.DICT:
        return _draw_dict(
            value=value,
            buffer=buffer,
            seen_insts=seen_insts,
            display_params=display_params,
        )

    if value.abstract_type == AbstractType.REF:
        assert display_params.language != Language.PYTHON
        return _draw_ref(
            value=value,
            buffer=buffer,
            seen_insts=seen_insts,
            display_params=display_params,
        )

    if value.abstract_type == AbstractType.STRUCT:
        return _draw_struct(
            value=value,
            buffer=buffer,
            seen_insts=seen_insts,
            display_params=display_params,
        )

    raise NotImplementedError(str(value.abstract_type))


def _log(msg: object):
    """log the given object on demand"""
    if DEBUG:
        print(msg)


def _get_nb_attributes(inst: object):
    """Return the nb of attributes using len of inst.__dict__"""
    try:
        nb_attributes = len(vars(inst))
    except TypeError:
        # TypeError thrown if inst has no __dict__ attributes
        nb_attributes = 0
    return nb_attributes


def _already_seen(value: Value, seen_insts: set[str]) -> str:
    """Returns the dot name or empty string if already seen."""
    dot_name = get_value_dotname(value)
    if dot_name in seen_insts:
        return ""
    seen_insts.add(dot_name)
    return dot_name


def _draw_header_rows(
    value: Value,
    table_buffer: DotTable,
    colspan: int,
    display_params: DisplayParams,
):
    """Draws the header rows according to display params : address, type"""
    if display_params.show_address:
        table_buffer.add_single_data_line(f"0x{value.address:x}", colspan=colspan)
    if display_params.show_type_in_heap:
        table_buffer.add_single_data_line(
            f"<U>{get_simplified_language_type(value.language_type)}</U>",
            colspan=colspan,
        )


def _draw_child_in_parent(display_params, col, line_buffer, elem: Value, port_name):
    """Draws the given child as a cell in the given line"""

    # If REF
    if elem.abstract_type == AbstractType.REF:
        pointed_value: Value = elem.content  # type: ignore
        # If REF to primitive type and primitive type are inlined
        # or REF to Nil and Nil are inlined
        # Draw IN the LIST
        if (
            display_params.inline_primitives
            and pointed_value.abstract_type == AbstractType.PRIMITIVE
            or display_params.inline_nil
            and pointed_value.abstract_type == AbstractType.NONE
        ):
            primitive: Union[int, float, str] = pointed_value.content  # type: ignore
            line_buffer.add_inline_data(
                get_primitive_display_str(primitive),
                bgcolor=col,
                port=port_name,
            )

            # Otherwise we draw address|dot|cross and the
            # pointed value
        else:
            line_buffer.add_inline_data(
                get_ref_display_str(
                    elem,
                    display_params.inline_nil,
                    display_params.inline_primitives,
                    display_params.inline_all_in_stack,
                    display_params.show_address,
                ),
                bgcolor=col,
                port=port_name,
            )
            if is_valid_ref(elem):
                return pointed_value

    # IF PRIMITIVE
    elif elem.abstract_type == AbstractType.PRIMITIVE:
        primitive: Union[int, float, str] = elem.content  # type: ignore
        line_buffer.add_inline_data(
            get_primitive_display_str(primitive),
            bgcolor=col,
            port=port_name,
        )

        # Else
    else:
        print(elem)
        raise NotImplementedError()
    return None


def _draw_none(
    value: Value,
    buffer: DotBuffer,
    seen_insts: set[str],
    display_params: DisplayParams,
):
    """Draw the None instance"""
    dot_name = _already_seen(value, seen_insts)
    if len(dot_name) == 0:
        return

    with dot_table(
        dot_name,
        parent=buffer,
        bgcolor=NONE_BACKGROUND,
        border="0",
        cellborder="1",
        cellspacing="0",
    ) as table_buffer:
        _draw_header_rows(
            value=value,
            colspan=1,
            table_buffer=table_buffer,
            display_params=display_params,
        )
        table_buffer.add_single_data_line("None", bgcolor=NONE_BACKGROUND)


def _draw_primitive(
    value: Value,
    buffer: DotBuffer,
    seen_insts: set[str],
    display_params: DisplayParams,
):
    """Draw a primitive instance"""
    dot_name = _already_seen(value, seen_insts)
    if len(dot_name) == 0:
        return
    primitive_content: Union[int, float, str] = value.content  # type:ignore
    with dot_table(
        dot_name,
        parent=buffer,
        bgcolor=INSTANCE_BACKGROUND,
        border="0",
        cellborder="1",
        cellspacing="0",
    ) as table_buffer:
        _draw_header_rows(
            value=value,
            colspan=1,
            table_buffer=table_buffer,
            display_params=display_params,
        )
        table_buffer.add_single_data_line(
            get_primitive_display_str(primitive_content), bgcolor=INSTANCE_BACKGROUND
        )


def _draw_ref(
    value: Value,
    buffer: DotBuffer,
    seen_insts: set[str],
    display_params: DisplayParams,
) -> PointersOutHeap:
    """Draw a ref instance"""
    dot_name = _already_seen(value, seen_insts)
    if len(dot_name) == 0:
        return []
    with dot_table(
        dot_name,
        parent=buffer,
        bgcolor=INSTANCE_BACKGROUND,
        border="0",
        cellborder="1",
        cellspacing="0",
    ) as table_buffer:
        _draw_header_rows(
            value=value,
            colspan=1,
            table_buffer=table_buffer,
            display_params=display_params,
        )
        ref_str = get_ref_display_str(
            value,
            display_params.inline_nil,
            display_params.inline_primitives,
            display_params.inline_all_in_stack,
            display_params.show_address,
        )
        port_name = f"heap_pointer_src_0x{value.address:x}"
        table_buffer.add_single_data_line(
            ref_str, bgcolor=INSTANCE_BACKGROUND, port=port_name
        )

        # If the ref is valid
        if is_valid_ref(value):
            # Draw pointed object and link if it is in the HEAP
            link_src = f"{dot_name}:{port_name}"
            pointed_value: Value = value.content  # type: ignore
            if pointed_value.location == Location.HEAP:
                draw_instance(pointed_value, buffer, seen_insts, display_params)
                pointed_value_dot_name = get_value_dotname(pointed_value)
                buffer.add_edge(
                    link_src,
                    f"{pointed_value_dot_name}",
                )
                return []
            # Else return the link to be drawn to the caller
            if pointed_value.location == Location.STACK:
                return [(link_src, pointed_value)]
            raise NotImplementedError()

        return []


def _draw_children(
    buffer: DotBuffer,
    seen_insts: set[str],
    display_params: DisplayParams,
    children: list[_ChildToDraw],
) -> PointersOutHeap:
    """Draws the given children along with links from parent."""
    pointers_out_heap: PointersOutHeap = []
    for child in children:
        # Draw the child value
        pointers_out_heap.extend(
            draw_instance(
                child.value,
                buffer=buffer,
                seen_insts=seen_insts,
                display_params=display_params,
            )
        )
        # Draw the edge between the parent and the child value
        # if this value is in the heap, else return the link
        if child.value.location == Location.HEAP:
            pointed_value_dot_name = get_value_dotname(child.value)
            buffer.add_edge(
                child.in_parent_port_name,
                f"{pointed_value_dot_name}",
            )
        else:
            pointers_out_heap.append((child.in_parent_port_name, child.value))
    return pointers_out_heap


# pylint: disable=too-many-locals
def _draw_list(
    value: Value,
    buffer: DotBuffer,
    seen_insts: set[str],
    display_params: DisplayParams,
) -> PointersOutHeap:
    """Draw a LIST"""
    dot_name = _already_seen(value, seen_insts)
    if len(dot_name) == 0:
        return []

    list_content: list[Value] = value.content  # type:ignore
    with dot_table(
        dot_name,
        parent=buffer,
        bgcolor=INSTANCE_BACKGROUND,
        border="0",
        cellborder="1",
        cellspacing="0",
    ) as table_buffer:
        _draw_header_rows(
            value=value,
            colspan=len(list_content) if len(list_content) else 1,
            table_buffer=table_buffer,
            display_params=display_params,
        )

        # If the LIST is empty
        if len(list_content) == 0:
            table_buffer.add_single_data_line("empty")
            return []

        # A LIST is drawn as a single line with N cells where N is
        # the number of elements in the LIST
        child_to_be_drawn = []
        child_count = 0
        parent_dot_name = get_value_dotname(value)
        with table_buffer.add_line() as line_buffer:
            for elem in list_content:
                port_name = f"port_child{child_count}"
                pointed_value = _draw_child_in_parent(
                    display_params,
                    INSTANCE_BACKGROUND,
                    line_buffer,
                    elem,
                    port_name,
                )
                if pointed_value is not None:
                    parent_port = f"{parent_dot_name}:port_child{child_count}"
                    child_to_be_drawn.append(_ChildToDraw(pointed_value, parent_port))
                    child_count += 1

        return _draw_children(
            buffer=buffer,
            seen_insts=seen_insts,
            display_params=display_params,
            children=child_to_be_drawn,
        )


# pylint: disable=too-many-locals
def _draw_dict(
    value: Value,
    buffer: DotBuffer,
    seen_insts: set[str],
    display_params: DisplayParams,
) -> PointersOutHeap:
    """Draw a DICT"""
    dot_name = _already_seen(value, seen_insts)
    if len(dot_name) == 0:
        return []

    dict_content: dict[Value, Value] = value.content  # type:ignore
    with dot_table(
        dot_name,
        parent=buffer,
        bgcolor=INSTANCE_BACKGROUND,
        border="0",
        cellborder="1",
        cellspacing="0",
    ) as table_buffer:
        _draw_header_rows(
            value=value,
            colspan=2 if len(dict_content) else 1,
            table_buffer=table_buffer,
            display_params=display_params,
        )

        # If the DICT is empty
        if len(dict_content) == 0:
            table_buffer.add_single_data_line("empty")
            return []

        # A DICT is drawn as 2 columns lines : key, value
        child_to_be_drawn = []
        child_count = 0
        parent_dot_name = get_value_dotname(value)
        with table_buffer.add_line() as key_value_line_buffer:
            key_value_line_buffer.add_inline_data(
                "key",
                bgcolor=INSTANCE_BACKGROUND,
            )
            key_value_line_buffer.add_inline_data(
                "value",
                bgcolor=INSTANCE_BACKGROUND,
            )
        for key_elem, value_elem in dict_content.items():
            with table_buffer.add_line() as line_buffer:
                key_port_name = f"key_port_child{child_count}"
                value_port_name = f"value_port_child{child_count}"
                for elem, port_name in (
                    (key_elem, key_port_name),
                    (value_elem, value_port_name),
                ):
                    pointed_value = _draw_child_in_parent(
                        display_params,
                        INSTANCE_BACKGROUND,
                        line_buffer,
                        elem,
                        port_name,
                    )
                    if pointed_value is not None:
                        parent_port = f"{parent_dot_name}:{port_name}"
                        child_to_be_drawn.append(
                            _ChildToDraw(pointed_value, parent_port)
                        )
                        child_count += 1

        return _draw_children(
            buffer=buffer,
            seen_insts=seen_insts,
            display_params=display_params,
            children=child_to_be_drawn,
        )


def _draw_struct(
    value: Value,
    buffer: DotBuffer,
    seen_insts: set[str],
    display_params: DisplayParams,
) -> PointersOutHeap:
    """Draw a STRUCT"""
    dot_name = _already_seen(value, seen_insts)
    if len(dot_name) == 0:
        return []

    struct_content: dict[str, Value] = value.content  # type:ignore
    with dot_table(
        dot_name,
        parent=buffer,
        bgcolor=INSTANCE_BACKGROUND,
        border="0",
        cellborder="1",
        cellspacing="0",
    ) as table_buffer:
        _draw_header_rows(
            value=value,
            colspan=len(struct_content) if len(struct_content) else 1,
            table_buffer=table_buffer,
            display_params=display_params,
        )

        # If the STRUCT is empty
        if len(struct_content) == 0:
            table_buffer.add_single_data_line("empty")
            return []

        # A STRUCT is drawn as two N cells lines :
        #   - the first contains attribute names
        #   - the second contains attribute values
        child_to_be_drawn = []
        child_count = 0
        parent_dot_name = get_value_dotname(value)
        with table_buffer.add_line() as line_buffer:
            for attribute_name in struct_content.keys():
                line_buffer.add_inline_data(
                    content=attribute_name, bgcolor=INSTANCE_BACKGROUND
                )
        with table_buffer.add_line() as line_buffer:
            for attribute_elem in struct_content.values():
                port_name = f"port_child{child_count}"
                pointed_value = _draw_child_in_parent(
                    display_params,
                    INSTANCE_BACKGROUND,
                    line_buffer,
                    attribute_elem,
                    port_name,
                )
                if pointed_value is not None:
                    parent_port = f"{parent_dot_name}:port_child{child_count}"
                    child_to_be_drawn.append(_ChildToDraw(pointed_value, parent_port))
                    child_count += 1

        return _draw_children(
            buffer=buffer,
            seen_insts=seen_insts,
            display_params=display_params,
            children=child_to_be_drawn,
        )
