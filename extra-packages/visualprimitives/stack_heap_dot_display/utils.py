"""A module with dot buffers that can be printed into text"""

import re
from typing import Union
from dataclasses import dataclass
from enum import Enum
from contextlib import contextmanager

from easytracker.types.variable import Location, AbstractType, Variable, Value
from ..dot_utils import DotBuffer, dot_cluster, dot_table

VARIABLE_BACKGROUND = "chartreuse"
INSTANCE_BACKGROUND = "gray"
CINSTANCE_BACKGROUND = "lightgray"
NONE_BACKGROUND = "deeppink"

DOT_CHAR = "\u25CF"
CROSS_CHAR = "\u00D7"

STACK_CLUSTER_NAME = "stack_table"
HEAP_CLUSTER_NAME = "heap"


class Language(Enum):
    """A simple language enum for tracing"""

    C = 0
    PYTHON = 1


@dataclass
class DisplayParams:
    """Class representing the set of display parameters.

    Attributes
      - inline_primitives    Inline PRIMITIVE values inside their parent (stack variable or heap object)
      - inline_nil           Inline NONE values inside their parent (stack variable or heap object)
      - inline_all_in_stack  Inline everything in the stack (hence no heap is displayed at all !)
      - show_address         When inline_all_in_stack is false, show address instead of ● for valid REF values
      - show_type_in_heap    Show the type in each heap objects
      - show_type_in_stack   Show the type column in the stack
      - language             Programming language of the inferior being displayed
    """

    inline_primitives: bool = False
    inline_nil: bool = False
    inline_all_in_stack: bool = False
    show_address: bool = False
    show_type_in_heap: bool = True
    show_type_in_stack: bool = True
    language: Language = Language.PYTHON

    def is_python(self):
        """Returns if the params language is Python"""
        return self.language == Language.PYTHON

    def is_c(self):
        """Same for C"""
        return self.language == Language.C


@contextmanager
def stack_cluster(parent: DotBuffer):
    """returns a table buffer for the stack"""
    # pylint: disable=no-member
    stack_ctx = dot_cluster("stack", parent=parent)
    stack_buffer = stack_ctx.__enter__()

    table_ctx = dot_table(
        STACK_CLUSTER_NAME, parent=stack_buffer, cellborder=1, border=0, cellspacing=0
    )
    table_buffer = table_ctx.__enter__()

    try:
        yield table_buffer
    finally:
        # TODO proper object for error management
        table_ctx.__exit__(None, None, None)
        stack_ctx.__exit__(None, None, None)


def get_var_dotname(var: Variable):
    """Returns the name identifying the variable in the dot file."""
    return f"{var.name}_{var.function_name}_var_0x{id(var):0x}"


def get_value_dotname(val: Value):
    """Returns the name identifying the value in the dot file."""
    return f"val_0x{val.address:x}"


def is_valid_ref(value: Value):
    """Indicates if the given REF value points to valid memory."""
    assert value.abstract_type == AbstractType.REF
    pointed_value: Value = value.content  # type: ignore
    return pointed_value.abstract_type != AbstractType.INVALID


def is_python_primitive(inst: object):
    """Indicates if an instance is a primitive or not."""
    return type(inst) in (int, float, str)


def get_primitive_display_str(primitive: Union[int, str, float]):
    """Returns the str to be displayed for a primitive."""
    return f'"{primitive}"' if isinstance(primitive, str) else f"{primitive}"


def get_ref_display_str(
    value: Value,
    inline_nil: bool,
    inline_primitives: bool,
    inline_all: bool,
    show_address: bool,
) -> str:
    """Returns the string to display for the given REF value."""
    assert value.abstract_type == AbstractType.REF
    pointed_value: Value = value.content  # type: ignore

    if is_valid_ref(value):
        if (
            inline_nil or inline_all
        ) and pointed_value.abstract_type == AbstractType.NONE:
            value_str = "None"
        elif (
            inline_primitives or inline_all
        ) and pointed_value.abstract_type == AbstractType.PRIMITIVE:
            primitive: Union[int, str, float] = pointed_value.content  # type: ignore
            value_str = get_primitive_display_str(primitive)
        elif inline_all:
            value_str = pointed_value.to_human_str(inline_ref=True)
        elif not show_address:
            value_str = DOT_CHAR
        else:
            value_str = f"0x{pointed_value.address:x}"
    else:  # The address is invalid, we draw a cross or NULL pointer
        if (
            pointed_value.abstract_type == AbstractType.INVALID
            and pointed_value.address == 0
        ):
            value_str = CROSS_CHAR
        else:
            value_str = CROSS_CHAR
    return value_str


def point_in(value: Value, loc: Location) -> bool:
    """Returns True if the given REF value points into the given location."""
    assert value.abstract_type == AbstractType.REF
    pointed_value: Value = value.content  # type: ignore
    return pointed_value.location == loc


def point_in_heap(value: Value) -> bool:
    """Returns True if the given REF value points into the heap."""
    assert value.abstract_type == AbstractType.REF
    return point_in(value, Location.HEAP)


def point_in_stack(value: Value) -> bool:
    """Returns True if the given REF value points into the stack."""
    assert value.abstract_type == AbstractType.REF
    return point_in(value, Location.STACK)


PYTHON_LANGUAGE_TYPE_REGEXP = re.compile("<class '(.*)'>")


def get_simplified_language_type(language_type: str) -> str:
    """Returns a simplified version of the given language_type where Python "<class" is removed"""
    match_res = PYTHON_LANGUAGE_TYPE_REGEXP.match(language_type)
    if match_res is None:
        return language_type
    return match_res.group(1).split(".")[-1]
