"""A module to draw the stack in a dot table buffer"""

# Standard imports
from typing import Union, Optional

# Third party imports
from easytracker import Frame, Variable, AbstractType

# Own imports
from .utils import (
    VARIABLE_BACKGROUND,
    DisplayParams,
    get_var_dotname,
    get_ref_display_str,
    get_primitive_display_str,
    get_simplified_language_type,
)
from ..dot_utils import (
    DotTable,
    LineBuffer,
)

DEBUG = 0


def log(msg: object):
    """Logs the given object on demand."""
    if DEBUG:
        print(msg)


def _draw_frame_variable(
    buffer: LineBuffer,
    variable: Variable,
    display_params: DisplayParams,
):
    """Draws a single line inside a frame table"""

    buffer.add_inline_data(
        content=str(variable.name), align="center", bgcolor=VARIABLE_BACKGROUND
    )

    type_str = get_simplified_language_type(variable.value.language_type)

    if display_params.show_type_in_stack:
        buffer.add_inline_data(
            content=f"<U>{type_str}</U>",
            align="center",
            bgcolor=VARIABLE_BACKGROUND,
        )

    # C variables of type pointer and all Python variables are REF
    if variable.value.abstract_type == AbstractType.REF:
        value_str = get_ref_display_str(
            variable.value,
            display_params.inline_nil,
            display_params.inline_primitives,
            display_params.inline_all_in_stack,
            display_params.show_address,
        )
    # Only C variable values can be primitive
    elif variable.value.abstract_type == AbstractType.PRIMITIVE:
        primitive: Union[int, str, float] = variable.value.content  # type: ignore
        value_str = get_primitive_display_str(primitive)
    # C struct
    elif variable.value.abstract_type == AbstractType.STRUCT:
        value_str = variable.value.to_human_str()
    else:
        print(variable)
        raise NotImplementedError()
    var_port = get_var_dotname(variable)
    buffer.add_inline_data(
        content=value_str,
        align="center",
        port=var_port,
        bgcolor=VARIABLE_BACKGROUND,
    )


def _draw_frame(
    table_buffer: DotTable,
    variables: list[Variable],
    display_params: DisplayParams,
    funcname: str,
    nb_cols_in_stack: int,
):
    """draws a frame in the stack table"""

    # funcname on the left (has to print the first var)
    with table_buffer.add_line() as first_line:
        first_line.add_inline_data(
            content=funcname, rowspan=len(variables) if len(variables) else 1
        )
        if len(variables) > 0:
            _draw_frame_variable(first_line, variables[0], display_params)
        else:
            first_line.add_inline_data(
                content="no local variables",
                bgcolor=VARIABLE_BACKGROUND,
                colspan=f"{nb_cols_in_stack}",
            )

    # print variables
    for var in variables[1:]:
        with table_buffer.add_line() as var_line:
            _draw_frame_variable(var_line, var, display_params)


def draw_stack(
    table_buffer: DotTable,
    frame: Frame,
    display_params: DisplayParams,
):
    """Draws the stack in the given buffer."""

    # Sorts local variables into frames
    frames_to_variables: dict[tuple[str, int], list[Variable]] = {}
    current_frame: Optional[Frame] = frame
    empty_stack = True
    while current_frame is not None:
        variables = list(current_frame.variables.values())
        frames_to_variables[(current_frame.name, current_frame.depth)] = variables
        if len(current_frame.variables) > 0:
            empty_stack = False
        current_frame = current_frame.parent

    # Display empty stack
    if empty_stack:
        with table_buffer.add_line() as line:
            line.add_inline_data(
                content="empty stack", border=0, bgcolor="white", colspan=2
            )
        return

    # Display headers
    with table_buffer.add_line() as header_line:
        header_line.add_inline_data(content="", border=0)
        header_line.add_inline_data(content="<B>name</B>")
        if display_params.show_type_in_stack:
            header_line.add_inline_data(content="<B>type</B>")
        header_line.add_inline_data(content="<B>value</B>")
        nb_cols_in_stack = 3 + int(display_params.show_type_in_stack)

    for (funcname, _), frame_vars in reversed(frames_to_variables.items()):
        _draw_frame(
            table_buffer, frame_vars, display_params, funcname, nb_cols_in_stack
        )
