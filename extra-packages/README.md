Extra packages for easytracker
==============================

This directory contains extra packages for visualization with `easytracker`.

The modules provided here are mainly used for the examples `tools` in the `easytracker`
top directory and for running integration tests.

These modules are subject to changes and should not be considered to provide a stable interface.

Enable the modules by setting the `PYTHONPATH` there:

    export PYTHONPATH="$PWD:$PYTHONPATH"

And ensure to have the following tools installed:
- fontconfig
- graphviz (dot)
- eog (for displaying images with --show option on some tools)

For instance, install these tools with:

    sudo apt install fontconfig graphviz eog

