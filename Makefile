
DEFAULT_TARGETS = generic_array easiest

TARGETS=$(DEFAULT_TARGETS) rust docs-site
TARGETS_CLEAN=$(TARGETS:%=%-clean)
TARGETS_DISTCLEAN=$(TARGETS:%=%-distclean)

TARGET_DIR_generic_array=tools/generic/test_array/inferiors
TARGET_DIR_easiest=tools/generic/easiest_tool/inferiors
TARGET_DIR_rust=tools/generic/test_array/inferiors
TARGET_DIR_docs-site=docs/site
TARGET_DEFAULT_rust=rust

PACKAGES=base python gdb
DISTS=$(PACKAGES:%=dist-%)
DISTS_UPLOAD=$(PACKAGES:%=dist-upload-%)
DISTS_INSTALL=$(PACKAGES:%=dist-install-%)
DISTS_CLEAN=$(PACKAGES:%=dist-clean-%)
DISTS_DISTCLEAN=$(PACKAGES:%=dist-distclean-%)
DISTS_DEVELOP=$(PACKAGES:%=dist-develop-%)
DIST_SRC_base=.
DIST_SRC_python=easytracker-python
DIST_SRC_gdb=easytracker-gdb

EXTRA_INDEX_URL = https://gitlab.inria.fr/api/v4/groups/corse/-/packages/pypi/simple

all: $(DEFAULT_TARGETS) _local-all

pages: docs-site
	rm -rf public/
	cp -a docs/site/site public/

_local-all:

$(TARGETS): %:
	$(MAKE) -C $(TARGET_DIR_$*) $(TARGET_DEFAULT_$*)

$(TARGETS_CLEAN): %-clean:
	-$(MAKE) -C $(TARGET_DIR_$*) clean

$(TARGETS_DISTCLEAN): %-distclean:
	-$(MAKE) -C $(TARGET_DIR_$*) distclean

clean: $(TARGETS_CLEAN) _local-clean

_local-clean: dist-clean

distclean: $(TARGETS_DISTCLEAN) _local-distclean

_local-distclean: _local-clean dist-distclean
	find . -name '*~' -delete
	find . -type d -name '__pycache__' | xargs -n1 rm -rf
	find . -type d -name '.mypy_cache' | xargs -n1 rm -rf
	find . -type d -name '.pytest_cache' | xargs -n1 rm -rf
	rm -rf public

develop:
	# Must be installed sequencially
	$(MAKE) -j 1 $(DISTS_DEVELOP)

dist: $(DISTS)

dist-upload: $(DISTS_UPLOAD)

dist-install:
	# Must be installed sequencially
	$(MAKE) -j 1 $(DISTS_INSTALL)

dist-clean: $(DISTS_CLEAN)

dist-distclean: $(DISTS_DISTCLEAN)

$(DISTS): dist-%:
	rm -rf $(DIST_SRC_$*)/dist/ $(DIST_SRC_$*)/build/ $(DIST_SRC_$*)/*.egg-info/
	python3 -m build $(DIST_SRC_$*)

$(DISTS_UPLOAD): dist-upload-%:
	twine upload --repository easytracker --skip-existing $(DIST_SRC_$*)/dist/easytracker_$*-*.tar.gz $(DIST_SRC_$*)/dist/easytracker_$*-*.whl

$(DISTS_INSTALL): dist-install-%:
	pip3 install --no-deps --force-reinstall $(DIST_SRC_$*)/dist/easytracker_$*-*.whl
	pip3 install --extra-index-url $(EXTRA_INDEX_URL) $(DIST_SRC_$*)/dist/easytracker_$*-*.whl

$(DISTS_DEVELOP): dist-develop-%:
	rm -rf $(DIST_SRC_$*)/build/ $(DIST_SRC_$*)/*.egg-info/
	pip3 install --extra-index-url $(EXTRA_INDEX_URL) -e ./$(DIST_SRC_$*)[dev]

$(DISTS_CLEAN): dist-clean-%:
	rm -rf $(DIST_SRC_$*)/build/ $(DIST_SRC_$*)/*.egg-info/

$(DISTS_DISTCLEAN): dist-distclean-%: dist-clean-%
	rm -rf $(DIST_SRC_$*)/dist/

.PHONY: all pages clean distclean _local-all _local-clean _local-distclean
.PHONY: develop dist dist-upload dist-install dist-clean dist-distclean
.PHONY: $(DISTS) $(DISTS_UPLOAD) $(DISTS_INSTALL) $(DISTS_DEVELOP) $(DISTS_CLEAN) $(DISTS_DISTCLEAN)
.PHONY: $(TARGETS) $(TARGETS_CLEAN) $(TARGETS_DISTCLEAN)
