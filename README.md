# User documentation

## Content of the repository

- `easytracker/` : Python source code for the abstract interface
- `easytracker-python/` : source code for the Python tracker
- `easytracker-gdb/` : source code for the GDB tracker
- `tests/` : all the tests to be ran before pushing new commits to the repository;
- `tools/` : Python source code for several tools using `easytracker`;
- `extra-packages/visualprimitives/`: Python source code for visual primitives used by the tools from the `tools` folder.

The whole interface is documented in the `easytracker/` directory, in the `interface.py` file and in all the files in the `types/` directory.

Online documentation pages are also available at [https://corse.gitlabpages.inria.fr/easytracker/](https://corse.gitlabpages.inria.fr/easytracker/)

## Install and use out of source

For out of source install, refer to the instructions below, otherwise for source based install or development mode refer to [Source Install](#install-and-use-from-latest-sources-or-for-development).

In order to use `easytracker` you need the following requirements:

- use Python at least 3.9
- [for GDB tracker only] have `gdb` at least version 11 built with Python support at least 3.9 in the `PATH` environment variable

Note: if you find a pickling error when using the `gdb` tracker it is likely because the Python version inside your `gdb` is below 3.9.
Use `ldd` on you `gdb` binary file to identify which Python version it embeds.

Install easytracker from the GitLab repository with the following commands (`easytracker-python` or `easytracker-gdb` are optional if you do not use the corresponding tracker):

    pip3 install --extra-index-url https://gitlab.inria.fr/api/v4/groups/corse/-/packages/pypi/simple easytracker-python easytracker-gdb

## Use easytracker as a requirement for another project

In order to use easytracker in another project, one may add the `--extra-index-url` option directly in the project's `requirements.txt`
file as shown below:

    $ cat requirements.txt
    --extra-index-url https://gitlab.inria.fr/api/v4/groups/corse/-/packages/pypi/simple
    easytracker-gdb>=1.0.0
    ...
    other-requirement
    ...
    

## Install and use from latest sources or for development

For development mode, one may want to install from the cloned source tree, in this case do an inplace editable install with:

    pip3 install -e '.[dev]'
    pip3 install -e './easytracker-python[dev]'
    pip3 install -e './easytracker-gdb[dev]'

For convenience, one may run instead:

    make develop

Note that `make develop` is not done as part of `make all` as it may conflict with out of source packages installation.

Also, in order to test provided examples in the `tools` directory, setup `extra-packages` with:

    export PYTHONPATH=$PWD/extra-packages
    pip3 install -r extra-packages/requirements.txt

Normally after these installation steps one may run and pass tests with:

    make -C tests

Note that one may additionally do:

- [for tools examples] run `make all`
- [for `generic/test_array` tool only] run `make rust` in the top repository folder to compile the `Rust` inferiors (`rustc` is hence needed)

Refer to the [Developer Documentation](#developer-documentation) below for contributing to `easyrtracker`.


## Tools provided in this repository

The `tools` folder is made of four folders:

- `c`: contains tools for the visualisation of `C` programs only
- `generic`: contains tools working on `C`, `Python` and `Rust` programs
- `python`: contains tools working on `Python` programs only
- `riscv`: contains tools working on `RISC-V` programs only

The following sections quickly describes some of these tools.

### Minimal print tool

This really simple tool executes the inferior code step by step and prints all the variables in the stack.
Its purpose is to demonstrate that the boilerplate code for `easytracker` is really small.

```cd tools/generic/easiest_tool```

```python easiest.py prog.py``` (Python code example)

```python easiest.py prog``` (C code example)


### Generic Array

This tool shows the same code with a small loop and permutation in Python, Rust and C.

```cd tools/generic/test_array/```

```python test_array.py python```

```python test_array.py C```

```python test_array.py Rust```

## Stack heap visualization

This generic tool shows the evolution of the stack and the heap during the execution of a program.
You can find several examples, after running each of them, the memory is drawn for every step on `stack_heap.svg` images that you can open and see.

```cd tools/generic/stack_heap_visualizator/```

```python stack_heap_visualizator.py everything_ref_int.py```

```python stack_heap_visualizator.py everything_ref_list.py```

```python stack_heap_visualizator.py everything_ref_params_int.py```


## Recursive function visualization

This tool needs a bit more setup to be run so you can read the dedicated `README.md` in `tools/python/rec_visualizator/`

## Specialized algorithm visualization

We made three tools to display the evolution of three different algorithms in Python, this can be found in the directory `tools/python/custom_visualizators/`.
All of them updates the image `common.svg` so when you start the first tool you can open this picture in an image viewer, it will be updated after each step of tool.

```cd tools/python/custom_visualizators/```

```python sort_visu.py``` (the source code is `sort.py`)

```python linked_list_visu.py``` (the source code is `linked_list.py`)

```python mergesort_visu.py``` (the source code is `mergesort.py`)


# Developer documentation

## Dev branches

Developments must be done and pushed in `dev/<user>/xxx` branches where `<user>` is the GitLab login name.
Once a new feature or bug fix is completed and tested in its dedicated branch, merge requests are created for integration in the `main` branch.

To get accepted, merge requests **must pass** all the tests launched with `make test` in the `test` folder.

To help running tests before doing the merge request, developers ars strongly encourage to configure a `git` client side hook as described below.
Moreover, the gitlab project is configured to run `make test` every time a `push` is performed.

## Git hooks

The `.githooks` directory contains client side hooks.
**You must** set up these client sides hooks locally on your machine using the following command launched from the root of the repository :

```console
> git config core.hooksPath .githooks
```

## Tagging stable package versions

When new fixes or functionalities are added to one of the `easytracker-base/python/gdb` package
one may tag the versions in order to further publish the pytohn package.

The tag naming convention follows the SemVer convention of `prefix-vX.Y.Z`
where update of `Y` (minor) should be done on new functionalitties, update of `X` (major)
on important and non backward compatible changes and update of `Z` (fix) for the rest.

For each package the following convention for tag must be followed:
- easytracker-base: tag `base-vX.Y.Z`
- easytracker-python: tag `python-vX.Y.Z`
- easytracker-gdb: tag `gdb-vX.Y.Z`

The tag must be pushed on a revision present on the left-hand side of the `main` branch,
i.e. after pull request branches are merged and the `main` branch tests have passed on the
GitLab CI.


## Publishing new package versions

Actually, as soon a a tag is pushed with the convention described above, a job in
GitLab CI will automatically publish to the package repository at
https://gitlab.inria.fr/CORSE/easytracker/-/packages.

Refer to the `.gitlab-ci.yaml` file for details on how tags are publised.

Note that the CI job relies on a secure file `.pypirc` configuration
uploaded to GitLab and containing a project API token
in order to publish the python packages.
The token may expire and the secure file should be recreated and uploaded
every year. Refer to the description of the `.pypirc` file below in order
to recreate this file.

Though in the case where manual publication may be necessary, one may build
the package distribution and upload to the GitLab package repository as described below:

- first ensure that you have put one of the `base-v*`, `python-v*` or `gdb-v*` tag and pushed the
  tag to the remote GitLab repository
- then update package distributions with: `make dist`
- then verify newly prepared distributions with: `make dist-install && make -C tests`, note that
  this will override development mode (one may switch back to development mode with `make develop`)
- last push the distributions with: `make dist-upload` (ref below for the configuration)
- the packages for which the versions were incremented are updated on GitLab and visible through:
  https://gitlab.inria.fr/CORSE/easytracker/-/packages. Verify that the corresponding package
  update date is correct.

In order to push the new distributions on GitLab, one must setup write permission on the package
repository:

- First create an API access token on GitLab at https://gitlab.inria.fr/-/profile/personal_access_tokens
  and set the `api` permission,
  a token key should be visible just after creation (`xxxx-YYYYYYYYY` for instance)
- Then setup the repository name/url and access token in the `$HOME/.pypirc` as shown below

Extract of `$HOME/.pypirc` suitable for pushing to the `easytracker` repository:

    [distutils]
    index-servers = easytracker  # repository name used when: twine upload --repository <name>

    [easytracker]
    repository = https://gitlab.inria.fr/api/v4/projects/corse%2Feasytracker/packages/pypi/
    username = API  # always use the username 'API'
    password = xxxx-YYYYYYYYY  # paste here your token key
