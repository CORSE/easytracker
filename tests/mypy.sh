#!/usr/bin/env bash
#
# mypy.sh: simple wrapper for mypy, pass configuration and
# explicit list of folders to check
#
set -euo pipefail

dir="$(dirname "$0")"

cd "$dir"/..

include=(
    easytracker/
    easytracker-gdb/
    easytracker-python/
    extra-packages/
    tools/
    tests/unit_tests/test_model/python
    tests/unit_tests/test_model/gdb
    tests/unit_tests/test_trackers/python
    tests/unit_tests/test_trackers/gdb
    tests/integration_tests
)

exclude=(
)

# Add path to local packages and use --explicit-package-bases --namespace-packages
# Otherwise:
# - mypy may not found editable packages installed with pip -e ... or PYTHONPATH
# - mypy may complain about duplicate module names when two basenames are identical
# - note that the easytracker module is in the topdir, hence the '.' in MYPYPATH
MYPYPATH=".:./easytracker-gdb:./easytracker-python:./extra-packages"
MYPY_OPTS="--explicit-package-bases --namespace-packages --config-file=tests/mypy.ini"
MYPY_EXCL="$(echo "${exclude[@]}"|sed 's/\(\S\S*\)/--exclude \1/g')"

if [ $# -gt 0 ]; then
    echo "running mypy version $(mypy --version)"
    ([ -z "${MYPY_DEBUG-}" ] || set -x; MYPYPATH="$MYPYPATH" mypy $MYPY_OPTS $MYPY_EXCL "$@")
else
    for i in "${include[@]}"; do
	files=($(git ls-files "$i"/'*.py'))
        echo "running mypy version $(mypy --version) on $i"
        path="$i"
        [ "$path" != "easytracker/" ] || path="." # specific case of easytracker whose base path is '.'
        ([ -z "${MYPY_DEBUG-}" ] || set -x; MYPYPATH="$path:$MYPYPATH" mypy $MYPY_OPTS $MYPY_EXCL "${files[@]}")
    done
fi


