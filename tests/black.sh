#!/usr/bin/env bash
#
# Run black on all python files in repo except generated files
#
set -euo pipefail
cd "$(dirname "$0")"/..

opt="${1-check}"
case "${1-check}" in
    check) black_opt=--check
	   ;;
    diff) black_opt="--diff --check"
	  ;;
    format)
	black_opt=
	;;
    *) echo "ERROR: unexpeted argument: $opt" >&2; exit 1
       ;;
esac

r=0
git ls-files '*.py' | sed '/\/tests\/refs\//d' | sed '/docs\/site\//d' | xargs black $black_opt || r=1

if [ "$opt" != format -a "$r" != 0 ]; then
    echo "ERROR: reindent with: $0 format (or view diff with $0 diff)" >&2
fi
exit "$r"
