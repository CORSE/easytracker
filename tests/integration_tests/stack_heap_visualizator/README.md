Integration tests for `stack_heap_visualizator`
==============

This folder contains integration tests for the `stack_heap_visualizator` generic tool.

Each integration test consist in launching the tool with particular arguments and to compare the generated images with previously generated and human validated reference images.

Run with:

    ./test.sh
    # or
    make test