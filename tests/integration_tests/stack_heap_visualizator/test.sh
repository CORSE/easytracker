#!/usr/bin/env bash
set -euo pipefail

! [ -t 2 ] || progress=--progress

# Get paths
full_path=$(realpath $0)
dir="$(dirname "$full_path")"
stack_heap_visualizator_dir=$(realpath -e "${dir}/../../../tools/generic/stack_heap_visualizator")
out_dir=${dir}/out
ref_dir=${dir}/refs

# Compile C inferiors
make -C ${stack_heap_visualizator_dir}/inferiors/c

declare -a arguments=(
	"${stack_heap_visualizator_dir}/inferiors/python/everything_ref_params_int.py --image_name stack_heap.dot"
  "${stack_heap_visualizator_dir}/inferiors/python/everything_ref_params_int.py --showaddresses --image_name stack_heap.dot"
  "${stack_heap_visualizator_dir}/inferiors/python/everything_ref_list.py --image_name stack_heap.dot"
  "${stack_heap_visualizator_dir}/inferiors/python/everything_ref_list.py --showaddresses --image_name stack_heap.dot"
  "${stack_heap_visualizator_dir}/inferiors/python/everything_ref_list.py --inlineprimitives --image_name stack_heap.dot"
  "${stack_heap_visualizator_dir}/inferiors/python/everything_ref_list.py --inlineprimitives --showaddresses --image_name stack_heap.dot"
  "${stack_heap_visualizator_dir}/inferiors/c/simple --image_name stack_heap.dot"
  "${stack_heap_visualizator_dir}/inferiors/c/heap_pointer --image_name stack_heap.dot"
 	"${stack_heap_visualizator_dir}/inferiors/c/heap_pointer --showaddresses --image_name stack_heap.dot"
 	"${stack_heap_visualizator_dir}/inferiors/c/stack_pointer --image_name stack_heap.dot"
 	"${stack_heap_visualizator_dir}/inferiors/c/stack_pointer --showaddresses --image_name stack_heap.dot"
 	"${dir}/inferiors/python/test_dict.py --image_name stack_heap.dot"
  "${dir}/inferiors/python/test_dict.py --showaddresses --image_name stack_heap.dot"
 	"${dir}/inferiors/python/test_dict.py --inlineprimitives --image_name stack_heap.dot"
  "${dir}/inferiors/python/test_dict.py --inlineprimitives --showaddresses --image_name stack_heap.dot"
	"${dir}/inferiors/python/test_class.py --image_name stack_heap.dot"
	"${dir}/inferiors/python/test_class.py --showaddresses --image_name stack_heap.dot"
	"${dir}/inferiors/python/test_class.py --inlineprimitives --image_name stack_heap.dot"
	"${dir}/inferiors/python/test_class.py --inlineprimitives --showaddresses --image_name stack_heap.dot"
  "${stack_heap_visualizator_dir}/inferiors/c/struct --image_name stack_heap.dot"
  "${stack_heap_visualizator_dir}/inferiors/c/struct --showaddresses --image_name stack_heap.dot"
)

declare -a remove_regexps=("s/val_0x[0-9a-f]+/inst_XXX/g"
						   "s/var_0x[0-9a-f]+/var_XXX/g"
						   "s/heap_pointer_src_0x[0-9a-f]+/heap_pointer_src_XXX/g"
						   "s/>0x[0-9a-f]+<\/TD>/>XXX<\/TD>/g"
						   "s/<TD ALIGN=\"center\" PORT=\"a_main_var_XXX\" BGCOLOR=\"chartreuse\">(0x[0-9a-f]+|-?[0-9]+|×|●)<\/TD>/<TD ALIGN=\"center\" PORT=\"a_main_var_XXX\" BGCOLOR=\"chartreuse\">XXX<\/TD>/g"
						   "s/<TD ALIGN=\"center\" PORT=\"b_main_var_XXX\" BGCOLOR=\"chartreuse\">(0x[0-9a-f]+|-?[0-9]+|×|●)<\/TD>/<TD ALIGN=\"center\" PORT=\"b_main_var_XXX\" BGCOLOR=\"chartreuse\">XXX<\/TD>/g"
						   "s/<TD ALIGN=\"center\" PORT=\"c_main_var_XXX\" BGCOLOR=\"chartreuse\">(0x[0-9a-f]+|-?[0-9]+|×|●)<\/TD>/<TD ALIGN=\"center\" PORT=\"c_main_var_XXX\" BGCOLOR=\"chartreuse\">XXX<\/TD>/g"
						   "s/<TD ALIGN=\"center\" PORT=\"d_main_var_XXX\" BGCOLOR=\"chartreuse\">(0x[0-9a-f]+|-?[0-9]+|×|●)<\/TD>/<TD ALIGN=\"center\" PORT=\"d_main_var_XXX\" BGCOLOR=\"chartreuse\">XXX<\/TD>/g"
						   "s/<TD ALIGN=\"center\" PORT=\"e_main_var_XXX\" BGCOLOR=\"chartreuse\">(0x[0-9a-f]+|-?[0-9]+|×|●)<\/TD>/<TD ALIGN=\"center\" PORT=\"e_main_var_XXX\" BGCOLOR=\"chartreuse\">XXX<\/TD>/g"
						   "s/<TD ALIGN=\"center\" PORT=\"f_main_var_XXX\" BGCOLOR=\"chartreuse\">(0x[0-9a-f]+|-?[0-9]+|×|●)<\/TD>/<TD ALIGN=\"center\" PORT=\"f_main_var_XXX\" BGCOLOR=\"chartreuse\">XXX<\/TD>/g"
						   "s/<TD ALIGN=\"center\" PORT=\"g_main_var_XXX\" BGCOLOR=\"chartreuse\">(0x[0-9a-f]+|-?[0-9]+|×|●)<\/TD>/<TD ALIGN=\"center\" PORT=\"g_main_var_XXX\" BGCOLOR=\"chartreuse\">XXX<\/TD>/g"
 						   "s/\(a=[0-9a-f]+/\(a=XXX/g"
 						   "s/b=-?[0-9a-f]+/\b=XXX/g"
 						   "s/field_two=.+\)/field_two=XXX\)/g"
)

rm -rf ${out_dir}
mkdir ${out_dir}
if [ -n "${REF-}" ]; then
	rm -rf ${ref_dir}
	mkdir ${ref_dir}
fi
count=0
for params in "${arguments[@]}"
do
	echo "Testing stack_heap_visualizator.py $(basename "${params}")"
	count_str=$(printf '%02d' "${count}")
	out_dir_count=${out_dir}/${count_str}
	mkdir ${out_dir_count}
	if [ -n "${REF-}" ]; then
		params="${params} -gsi"
	fi
	command="env EASYTRACKER_HEAP_INIT_VALUE=0 ${stack_heap_visualizator_dir}/stack_heap_visualizator.py ${params} -o ${out_dir_count}"
	echo "running $command"
	eval "$command"
	res=0

	ref_dir_count=${ref_dir}/${count_str}
	if [ -n "${REF-}" ]; then
		rm -rf ${ref_dir_count}
		mkdir ${ref_dir_count}
	fi
	img_count=1
	for image in "${out_dir_count}"/*-stack_heap.dot
	do
		image_count_str=$(printf '%03d' "${img_count}")
		image_name="$(basename "${image}")"
		image_before_sed="${out_dir_count}/${image_name}-before-sed.dot"
		cp "${image}" "${image_before_sed}"
		ref_image="${ref_dir_count}/${image_name}"
		for regexp in "${remove_regexps[@]}"
		do
		    sed -i -E "${regexp}" "${image}"
		done
		if [ -n "${REF-}" ]; then
			echo "updating refs/${count_str}/${image_name} with out/${count_str}/${image_name}"
			cp "${image}" "${ref_image}"
			dot -Tsvg ${image_before_sed} -o ${ref_image}.jpg
			src_image="${out_dir_count}/${image_count_str}-source.jpg"
			convert "${ref_image}.jpg" "${src_image}" +append "${ref_image}.jpg"
		else
			echo "running diff out/${count_str}/${image_name} refs/${count_str}/${image_name}"
			diff "$image" $ref_image | head -10 || res=1
			if [ $res -ne 0 ]; then
				echo "Test FAILED"
				echo "While running command: $command"
				exit 1
			else
				echo "Test PASSED"
			fi
		fi
		((img_count=img_count+1))
	done
	((count=count+1))
done
exit $res

