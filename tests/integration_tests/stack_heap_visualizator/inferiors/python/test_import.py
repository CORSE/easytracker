"""A simple test for import"""


import lib  # type: ignore


def main():
    """Entry point"""
    a_variable = 7
    another_variable = lib.a_function()
    a_variable = a_variable + another_variable


if __name__ == "__main__":
    main()
