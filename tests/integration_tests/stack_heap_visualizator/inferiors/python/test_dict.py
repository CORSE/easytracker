"""A very simple inferior to test dictionary"""


def main():
    """Entry point"""
    empty_dict = {}
    non_empty_dict = {"Manu": 40, "Florent": "presque 42"}
    dict_of_non_primitives = {
        "Manu": {"Alexia": 9, "Maxime": 6},
        "Florent": ["Hugo", "Zoéline", "Manolé"],
    }
    print(empty_dict)
    print(non_empty_dict)
    print(dict_of_non_primitives)


if __name__ == "__main__":
    main()
