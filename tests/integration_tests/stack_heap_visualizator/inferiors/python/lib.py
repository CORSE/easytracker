"""A simple library module for test"""


def a_function():
    """A test function to be used in another module"""
    a_variable = 7
    return a_variable + 4
