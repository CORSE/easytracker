"""A simple test for classes"""


class Cell:
    """A cell of a linked list"""

    def __init__(self, value, next_cell=None) -> None:
        self.value = value
        self.next = next_cell


def main():
    """Entry point"""
    a_linked_list = Cell(1, Cell(2, Cell(17)))
    print(a_linked_list.next.next.value)  # type:ignore


if __name__ == "__main__":
    main()
