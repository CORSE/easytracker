"""A simple test for classes"""

import dataclasses
import typing


@dataclasses.dataclass
class Cell:
    """A cell of a linked list"""

    value: int
    next: typing.Optional["Cell"] = None


def main():
    """Entry point"""
    a_linked_list = Cell(1, Cell(2, Cell(17)))
    print(a_linked_list.next.next.value)  # type:ignore


if __name__ == "__main__":
    main()
