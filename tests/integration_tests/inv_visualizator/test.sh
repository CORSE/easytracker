#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"
absdir="$(readlink -e "$dir")"

outdir="out"
refdir="ref"
tools_dir="$absdir/../../../tools"
testdir="$tools_dir/python/inv_visualizator"

rm -rf "$outdir/"
mkdir -p "$outdir/"
pushd "$outdir/" >/dev/null
echo "Executing test from $PWD: $testdir/inv_visualizator.py -li i -ri j $testdir/inferiors/sort_array_of_binaries.py sort array"
"$testdir"/inv_visualizator.py -li i -ri j "$testdir"/inferiors/sort_array_of_binaries.py sort array
popd >/dev/null

if [ "${REF-}" = 1 ]; then
    # Actually there we test only that the images have been generated
    echo "REF: no reference to update"
    exit
fi

sources_count=$(ls "$outdir"/source*.jpg 2>/dev/null | wc -l || true)
arrays_count=$(ls "$outdir"/array*.svg 2>/dev/null | wc -l || true)

if [ -z "$arrays_count" -o "$arrays_count" = 0 ]; then
    echo "FAILED: no image generated in dir: $outdir/"
    exit 1
fi

if [ "$arrays_count" != "$sources_count" ]; then
    echo "FAILED: array images and source images count mismatch $arrays_count != $sources_count"
    exit 1
fi
echo "SUCCESS: $arrays_count (arrays) + $sources_count (sources) images generated"
