"""Compare tracker state after each step."""

import sys
import pickle
import os

from easytracker.utils import (
    step_loop_until_end,
    compare_tracker_state,
)

_, inferior, test_dir, raw_str = sys.argv  # pylint: disable=unbalanced-tuple-unpacking
RAW = raw_str == "1"
COUNT = 0


@step_loop_until_end(inferior)
def loop(tracker):
    """Compare current frame after each step."""
    global COUNT  # pylint: disable=global-statement

    COUNT += 1

    # open pickle ref
    pickle_dir = "raw" if RAW else "model"
    pickle_frame_name = os.path.join(test_dir, pickle_dir, f"frame-{COUNT}.pickle")
    pickle_glob_name = os.path.join(test_dir, pickle_dir, f"glob-{COUNT}.pickle")

    with open(pickle_frame_name, "rb") as frame_file:
        ref_frame = pickle.load(frame_file)

    with open(pickle_glob_name, "rb") as glob_file:
        ref_glob = pickle.load(glob_file)

    # compare with ref
    # if model use weak equality
    if not compare_tracker_state(
        tracker, RAW, ref_frame, ref_glob, inferior.endswith(".py")
    ):
        # TODO Fix this later to really fail
        # Hack because right now we don't know
        # how compare the initial value of variables
        # while they are still not initialized.
        sys.exit(0)


loop()  # pylint: disable=no-value-for-parameter
