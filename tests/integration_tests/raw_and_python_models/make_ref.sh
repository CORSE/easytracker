#!/usr/bin/env bash
#
# Takes an inferior as argument, if the argument is a C file, it is compiled before
# The basename is used for the directory and it creates the pickled ref of the easytracker model

filename=$(basename $1)
dir_name="${filename%.*}"

REF_TOOL_PATH=../../tools/generic/debug/model/debug.py

# empty the old ref directory
mkdir -p "$dir_name"/raw
mkdir -p "$dir_name"/model
rm "$dir_name"/raw/*.pickle
rm "$dir_name"/model/*.pickle

# if this is a C file compile it in a directory
if [ "${filename: -2}" == ".c" ]
then
    mkdir -p "$dir_name"
    gcc -g $1 -o "$dir_name"/a.out
fi

# run the debug program
python "$REF_TOOL_PATH" "$1" -o "$dir_name"/raw -p -r
python "$REF_TOOL_PATH" "$1" -o "$dir_name"/model -r





