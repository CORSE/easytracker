#!/usr/bin/env bash
#
# run_tests.sh:
#
# For C:
# Goes around every folder in the c directory, compile the c file to a.out and run the test.py file.
# if the test.py file exit code is 0 the test succeded
#
# For Python:
# TODO

stack_heap_visu_dir=../tools/generic/stack_heap_visualizator/inferiors
python_files="$stack_heap_visu_dir"/python/*.py
c_dir="$stack_heap_visu_dir"/c
c_files="$c_dir/simple $c_dir/stack_pointer $c_dir/heap_point"

if [ $# -eq 1 ]
then
    if [ "$1" == "ref" ]; then
        # Make ref

        # Python
        for file in $python_files ; do
            ./make_ref.sh $file
        done

        for file in $c_files ; do
            echo $file
            ./make_ref.sh $file
        done

        exit
    fi
fi

# Normal testing
test_file () {
  # check python test exit code
  python easytracker_tests/test.py "$1" easytracker_tests/"$2" $3
  res=$?
  echo $res
  if [ $3 -eq 1 ]
  then
      kind="raw"
  else
      kind="model"
  fi
  if [ $res -eq 0 ]
  then
      echo -e "\e[32mPASSED\e[0m $(basename $2) $kind"
  else
      echo -e "\e[31mFAILED\e[0m $(basename $2) $kind"
  fi
}

echo C files: "$c_files"
# C TESTS
for f in $c_files ; do
    dir=$(basename $f)

    test_file $f $dir 0
    test_file $f $dir 1
done
echo Python files: "$python_files"
# Python TESTS
for f in $python_files ; do
    full_name=$(basename $f)
    dir=$(echo "$full_name" | cut -f 1 -d '.')
    test_file $f $dir 0
    test_file $f $dir 1
done