EasyTracker Tests
==============

See the `requirements_dev.txt` file in the root directory for the dependencies required to launch the tests.

Run all tests in the directory with:

    make test

Rebuild references for comparative tests with:

    make ref

Clean test directory with:

    make clean


Integration tests
-----------------

Integration tests are located in the `integration_tests` folder.
See the `README.md` file in each subfolder of the `integration_tests` folder for details about a particular integration test.

Run integration tests with:

    make test-inte


Unitary tests
-----------------

Unitary tests are located in the `unit_tests` folder and use the `pytest` tool which discovers automatically test function inside this `unit_tests` directory.

Run unitary tests with:

    make test-unit

In order to add unitary test, add a new `.py` file with some functions named `test_*` containing tests with asserts.

For instance adding a new test may be as simple as adding a `test_parameter.py` file content:

    from computation import Parameter

    def test_parameter():
        param = Parameter("some")
        assert(param.name == "some")

Ref to `pytest` documentation and existing tests for more details.


Static checkers
--------------

We use three static checkers:

- code formatting is done by `black`;
- Python syntactic checks are done by `pylint`;
- type checks are done by `mypy`.

For each checker there is in the directory a simple wrapper script which contains the list of folders to check.

Run code formatting checks with:

    ./black.sh
    # or
    make test-black

Run syntactic checks with:

    ./pylint.sh
    # or
    make test-pylint

Run type checks with:

    ./mypy.sh
    # or
    make test-mypy

You can also run the three static checkers at once with:

    make test-static

