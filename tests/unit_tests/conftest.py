"""Fixtures for tracker tests"""

import tempfile
import os
import subprocess
from pytest import fixture
from pytest import mark
from easytracker import init_tracker


parametrize = mark.parametrize


@fixture
def gdb_tracker(c_sources):
    """Creates and compiles the inferior sources passed as string"""
    tmp_exe_fd, tmp_exe_fpath = tempfile.mkstemp(suffix=".exe")
    os.close(tmp_exe_fd)
    tmps_c = [tempfile.mkstemp(suffix=".c") for _ in c_sources]
    for tmp_c_fd, _ in tmps_c:
        os.close(tmp_c_fd)
    try:
        for (_, tmp_c_fpath), c_source in zip(tmps_c, c_sources):
            with open(tmp_c_fpath, "w", encoding="utf8") as tmpf:
                tmpf.write(c_source)
        subprocess.check_output(
            args=["gcc"] + [x[1] for x in tmps_c] + ["-g", "-O0", "-o", tmp_exe_fpath]
        )
        track = init_tracker("GDB")
        track.load_program(tmp_exe_fpath)
        yield track
        track.resume()
        track.terminate()
    finally:
        os.remove(tmp_exe_fpath)
        for _, tmp_c_fpath in tmps_c:
            os.remove(tmp_c_fpath)


@fixture
def python_tracker(file, lineno):
    """Creates a Python tracker from given file and nexting until given line"""

    tracker = init_tracker("python")
    tracker.load_program(file)
    tracker.start()
    while tracker.next_lineno != lineno:
        tracker.next()
    yield tracker
    tracker.resume()
    tracker.terminate()
