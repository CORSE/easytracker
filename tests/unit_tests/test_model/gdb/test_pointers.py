"""Test pointers types"""

# pylint: disable=redefined-outer-name

from pytest import fixture, mark
from easytracker import AbstractType, Location

INFERIOR = [
    """#include <stdlib.h>
int main() {
    int i = 5;
    int* pointer_to_stack = &i;
    int* pointer_to_heap_single = malloc(sizeof(int));
    *pointer_to_heap_single = 7;
    int* pointer_to_heap_array = malloc(4*sizeof(int));
    return 8;
}
"""
]


@fixture
def tracker_at_return(gdb_tracker):
    """Start and go to the end"""
    gdb_tracker.start()
    gdb_tracker.next()
    gdb_tracker.next()
    gdb_tracker.next()
    gdb_tracker.next()
    gdb_tracker.next()
    gdb_tracker.next()
    return gdb_tracker


@mark.parametrize("c_sources", [INFERIOR])
def test_pointer_to_stack(tracker_at_return):
    """Test a pointer to an int in the stack"""
    var = tracker_at_return.get_current_frame(as_raw_python_objects=False).variables[
        "pointer_to_stack"
    ]
    assert var is not None
    assert var.value.abstract_type == AbstractType.REF
    assert var.value.location == Location.STACK
    assert " ".join(var.value.language_type.split()).replace(" ", "") == "int*"

    referenced_value = var.value.content
    assert referenced_value.abstract_type == AbstractType.PRIMITIVE
    assert referenced_value.location == Location.STACK
    assert referenced_value.content == 5
    assert referenced_value.language_type == "int"


@mark.parametrize("c_sources", [INFERIOR])
def test_pointer_to_heap_single(tracker_at_return):
    """Test a pointer to a single int in the heap"""
    var = tracker_at_return.get_current_frame(as_raw_python_objects=False).variables[
        "pointer_to_heap_single"
    ]
    assert var is not None
    assert var.value.abstract_type == AbstractType.REF
    assert var.value.location == Location.STACK
    assert " ".join(var.value.language_type.split()).replace(" ", "") == "int*"

    referenced_value = var.value.content
    assert referenced_value.abstract_type == AbstractType.PRIMITIVE
    assert referenced_value.location == Location.HEAP
    assert referenced_value.content == 7
    assert referenced_value.language_type == "int"


@mark.parametrize("c_sources", [INFERIOR])
def test_pointer_to_heap_array(tracker_at_return):
    """Test a pointer to an array of ints in the heap"""
    var = tracker_at_return.get_current_frame(as_raw_python_objects=False).variables[
        "pointer_to_heap_array"
    ]
    assert var is not None
    assert var.value.abstract_type == AbstractType.REF
    assert var.value.location == Location.STACK
    assert " ".join(var.value.language_type.split()).replace(" ", "") == "int*"

    referenced_value = var.value.content
    assert referenced_value.abstract_type == AbstractType.LIST
    assert referenced_value.location == Location.HEAP
    assert isinstance(referenced_value.content, list)
    assert len(referenced_value.content) == 4
    for int_value in referenced_value.content:
        assert int_value.abstract_type == AbstractType.PRIMITIVE
        assert isinstance(int_value.content, int)
        assert int_value.location == Location.HEAP
        assert int_value.language_type == "int"
