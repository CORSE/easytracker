"""Test primitive types"""

# pylint: disable=redefined-outer-name

from pytest import fixture, mark
from easytracker import AbstractType, Location

INFERIOR = [
    """
int main() {
    int a = 11;
    float b = 5.0;
    char c = 'M';
    char* d = "Selva";
    return 8;
}
"""
]


@fixture
def tracker_at_return(gdb_tracker):
    """Start and go to the end"""
    gdb_tracker.start()
    gdb_tracker.next()
    gdb_tracker.next()
    gdb_tracker.next()
    gdb_tracker.next()
    gdb_tracker.next()
    return gdb_tracker


@mark.parametrize("c_sources", [INFERIOR])
def test_int(tracker_at_return):
    """Test that a is an int"""
    int_var = tracker_at_return.get_current_frame(
        as_raw_python_objects=False
    ).variables["a"]
    assert int_var is not None
    assert int_var.value.abstract_type == AbstractType.PRIMITIVE
    assert int_var.value.content == 11
    assert int_var.value.location == Location.STACK
    assert int_var.value.language_type == "int"


@mark.parametrize("c_sources", [INFERIOR])
def test_float(tracker_at_return):
    """Test that b is a float"""
    float_var = tracker_at_return.get_current_frame(
        as_raw_python_objects=False
    ).variables["b"]
    assert float_var is not None
    assert float_var.value.abstract_type == AbstractType.PRIMITIVE
    assert float_var.value.content == 5.0
    assert float_var.value.location == Location.STACK
    assert float_var.value.language_type == "float"


@mark.parametrize("c_sources", [INFERIOR])
def test_char(tracker_at_return):
    """Test that c is a char"""
    char_var = tracker_at_return.get_current_frame(
        as_raw_python_objects=False
    ).variables["c"]
    assert char_var is not None
    assert char_var.value.abstract_type == AbstractType.PRIMITIVE
    assert char_var.value.content == "M"
    assert char_var.value.location == Location.STACK
    assert char_var.value.language_type == "char"


@mark.parametrize("c_sources", [INFERIOR])
def test_string(tracker_at_return):
    """Test that d is a string"""
    str_var = tracker_at_return.get_current_frame(
        as_raw_python_objects=False
    ).variables["d"]
    assert str_var is not None
    assert str_var.value.abstract_type == AbstractType.REF
    assert str_var.value.location == Location.STACK
    assert " ".join(str_var.value.language_type.split()).replace(" ", "") == "char*"

    # TODO : change GDB tracker so that d is a String
    referenced_value = str_var.value.content
    print(referenced_value)
    assert referenced_value.content == "S"
    # assert referenced_value.location == Location.GLOBAL TODO: to be fixed in GDB tracker
    assert referenced_value.language_type == "char"
