# pylint: disable=redefined-outer-name

"""Test module globals"""

import pathlib
from pytest import fixture
from easytracker import init_tracker, Variable, Value, AbstractType, Location


def main():
    """Program's entry point"""
    print("Nothing to do, since we just check globals")


@fixture
def tracker():
    """Fixture creating the tracker"""
    track = init_tracker("python")
    track.load_program(pathlib.Path(__file__).resolve())
    track.start()
    while track.next_lineno != 12:
        track.next()
    yield track
    track.resume()
    track.terminate()


def test_module(tracker):
    """Test MODULE abstract type"""
    globs = tracker.get_global_variables(as_raw_python_objects=False)
    assert isinstance(globs, dict)
    pathlib_var = globs["pathlib"]
    assert isinstance(pathlib_var, Variable)
    pathlib_val = pathlib_var.value
    assert isinstance(pathlib_val, Value)
    assert pathlib_val.abstract_type == AbstractType.REF
    assert pathlib_val.location == Location.HEAP
    referenced_value = pathlib_val.content
    assert referenced_value.abstract_type == AbstractType.UNKNOWN
    assert isinstance(referenced_value.content, str)

    for glob in [
        "fixture",
        "init_tracker",
        "Variable",
        "Value",
        "AbstractType",
        "Location",
    ]:
        assert glob in globs
