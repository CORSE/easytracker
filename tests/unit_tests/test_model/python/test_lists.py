# pylint: disable=redefined-outer-name

"""Test list types for Python"""

import pathlib
from pytest import mark
from easytracker import AbstractType


def main():
    """Program's entry point"""

    list_var = [1, 1.0, "1"]
    tuple_var = tuple([1, 1.0, "1"])
    reclist_var = []
    reclist_var.append(reclist_var)
    reclist_var.append(1)
    print(f"{list_var = }, {tuple_var = }, {reclist_var = } ")


def check_ref_to_list(var):
    """Check that var is a REF to a LIST"""
    assert var is not None
    assert var.value.abstract_type == AbstractType.REF
    referenced_value = var.value.content
    assert referenced_value is not None
    assert referenced_value.abstract_type == AbstractType.LIST
    assert isinstance(referenced_value.content, tuple)
    return referenced_value


def check_list_or_tuple(var):
    """Check that var matches abstract LIST [1, 1.0, "1"]"""
    referenced_value = check_ref_to_list(var)
    assert len(referenced_value.content) == 3
    assert referenced_value.content[0].abstract_type == AbstractType.REF
    assert referenced_value.content[0].content.abstract_type == AbstractType.PRIMITIVE
    assert referenced_value.content[0].content.content == 1
    assert referenced_value.content[1].abstract_type == AbstractType.REF
    assert referenced_value.content[1].content.abstract_type == AbstractType.PRIMITIVE
    assert referenced_value.content[1].content.content == 1.0
    assert referenced_value.content[2].abstract_type == AbstractType.REF
    assert referenced_value.content[2].content.abstract_type == AbstractType.PRIMITIVE
    assert referenced_value.content[2].content.content == "1"


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [15])
def test_tuple(python_tracker):
    """Test LIST primitive for tuple"""
    tuple_var = python_tracker.get_current_frame(as_raw_python_objects=False).variables[
        "tuple_var"
    ]
    check_list_or_tuple(tuple_var)

    assert tuple_var.value.to_human_str().startswith("ref to 0x")
    assert tuple_var.value.to_human_str(inline_ref=True) == '[1, 1.0, "1"]'


def check_raw_tuple_list(raw_var):
    """Checks that given raw var is a LIST [1, 1.0, '1']"""
    assert raw_var is not None
    value = raw_var.value
    assert isinstance(value, list)
    assert len(value) == 3
    assert value[0] == 1
    assert value[1] == 1.0
    assert value[2] == "1"


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [15])
def test_tuple_raw(python_tracker):
    """Test LIST primitive for tuple in RAW model"""
    tuple_var = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "tuple_var"
    ]
    check_raw_tuple_list(tuple_var)


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [14])
def test_list(python_tracker):
    """Test LIST primitive for list"""
    list_var = python_tracker.get_current_frame(as_raw_python_objects=False).variables[
        "list_var"
    ]
    check_list_or_tuple(list_var)

    assert list_var.value.to_human_str().startswith("ref to 0x")
    assert list_var.value.to_human_str(inline_ref=True) == '[1, 1.0, "1"]'


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [15])
def test_list_raw(python_tracker):
    """Test LIST primitive for list in RAW model"""
    list_var = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "tuple_var"
    ]
    check_raw_tuple_list(list_var)


def check_reclist(tuple_var):
    """Check that recursive list matches [[...], 1] where element 0 points back to list"""
    referenced_value = check_ref_to_list(tuple_var)
    assert len(referenced_value.content) == 2
    assert referenced_value.content[0].abstract_type == AbstractType.REF
    assert (
        referenced_value.content[0].content is referenced_value
    )  # cycle to list value
    assert referenced_value.content[1].abstract_type == AbstractType.REF
    assert referenced_value.content[1].content.abstract_type == AbstractType.PRIMITIVE
    assert referenced_value.content[1].content.content == 1


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [18])
def test_reclist(python_tracker):
    """
    Test LIST primitive for a recursive list.
    The recursive reference points to the same Value object
    """
    reclist_var = python_tracker.get_current_frame(
        as_raw_python_objects=False
    ).variables["reclist_var"]
    check_reclist(reclist_var)

    assert reclist_var.value.to_human_str().startswith("ref to 0x")
    assert reclist_var.value.to_human_str(inline_ref=True) == "[[...], 1]"


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [18])
def test_reclist_raw(python_tracker):
    """
    Test LIST primitive for a recursive list in the RAW model
    The recursive reference points to the same list object
    """
    reclist_var = python_tracker.get_current_frame(
        as_raw_python_objects=True
    ).variables["reclist_var"]

    assert reclist_var is not None
    value = reclist_var.value
    assert isinstance(value, list)
    assert len(value) == 2
    assert value[0] == value
    assert value[1] == 1
