# pylint: disable=redefined-outer-name

"""Test all primitive types for Python"""

import pathlib
from pytest import mark
from easytracker import AbstractType


def main():
    """Program's entry point"""

    int_var = 2
    str_var = "Manu"
    float_var = 4.0
    print(f"{int_var = }, {str_var = }, {float_var = }")


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [14])
def test_int(python_tracker):
    """Test INT primitive"""
    int_var = python_tracker.get_current_frame(as_raw_python_objects=False).variables[
        "int_var"
    ]
    assert int_var is not None
    assert int_var.value.abstract_type == AbstractType.REF
    referenced_value = int_var.value.content
    assert referenced_value is not None
    assert referenced_value.abstract_type == AbstractType.PRIMITIVE
    assert referenced_value.content == 2


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [14])
def test_int_raw(python_tracker):
    """Test INT primitive in the raw model"""
    int_var = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "int_var"
    ]
    assert int_var is not None
    assert int_var.value == 2


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [15])
def test_str(python_tracker):
    """Test STR primitive"""
    str_var = python_tracker.get_current_frame(as_raw_python_objects=False).variables[
        "str_var"
    ]
    assert str_var is not None
    assert str_var.value.abstract_type == AbstractType.REF
    referenced_value = str_var.value.content
    assert referenced_value is not None
    assert referenced_value.abstract_type == AbstractType.PRIMITIVE
    assert referenced_value.content == "Manu"


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [15])
def test_str_raw(python_tracker):
    """Test STR primitive in the raw model"""
    str_var = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "str_var"
    ]
    assert str_var is not None
    assert str_var.value == "Manu"


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [16])
def test_float(python_tracker):
    """Test FLOAT primitive"""
    float_var = python_tracker.get_current_frame(as_raw_python_objects=False).variables[
        "float_var"
    ]
    assert float_var is not None
    assert float_var.value.abstract_type == AbstractType.REF
    referenced_value = float_var.value.content
    assert referenced_value is not None
    assert referenced_value.abstract_type == AbstractType.PRIMITIVE
    assert referenced_value.content == 4.0


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [16])
def test_float_raw(python_tracker):
    """Test FLOAT primitive in the raw model"""
    float_var = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "float_var"
    ]
    assert float_var is not None
    assert float_var.value == 4.0
