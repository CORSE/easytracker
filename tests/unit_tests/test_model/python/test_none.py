# pylint: disable=redefined-outer-name

"""Test all primitive types for Python"""

import pathlib
from pytest import mark
from easytracker import AbstractType


def main():
    """Program's entry point"""

    none_var = None
    print(f"{none_var = }")


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [14])
def test_none(python_tracker):
    """Test None"""
    int_var = python_tracker.get_current_frame(as_raw_python_objects=False).variables[
        "none_var"
    ]
    assert int_var is not None
    assert int_var.value.abstract_type == AbstractType.REF
    referenced_value = int_var.value.content
    assert referenced_value is not None
    assert referenced_value.abstract_type == AbstractType.NONE
    assert referenced_value.content is None


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [14])
def test_none_raw(python_tracker):
    """Test None in raw model"""
    int_var = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "none_var"
    ]
    assert int_var is not None
    assert int_var.value is None
