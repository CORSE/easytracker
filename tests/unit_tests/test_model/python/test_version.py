"""Test version information"""
import easytracker


def test_version():
    """Simple version tuple test"""
    assert hasattr(easytracker, "__version__")
    tuples = easytracker.__version__.split(".")
    major, minor, patch = [int(t) for t in tuples[:3]]
    assert major >= 1
    assert minor >= 0
    assert patch >= 0
