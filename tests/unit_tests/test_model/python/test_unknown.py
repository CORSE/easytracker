# pylint: disable=redefined-outer-name

"""Test unknown types for Python"""

import pathlib
from pytest import mark
from easytracker import AbstractType


class SlotsOnly:
    """A class with only slots"""

    __slots__ = "field1", "field2"

    def __init__(self, field1, field2) -> None:
        self.field1 = field1
        self.field2 = field2


def main():
    """Program's entry point"""

    slots_only_var = SlotsOnly("foo", 2)
    print(f"{slots_only_var = }")


def check_unknown_slots(unk_var, type_str, repr_str):
    """Check that the unknown type matches type and repr"""
    assert unk_var is not None
    assert unk_var.value.abstract_type == AbstractType.REF
    referenced_value = unk_var.value.content
    assert referenced_value is not None
    assert referenced_value.abstract_type == AbstractType.UNKNOWN
    assert referenced_value.language_type == type_str
    assert isinstance(referenced_value.content, str)
    assert referenced_value.content.startswith(repr_str)


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [24])
def test_unknown_slots(python_tracker):
    """Test UNKNOWN type for the instance of our class with slots only"""
    unk_var = python_tracker.get_current_frame(as_raw_python_objects=False).variables[
        "slots_only_var"
    ]
    check_unknown_slots(
        unk_var,
        "<class 'test_unknown.SlotsOnly'>",
        "<test_unknown.SlotsOnly object at",
    )


def check_unknown_slots_raw(unk_var, repr_str):
    """Check that the unknown raw type matches type and repr"""
    assert unk_var is not None
    assert isinstance(unk_var.value, str)
    assert unk_var.value.startswith(repr_str)


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [24])
def test_unknown_slots_raw(python_tracker):
    """Test UNKNOWN type for the instance of our class with slots only"""
    unk_var = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "slots_only_var"
    ]
    check_unknown_slots_raw(
        unk_var,
        "<test_unknown.SlotsOnly object at",
    )


if __name__ == "__main__":
    main()
