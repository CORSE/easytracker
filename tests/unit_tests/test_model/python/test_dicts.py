# pylint: disable=redefined-outer-name

"""Test DICT types for Python"""

import pathlib
from pytest import mark
from easytracker import AbstractType


def main():
    """Program's entry point"""

    dict_var = {"un": 1, 1.0: "un", "1": 1}
    recdict_var = {}
    recdict_var["myself"] = recdict_var
    recdict_var[1] = "un"
    print(f"{dict_var = }, {recdict_var = }")


def check_dict(dict_var):
    """Check that dict abstract ref matches {"un": 1, 1.0: "un", "1": 1}"""
    assert dict_var is not None
    assert dict_var.value.abstract_type == AbstractType.REF
    referenced_value = dict_var.value.content
    assert referenced_value is not None
    assert referenced_value.abstract_type == AbstractType.DICT
    assert isinstance(referenced_value.content, dict)
    assert len(referenced_value.content) == 3
    items = list(referenced_value.content.items())
    assert items[0][0].abstract_type == AbstractType.REF
    assert items[0][0].content.abstract_type == AbstractType.PRIMITIVE
    assert items[0][0].content.content == "un"
    assert items[0][1].abstract_type == AbstractType.REF
    assert items[0][1].content.abstract_type == AbstractType.PRIMITIVE
    assert items[0][1].content.content == 1
    assert items[1][0].abstract_type == AbstractType.REF
    assert items[1][0].content.abstract_type == AbstractType.PRIMITIVE
    assert items[1][0].content.content == 1.0
    assert items[1][1].abstract_type == AbstractType.REF
    assert items[1][1].content.abstract_type == AbstractType.PRIMITIVE
    assert items[1][1].content.content == "un"
    assert items[2][0].abstract_type == AbstractType.REF
    assert items[2][0].content.abstract_type == AbstractType.PRIMITIVE
    assert items[2][0].content.content == "1"
    assert items[2][1].abstract_type == AbstractType.REF
    assert items[2][1].content.abstract_type == AbstractType.PRIMITIVE
    assert items[2][1].content.content == 1


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [17])
def test_dict(python_tracker):
    """Test DICT abstract type"""
    dict_var = python_tracker.get_current_frame(as_raw_python_objects=False).variables[
        "dict_var"
    ]
    check_dict(dict_var)

    assert dict_var.value.to_human_str().startswith("ref to 0x")
    assert (
        dict_var.value.to_human_str(inline_ref=True) == '{"un": 1, 1.0: "un", "1": 1}'
    )


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [17])
def test_dict_raw(python_tracker):
    """Test DICT abstract type in the raw model: {"un": 1, 1.0: "un", "1": 1}"""
    dict_var = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "dict_var"
    ]
    assert dict_var is not None
    value = dict_var.value
    assert value == {"un": 1, 1.0: "un", "1": 1}


def check_recdict(dict_var):
    """Check that recursive dict matches {"myself":{...}, 1:"un"} where element 0 points back to dict"""
    assert dict_var is not None
    assert dict_var.value.abstract_type == AbstractType.REF
    referenced_value = dict_var.value.content
    assert referenced_value is not None
    assert referenced_value.abstract_type == AbstractType.DICT
    assert isinstance(referenced_value.content, dict)
    assert len(referenced_value.content) == 2
    items = list(referenced_value.content.items())
    assert items[0][0].abstract_type == AbstractType.REF
    assert items[0][1].content is referenced_value  # cycle to dict value
    assert items[1][0].abstract_type == AbstractType.REF
    assert items[1][0].content.abstract_type == AbstractType.PRIMITIVE
    assert items[1][0].content.content == 1
    assert items[1][1].abstract_type == AbstractType.REF
    assert items[1][1].content.abstract_type == AbstractType.PRIMITIVE
    assert items[1][1].content.content == "un"


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [17])
def test_recdict(python_tracker):
    """
    Test DICT abstract type for a recursive dict.
    The recursive reference points to the same Value object
    """
    recdict_var = python_tracker.get_current_frame(
        as_raw_python_objects=False
    ).variables["recdict_var"]
    check_recdict(recdict_var)

    assert recdict_var.value.to_human_str().startswith("ref to 0x")
    assert (
        recdict_var.value.to_human_str(inline_ref=True) == '{"myself": {...}, 1: "un"}'
    )


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [17])
def test_recdict_raw(python_tracker):
    """Test DICT abstract type in the raw model: {"myself":{...}, 1:"un"}"""
    rect_var = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "recdict_var"
    ]
    assert rect_var is not None
    value = rect_var.value
    assert value["myself"] is value
    assert value[1] == "un"


if __name__ == "__main__":
    main()
