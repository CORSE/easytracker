# pylint: disable=redefined-outer-name

"""Test unknown types for Python"""

import pathlib
from dataclasses import dataclass
from typing import Optional

from pytest import mark
from easytracker import AbstractType


@dataclass
class SimpleStruct:
    """A STRUCT type for the abstract model"""

    field_one: int
    field_two: str
    field_three: float


@dataclass
class StructInStruct:
    """A STRUCT type for the abstract model"""

    field_one: int
    field_two: str
    field_three: SimpleStruct


@dataclass
class RecStruct:
    """A STRUCT type for the abstract model"""

    field_one: int
    field_two: str
    field_three: Optional["RecStruct"]


def main():
    """Program's entry point"""

    simple_var = SimpleStruct(1, "two", 3.0)

    struct_in_struct_var = StructInStruct(4, "five", SimpleStruct(6, "seven", 8.0))

    rec_var1 = RecStruct(9, "ten", None)
    rec_var1.field_three = rec_var1

    rec_var2 = RecStruct(11, "twelve", None)
    rec_var2_child = RecStruct(13, "fourteen", rec_var2)
    rec_var2.field_three = rec_var2_child

    print(f"{simple_var = }")
    print(f"{struct_in_struct_var}")
    print(f"{rec_var1 = }")
    print(f"{rec_var2 = }")


def check_3_fields(struct_value, langage_type):
    """
    Check that the given value is a STRUCT with 3 specifics fields.
    Also check that the third field has the given language type.
    """
    assert struct_value.abstract_type == AbstractType.REF
    referenced_value = struct_value.content
    assert referenced_value is not None
    assert referenced_value.abstract_type == AbstractType.STRUCT
    assert isinstance(referenced_value.content, dict)
    assert len(referenced_value.content) == 3
    items = list(referenced_value.content.items())
    assert items[0][0] == "field_one"
    assert items[0][1].content.abstract_type == AbstractType.PRIMITIVE
    assert items[0][1].content.language_type == "<class 'int'>"
    assert items[1][0] == "field_two"
    assert items[1][1].content.abstract_type == AbstractType.PRIMITIVE
    assert items[1][1].content.language_type == "<class 'str'>"
    assert items[2][0] == "field_three"
    assert items[2][1].content.language_type == langage_type

    return items


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [54])
def test_simple_struct(python_tracker):
    """Test STRUCT type for the instance SimpleStruct(1, "two", 3.0)"""
    simple_var = python_tracker.get_current_frame(
        as_raw_python_objects=False
    ).variables["simple_var"]
    assert simple_var is not None

    items = check_3_fields(simple_var.value, "<class 'float'>")
    assert items[0][1].content.content == 1
    assert items[1][1].content.content == "two"
    assert items[2][1].content.content == 3.0

    assert (
        simple_var.value.to_human_str(inline_ref=True)
        == '(field_one=1, field_two="two", field_three=3.0)'
    )


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [54])
def test_simple_struct_raw(python_tracker):
    """Test STRUCT type in the raw model for the instance SimpleStruct(1, "two", 3.0)"""
    simple_var = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "simple_var"
    ]
    assert simple_var is not None
    value = simple_var.value
    assert isinstance(value, dict)
    expected = (("field_one", 1), ("field_two", "two"), ("field_three", 3.0))
    for key, val in expected:
        assert value[key] == val


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [54])
def test_struct_in_struct(python_tracker):
    """
    Test STRUCT type for the instance
      StructInStruct(4, "five", SimpleStruct(6, "seven", 8.0))
    """
    struct_in_struct_var = python_tracker.get_current_frame(
        as_raw_python_objects=False
    ).variables["struct_in_struct_var"]
    assert struct_in_struct_var is not None

    items = check_3_fields(
        struct_in_struct_var.value, "<class 'test_struct.SimpleStruct'>"
    )
    assert items[0][1].content.content == 4
    assert items[1][1].content.content == "five"
    third_field_value = items[2][1]
    third_field_items = check_3_fields(third_field_value, "<class 'float'>")
    assert third_field_items[0][1].content.content == 6
    assert third_field_items[1][1].content.content == "seven"
    assert third_field_items[2][1].content.content == 8.0

    assert (
        struct_in_struct_var.value.to_human_str(inline_ref=True)
        == '(field_one=4, field_two="five", field_three=(field_one=6, field_two="seven", field_three=8.0))'
    )


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [54])
def test_struct_struct_raw(python_tracker):
    """
    Test STRUCT type in the raw model for the instance
       StructInStruct(4, "five", SimpleStruct(6, "seven", 8.0))
    """
    struct_in_struct_var = python_tracker.get_current_frame(
        as_raw_python_objects=True
    ).variables["struct_in_struct_var"]
    assert struct_in_struct_var is not None
    value = struct_in_struct_var.value
    assert isinstance(value, dict)
    expected = (
        ("field_one", 4),
        ("field_two", "five"),
        ("field_three", {"field_one": 6, "field_two": "seven", "field_three": 8.0}),
    )
    for key, val in expected:
        assert value[key] == val


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [54])
def test_rec_struct(python_tracker):
    """Test STRUCT type for the instance RecStruct(9, "ten", ...) where ... is recursive reference"""
    rec_var1 = python_tracker.get_current_frame(as_raw_python_objects=False).variables[
        "rec_var1"
    ]
    assert rec_var1 is not None

    items = check_3_fields(rec_var1.value, "<class 'test_struct.RecStruct'>")
    assert items[0][1].content.content == 9
    assert items[1][1].content.content == "ten"
    assert items[2][1].content is rec_var1.value.content

    assert (
        rec_var1.value.to_human_str(inline_ref=True)
        == '(field_one=9, field_two="ten", field_three=...)'
    )


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [54])
def test_rec_struct_raw(python_tracker):
    """
    Test STRUCT type in the raw model for the instance
    RecStruct(9, "ten", ...) where ... is recursive reference
    """
    rec_var1 = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "rec_var1"
    ]
    assert rec_var1 is not None
    value = rec_var1.value
    assert isinstance(value, dict)
    expected = (("field_one", 9), ("field_two", "ten"), ("field_three", value))
    for key, val in expected:
        if isinstance(val, dict):
            assert value[key] is val
        else:
            assert value[key] == val


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [54])
def test_indirect_rec_struct(python_tracker):
    """
    Test STRUCT type for the instance :
      RecStruct(11, "twelve", RecStruct(13, "fourteen", ...))
    where ... is a recursive reference to RecStruct(11, "twelve", ...
    """
    rec_var2 = python_tracker.get_current_frame(as_raw_python_objects=False).variables[
        "rec_var2"
    ]
    assert rec_var2 is not None

    items = check_3_fields(rec_var2.value, "<class 'test_struct.RecStruct'>")
    assert items[0][1].content.content == 11
    assert items[1][1].content.content == "twelve"
    third_field_value = items[2][1]
    third_field_items = check_3_fields(
        third_field_value, "<class 'test_struct.RecStruct'>"
    )
    assert third_field_items[0][1].content.content == 13
    assert third_field_items[1][1].content.content == "fourteen"
    assert third_field_items[2][1].content is rec_var2.value.content

    assert (
        rec_var2.value.to_human_str(inline_ref=True)
        == '(field_one=11, field_two="twelve", field_three=(field_one=13, field_two="fourteen", field_three=...))'
    )


@mark.parametrize("file", [pathlib.Path(__file__).resolve()])
@mark.parametrize("lineno", [54])
def test_indirect_rec_struct_raw(python_tracker):
    """
    Test STRUCT type for the instance :
      RecStruct(11, "twelve", RecStruct(13, "fourteen", ...))
    where ... is a recursive reference to RecStruct(11, "twelve", ...
    """
    rec_var2 = python_tracker.get_current_frame(as_raw_python_objects=True).variables[
        "rec_var2"
    ]
    assert rec_var2 is not None
    value = rec_var2.value
    assert isinstance(value, dict)
    expected = (("field_one", 11), ("field_two", "twelve"))
    for key, val in expected:
        assert value[key] == val
    in_value = value["field_three"]
    assert isinstance(in_value, dict)
    expected = (("field_one", 13), ("field_two", "fourteen"))
    for key, val in expected:
        assert in_value[key] == val
    assert in_value["field_three"] is value


if __name__ == "__main__":
    main()
