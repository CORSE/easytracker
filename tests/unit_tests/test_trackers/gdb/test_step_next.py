# pylint: disable=redefined-outer-name,unused-import

"""Simple tests for next and steps and line number checks"""

from pytest import mark

INFERIOR = [
    """int b;

int add(int a, int b) {
    return a + b;
}

void padd(int* res, int a, int b) {
    *res = a + b;
}

int main() {
    int a, c;
    a = 1;
    b = 2;
    padd(&c, a, b); // c = 3

    padd(&a, c, b); // a = 5

    b = add(a, c); // b = 8

    return 8;
}
"""
]


@mark.parametrize("c_sources", [INFERIOR])
def test_step_next(gdb_tracker):
    """performs some step and next and checks line number"""
    assert gdb_tracker.next_lineno is None
    assert gdb_tracker.last_lineno is None

    gdb_tracker.start()
    assert gdb_tracker.next_lineno == 11

    gdb_tracker.next()
    # declarations are skipped
    assert gdb_tracker.next_lineno == 13

    gdb_tracker.next()
    gdb_tracker.next()
    assert gdb_tracker.next_lineno == 15

    gdb_tracker.step()
    assert gdb_tracker.next_lineno == 8
    gdb_tracker.next()
    assert gdb_tracker.next_lineno == 9
    gdb_tracker.next()
    # an empty line is skipped
    assert gdb_tracker.next_lineno == 17
