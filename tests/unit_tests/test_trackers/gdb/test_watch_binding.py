# pylint: disable=redefined-outer-name,unused-import,unused-variable,too-many-locals

"""Simple tests for watchpoints with bindings"""

from pytest import mark
from easytracker import PauseReasonType, NameBinding

INFERIOR = [
    """/* C source test */
int pow2(int n) {
    /* Computes power of two. */
    int res = 1;
    for (int i = 0; i < n; i++)
        res = res * 2;
    return res;
}

int main() {
    /* Entry point of the program. Return on the same to test watch on last statement */
    int res = pow2(3); return 0;
}
"""
]


# Note that for the gdb tracker it is not possible currently to watch a
# local variable without a function binding is not already in the function
# Hence we do not test with_bindings to False as in the python tracker test
@mark.parametrize("c_sources", [INFERIOR])
@mark.parametrize("with_bindings,with_depth", [[True, False], [True, False]])
def test_watch_binding(gdb_tracker, with_bindings, with_depth):
    """Cheks the pause_reason"""

    pow2_line = 2
    main_line = 10

    # Watchpoints report line number after the change
    # In the case of last statement in loop, it is
    # the loop start
    # In the case of last statement in the function
    # it is the actual statement line
    # Otherwise, it is the next line to execute
    # exected contains tuples: (value, line, depth)
    expected = [
        (1, pow2_line + 3, 1),
        (2, pow2_line + 3, 1),
        (4, pow2_line + 3, 1),
        (8, pow2_line + 3, 1),
        (8, main_line + 2, 0),
    ]
    count = 0

    # Set function tracking and restart
    wids = []
    if with_bindings:
        if with_depth:
            wids.append(gdb_tracker.watch(NameBinding("res", "pow2", depth=1)))
            wids.append(gdb_tracker.watch(NameBinding("res", "main", depth=0)))
        else:
            wids.append(gdb_tracker.watch(NameBinding("res", "pow2")))
            wids.append(gdb_tracker.watch(NameBinding("res", "main")))
    else:
        wids.append(gdb_tracker.watch("res"))

    gdb_tracker.start()
    stop_reason = gdb_tracker.resume()
    new_val = None
    while stop_reason.type != PauseReasonType.EXITED:
        print(f"{stop_reason = }")
        assert stop_reason.type == PauseReasonType.WATCHPOINT
        # For the gdb tracker the pause reason tuple in watchpoints is
        # wpnum, watch expression, str old val, str new val
        # Note tha args are different in the python tracker
        # Use get_variable_value() to have a compatible interface.
        wpnum, var, old_str, new_str = stop_reason.args
        assert wpnum in wids
        assert var == "res"
        new_val = int(new_str)
        assert new_val == expected[count][0]
        line = stop_reason.line
        assert isinstance(line, int)
        assert line == expected[count][1]
        print(f"{new_val = }")
        frame = gdb_tracker.get_current_frame(as_raw_python_objects=False)
        res_value = frame.variables["res"].value.content
        var_value = gdb_tracker.get_variable_value(
            "res", as_raw_python_objects=False
        ).value.content
        assert res_value == new_val
        assert var_value == new_val
        stop_reason = gdb_tracker.resume()
        count += 1

    assert count == len(expected)
