# pylint: disable=redefined-outer-name,unused-import

"""Simple tests for line and function breakpoints"""

from pytest import mark
from easytracker import PauseReasonType

INFERIOR = [
    """int b;

int add(int a, int b) {
    int res = a + b;
    return res;
}

int main() {
    int a, c;
    a = 1;
    b = 2;
    c = add(a, b); // c = 3
    c++;
    return c;
}
"""
]


@mark.parametrize("c_sources", [INFERIOR])
def test_break_before_line_and_function(gdb_tracker):
    """Add before breakpoints and resume to check they are reached properly"""
    assert gdb_tracker.next_lineno is None
    assert gdb_tracker.last_lineno is None

    pause_reason = gdb_tracker.start()
    assert gdb_tracker.next_lineno == 10
    assert pause_reason.type == PauseReasonType.START

    gdb_tracker.break_before_line(lineno=12, maxdepth=5)
    gdb_tracker.break_before_func(funcname="add", maxdepth=5)

    pause_reason = gdb_tracker.resume()
    assert gdb_tracker.next_lineno == 12
    assert pause_reason.type == PauseReasonType.BREAKPOINT

    pause_reason = gdb_tracker.resume()
    assert gdb_tracker.next_lineno == 4
    assert pause_reason.type == PauseReasonType.BREAKPOINT

    pause_reason = gdb_tracker.resume()
    assert gdb_tracker.next_lineno is None
    assert pause_reason.type == PauseReasonType.EXITED
    assert gdb_tracker.exit_code == 4


@mark.parametrize("c_sources", [INFERIOR])
def test_break_before_end_of_function(gdb_tracker):
    """Add before end of function breakpoint and resume to check it is reached properly"""
    assert gdb_tracker.next_lineno is None
    assert gdb_tracker.last_lineno is None

    pause_reason = gdb_tracker.start()
    assert gdb_tracker.next_lineno == 10
    assert pause_reason.type == PauseReasonType.START

    gdb_tracker.break_end_of_func(funcname="add", maxdepth=5)

    pause_reason = gdb_tracker.resume()
    assert pause_reason.type == PauseReasonType.RETURN
    assert gdb_tracker.last_lineno == 10
    assert gdb_tracker.next_lineno == 12

    pause_reason = gdb_tracker.resume()
    assert pause_reason.type == PauseReasonType.EXITED
    assert gdb_tracker.next_lineno is None
    assert gdb_tracker.exit_code == 4
