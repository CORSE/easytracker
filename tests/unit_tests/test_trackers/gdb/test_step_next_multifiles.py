# pylint: disable=redefined-outer-name,unused-import

"""Simple tests for next and steps and line number checks"""
import os.path

from pytest import mark
from easytracker import Frame


INFERIOR = [
    """int add(int a, int b) {
    return a + b;
}""",
    """int main() {
    int a, b, c;
    a = 1;
    b = 2;
    c = add(a, b); // c = 3
    return 8;
}
""",
]


@mark.parametrize("c_sources", [INFERIOR])
def test_step_next_multifiles(gdb_tracker):
    """performs some step and next and checks line number"""
    assert gdb_tracker.next_lineno is None
    assert gdb_tracker.last_lineno is None
    assert gdb_tracker.last_source_file is None
    assert gdb_tracker.next_source_file is None

    gdb_tracker.start()
    frame: Frame = gdb_tracker.get_current_frame()
    main_file = frame.next_source_file
    assert os.path.isabs(main_file)
    assert main_file.startswith("/tmp/tmp")
    assert main_file.endswith(".c")
    assert frame.next_lineno == gdb_tracker.next_lineno == 3  # declarations are skipped
    assert gdb_tracker.last_source_file is None
    assert gdb_tracker.next_source_file == frame.next_source_file

    gdb_tracker.next()
    frame = gdb_tracker.get_current_frame()
    assert frame.next_lineno == gdb_tracker.next_lineno == 4
    assert gdb_tracker.last_source_file == main_file
    assert gdb_tracker.next_source_file == frame.next_source_file == main_file

    gdb_tracker.next()
    frame = gdb_tracker.get_current_frame()
    assert frame.next_lineno == gdb_tracker.next_lineno == 5
    assert gdb_tracker.last_source_file == main_file
    assert gdb_tracker.next_source_file == frame.next_source_file == main_file

    gdb_tracker.step()
    frame = gdb_tracker.get_current_frame()
    aux_file = frame.next_source_file
    assert aux_file.startswith("/tmp/tmp")
    assert aux_file.endswith(".c")
    assert aux_file != main_file
    assert frame.next_lineno == gdb_tracker.next_lineno == 2  # declarations are skipped
    assert gdb_tracker.last_source_file == main_file
    assert gdb_tracker.next_source_file == frame.next_source_file

    gdb_tracker.next()
    gdb_tracker.next()
    frame = gdb_tracker.get_current_frame()
    assert frame.next_lineno == gdb_tracker.next_lineno == 6  # declarations are skipped
    assert gdb_tracker.last_source_file == aux_file
    assert gdb_tracker.next_source_file == frame.next_source_file == main_file
