# pylint: disable=redefined-outer-name,unused-import

"""Simple tests for next and steps and line number checks"""

from pytest import mark
from easytracker import PauseReasonType, AbstractType

INFERIOR = [
    """#include <stdio.h>

int sum_of_first_ints(int i) {
    printf("i = %d\\n", i);
    if (i == 1) {
        return 1;
    }
    return i + sum_of_first_ints(i - 1);
}

int main() {
    printf("dummy line\\n");
    int res = sum_of_first_ints(10);
    printf("res = %d\\n", res);
    return res;
}
"""
]


@mark.parametrize("c_sources", [INFERIOR])
def test_track_function(gdb_tracker):
    """Checks several things related to tracking functions"""

    # Set function tracking
    gdb_tracker.track_function("sum_of_first_ints", maxdepth=1)

    # Continue until the function is called and check reason
    gdb_tracker.start()
    gdb_tracker.resume()
    stop_reason = gdb_tracker.pause_reason
    assert stop_reason.type == PauseReasonType.CALL
    assert isinstance(stop_reason.line, int)

    # Do a next and check reason
    gdb_tracker.next()
    stop_reason = gdb_tracker.pause_reason
    assert stop_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    assert isinstance(stop_reason.line, int)

    # Do a resume and check reason
    gdb_tracker.resume()
    stop_reason = gdb_tracker.pause_reason
    assert stop_reason.type == PauseReasonType.RETURN
    assert stop_reason.args[2] == 55
    assert isinstance(stop_reason.line, int)

    # Compared to python tracker, we don't have access to the frame
    # anymore here in the GDB tracker, hence we can't check local
    # variables

    # Check that resumes go to program's completion
    pause_reason = gdb_tracker.resume()
    assert pause_reason.type == PauseReasonType.EXITED
    assert pause_reason.args[0] == 55
    assert pause_reason.line is None
    assert gdb_tracker.exit_code == 55
