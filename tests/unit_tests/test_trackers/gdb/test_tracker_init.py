"""Simple tests for tracker initialisations"""

# pylint: disable=redefined-outer-name
# pylint: disable=import-outside-toplevel

import tempfile
import subprocess
from pathlib import Path
from pytest import fixture

INFERIOR_CODE = """int main() { return 0; }"""


@fixture
def c_inferior():
    """Creates and compiles the inferior"""
    with tempfile.TemporaryDirectory() as tmpdir:
        c_fname = Path(tmpdir) / "inferior.c"
        out_fname = Path(tmpdir) / "inferior.out"
        with open(c_fname, "w", encoding="utf8") as c_file:
            c_file.write(INFERIOR_CODE)
        subprocess.run(
            f"gcc {c_fname} -g -O0 -o {out_fname}", check=True, text=True, shell=True
        )
        yield out_fname


def test_gdb_tracker(c_inferior):
    """Test standard Tracker() interface"""
    from easytracker_gdb import Tracker

    tracker = Tracker()
    tracker.load_program(c_inferior)
    tracker.terminate()


def test_gdb_gdb_tracker(c_inferior):
    """Test legacy GdbTracker() interface"""
    from easytracker_gdb.gdb_tracker import GdbTracker

    tracker = GdbTracker()
    tracker.load_program(c_inferior)
    tracker.terminate()


def test_init_tracker_gdb_legacy(c_inferior):
    """Test legacy init_tracker name GDB"""
    from easytracker import init_tracker

    tracker = init_tracker("GDB")
    tracker.load_program(c_inferior)
    tracker.terminate()


def test_init_tracker_gdb(c_inferior):
    """Test standard init_tracker name gdb resolving to easytracker_gdb"""
    from easytracker import init_tracker

    tracker = init_tracker("gdb")
    tracker.load_program(c_inferior)
    tracker.terminate()
