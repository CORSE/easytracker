"""Test version information"""
import easytracker_gdb


def test_version():
    """Simple version tuple test"""
    assert hasattr(easytracker_gdb, "__version__")
    tuples = easytracker_gdb.__version__.split(".")
    major, minor, patch = [int(t) for t in tuples[:3]]
    assert major >= 1
    assert minor >= 0
    assert patch >= 0
