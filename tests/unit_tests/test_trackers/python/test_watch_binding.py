# pylint: disable=redefined-outer-name,unused-import,unused-variable,too-many-locals

"""Simple tests for watchpoints with bindings"""

import pytest
from fixtures import tracker, parametrize
from easytracker import PauseReasonType, NameBinding, RawVariable


def pow2(i: int):
    "Computes power of two."
    res = 1
    for _ in range(i):
        res = res * 2
    return res


def main():
    "Entry point of the program."
    res = pow2(3)


def _set_watchpoints(tracker, with_bindings, with_depth) -> list[int]:
    """Set watchpoints according to the parameters"""
    watchpoint_ids = []
    if with_bindings:
        if with_depth:
            watchpoint_ids.append(tracker.watch(NameBinding("res", "pow2", depth=1)))
            watchpoint_ids.append(tracker.watch(NameBinding("res", "main", depth=0)))
        else:
            watchpoint_ids.append(tracker.watch(NameBinding("res", "pow2")))
            watchpoint_ids.append(tracker.watch(NameBinding("res", "main")))
    else:
        watchpoint_ids.append(tracker.watch("res"))
    return watchpoint_ids


@parametrize("file", [__file__])
@parametrize("with_bindings, with_depth", [[False, False], [True, False], [True, True]])
def test_watch_binding(tracker, with_bindings, with_depth):
    """Checks the pause_reason"""

    pow2_line = pow2.__code__.co_firstlineno
    main_line = main.__code__.co_firstlineno

    # Watchpoints report line number after the change
    # In the case of last statement in loop, it is
    # the loop start
    # In the case of last statement in the function
    # it is the actual statement line
    # Otherwise, it is the next line to execute
    # expected contains tuples: (value, line, depth)
    expected = [
        (1, pow2_line + 3, 1),
        (2, pow2_line + 3, 1),
        (4, pow2_line + 3, 1),
        (8, pow2_line + 3, 1),
        (8, main_line + 2, 0),
    ]
    count = 0

    watchpoint_ids = _set_watchpoints(tracker, with_bindings, with_depth)
    stop_reason = tracker.resume()
    res_val = None
    while stop_reason.type != PauseReasonType.EXITED:
        print(f"{stop_reason = }")
        assert stop_reason.type == PauseReasonType.WATCHPOINT
        # For the python tracker the pause reason args in watchpoints is
        # raw variable, wpnum
        # Note that args are different in the gdb tracker
        # Use get_variable_value() to have a compatible interface.
        var, wpnum = stop_reason.args
        assert wpnum in watchpoint_ids
        assert isinstance(var, RawVariable)
        res_val = var.value
        assert res_val == expected[count][0]
        line = stop_reason.line
        assert isinstance(line, int)
        assert line == expected[count][1]
        print(f"{res_val = }")
        frame = tracker.get_current_frame(as_raw_python_objects=True)
        var_frame = frame.variables["res"].value
        var_value = tracker.get_variable_value("res", as_raw_python_objects=True).value
        assert var_frame == res_val
        assert var_value == res_val
        stop_reason = tracker.resume()
        count += 1

    assert count == len(expected)


@parametrize("file", [__file__])
@parametrize("with_bindings, with_depth", [[False, False], [True, False], [True, True]])
def test_delete_watch_binding(tracker, with_bindings, with_depth):
    """Checks that watchpoints can be deleted properly"""
    watchpoint_ids = _set_watchpoints(tracker, with_bindings, with_depth)
    for watchpoint_id in watchpoint_ids:
        tracker.delete_breakpoint(watchpoint_id)
    pause_reason = tracker.resume()
    assert pause_reason.type == PauseReasonType.EXITED
