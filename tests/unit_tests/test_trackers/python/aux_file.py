"""A very simple auxiliary file to test applications with multiple files"""


def add(a_param: int, b_param: int) -> int:
    """Returns the sum of two integers."""
    return a_param + b_param
