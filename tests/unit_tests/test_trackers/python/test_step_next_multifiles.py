# pylint: disable=redefined-outer-name,unused-import

"""Test step and next in an application made of two files"""

import os
from fixtures import tracker, parametrize
from easytracker.types.frame import RawFrame

import aux_file


def main():
    """Entry point of the program."""
    print("dummy line")
    res = aux_file.add(3, 4)
    print(res)


@parametrize("file", [__file__])
def test_step_next_multifiles(tracker):
    """Checks we correctly move between files."""

    frame: RawFrame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file is None
    assert tracker.next_source_file == frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == 12

    tracker.next()
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == __file__
    assert tracker.next_source_file == frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == 14

    tracker.next()
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == __file__
    assert tracker.next_source_file == frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == 15

    tracker.step()
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    aux_file_path = os.path.abspath(aux_file.__file__)
    assert tracker.last_source_file == __file__
    assert tracker.next_source_file == frame.next_source_file == aux_file_path
    assert tracker.next_lineno == frame.next_lineno == 4

    tracker.next()
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == aux_file_path
    assert tracker.next_source_file == frame.next_source_file == aux_file_path
    assert tracker.next_lineno == frame.next_lineno == 6

    tracker.next()
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == aux_file_path
    assert tracker.next_source_file == frame.next_source_file == aux_file_path
    assert tracker.next_lineno == frame.next_lineno == 6

    tracker.step()  # TODO : DIRTY hack because our python tracker next implementation is BUGGY
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == aux_file_path
    assert tracker.next_source_file == frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == 16


if __name__ == "__main__":
    main()
