"""
Test on a high number of runs and with high frequency thread switch
that a simple python tracker does not dead lock.
This issue was reported at https://gitlab.inria.fr/CORSE/easytracker/-/issues/22
"""

# pylint: disable=import-outside-toplevel

import logging

# Number of iterations, >= 1000 at least for producing the bug
ITERATIONS = 10000
# Thread switch interval, set to very low value for producing the bug
SWICTH_INTERVAL_MS = 0.001
# Seconds before considering a dead-lock, >= 1 should be sufficient
TIMEOUT_SECS = 1
# DEBUG
DEBUG = False

logger = logging.getLogger(__name__)


def main():
    """Inferior program, just iterating"""
    for _i in range(ITERATIONS):
        pass


def controller(queue):
    """Controller, just stepping and pushing values of inferior's i to queue"""
    import sys
    from easytracker import init_tracker

    if DEBUG:
        logging.getLogger("easytracker_python.utils.cothread").setLevel(logging.DEBUG)
    sys.setswitchinterval(SWICTH_INTERVAL_MS / 1000)
    tracker = init_tracker("python")
    tracker.load_program(__file__)
    tracker.start()
    last_i = None
    while tracker.exit_code is None:
        var = tracker.get_variable_value("_i", as_raw_python_objects=True)
        if var is not None and var.value != last_i:
            queue.put(var.value)
            last_i = var.value
        tracker.step()
    logger.debug("terminating tracker")
    tracker.terminate()
    queue.put(tracker.exit_code)
    logger.debug("exiting process")


def test_tracker_race():
    """Test that no dead lock occurs between the inferior and controller"""
    import multiprocessing as mp
    from queue import Empty

    if DEBUG:
        logger.setLevel(logging.DEBUG)
    queue = mp.Queue()
    proc = mp.Process(target=controller, args=(queue,), daemon=True)
    logger.debug("starting process")
    proc.start()
    for i in range(ITERATIONS):
        try:
            main_i = queue.get(timeout=TIMEOUT_SECS)
            assert main_i == i, f"Invalid value of i: {main_i}, expected {i}"
        except Empty:
            break
    if ITERATIONS > 0:
        if i != ITERATIONS - 1:
            proc.terminate()
        assert i == ITERATIONS - 1, f"DEAD LOCK at i = {i} / {ITERATIONS - 1}..."
    logger.debug("joining process")
    proc.join()
    logger.debug("joined process")
    exit_code = queue.get(timeout=TIMEOUT_SECS)
    assert exit_code == 0
