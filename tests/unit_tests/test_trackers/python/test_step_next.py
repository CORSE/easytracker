# pylint: disable=redefined-outer-name,unused-import

"""Test step and next"""


from fixtures import tracker, parametrize
from easytracker.types.frame import RawFrame
from easytracker import PauseReasonType


def add(a_param: int, b_param: int) -> int:
    """Returns the sum of two integers."""
    return a_param + b_param


def main():
    """Entry point of the program."""
    print("dummy line")
    res = add(3, 4)
    print(res)


@parametrize("file", [__file__])
def test_step_next_multifiles(tracker):
    """Checks we correctly move between files."""

    frame: RawFrame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file is None
    assert tracker.next_source_file == frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == 16

    pause_reason = tracker.next()
    assert pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == __file__
    assert tracker.next_source_file == frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == pause_reason.line == 18

    pause_reason = tracker.next()
    assert pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == __file__
    assert tracker.next_source_file == frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == pause_reason.line == 19

    pause_reason = tracker.step()
    assert pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == __file__
    assert tracker.next_source_file == frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == pause_reason.line == 11

    pause_reason = tracker.next()
    assert pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == __file__
    assert tracker.next_source_file == frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == pause_reason.line == 13

    pause_reason = tracker.next()
    assert pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == __file__
    assert tracker.next_source_file == frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == pause_reason.line == 13

    pause_reason = tracker.next()
    assert pause_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    frame = tracker.get_current_frame(as_raw_python_objects=True)
    assert tracker.last_source_file == __file__
    assert tracker.next_source_file == __file__
    assert frame.next_source_file == __file__
    assert tracker.next_lineno == frame.next_lineno == pause_reason.line == 20


if __name__ == "__main__":
    main()
