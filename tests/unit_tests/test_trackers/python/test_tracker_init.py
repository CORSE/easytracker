"""Simple tests for tracker initialisations"""

# pylint: disable=import-outside-toplevel


def main():
    """Tracked entry point"""
    return 0


def test_python_tracker():
    """Test standard Tracker() interface"""
    from easytracker_python import Tracker

    tracker = Tracker()
    tracker.load_program(__file__)
    tracker.terminate()


def test_python_python_tracker():
    """Test legacy PythonTracker() interface"""
    from easytracker_python.python_tracker import PythonTracker

    tracker = PythonTracker()
    tracker.load_program(__file__)
    tracker.terminate()


def test_init_tracker_python():
    """Test init_tracker interface for python"""
    from easytracker import init_tracker

    tracker = init_tracker("python")
    tracker.load_program(__file__)
    tracker.terminate()
