"""Simple tests for tracker start method"""

from easytracker import init_tracker, PauseReasonType


def main():
    """Tracked entry point"""
    return 0


def test_start():
    """Test standard Tracker() interface"""

    tracker = init_tracker("python")
    tracker.load_program(__file__)
    pause_reason = tracker.start()
    assert pause_reason.type == PauseReasonType.START
    assert pause_reason.line is None
    tracker.terminate()
