"""
Test the CoThread class
"""

import sys
import logging
from easytracker_python.utils import CoThread

# Thread switch interval, set to very low value
SWICTH_INTERVAL_MS = 0.001
# Set to True for debug output
DEBUG = False

logger = logging.getLogger(__name__)
if DEBUG:
    logger.setLevel(logging.DEBUG)
    logging.getLogger("easytracker_python.utils.cothread").setLevel(logging.DEBUG)

sys.setswitchinterval(SWICTH_INTERVAL_MS / 1000)


class EarlyException(Exception):
    """Exception used for tests"""


def test_empty_start_join():
    """Simulate with a null coroutine"""
    cor = CoThread(target=None, daemon=False)
    cor.start()
    cor.join(timeout=1)
    assert not cor.is_alive()
    assert cor.co_exited
    assert cor.co_exception is None


def test_start_yield_join():
    """Simulate 1 yield"""

    def target():
        res = cor.yield_to_main("co-0")
        assert res is None

    cor = CoThread(target=target)
    cor.start()
    res = cor.yield_to_co()
    assert res == "co-0"
    cor.join(timeout=1)
    assert not cor.is_alive()
    assert cor.co_exited
    assert cor.co_exception is None


def test_start_yield2_join():
    """Simulate 2 yields"""

    def target():
        res = cor.yield_to_main("co-0")
        assert res == "main-1"
        res = cor.yield_to_main("co-1")
        assert res is None

    cor = CoThread(target=target)
    cor.start()
    res = cor.yield_to_co()
    assert res == "co-0"
    res = cor.yield_to_co("main-1")
    assert res == "co-1"
    cor.join(timeout=1)
    assert not cor.is_alive()
    assert cor.co_exited
    assert cor.co_exception is None


def test_start_except_join():
    """Force exception in coroutine"""

    def target():
        res = cor.yield_to_main("co-0")
        assert res == "main-1"
        raise EarlyException("coroutine exception")

    cor = CoThread(target=target)
    cor.start()
    res = cor.yield_to_co()
    assert res == "co-0"
    res = cor.yield_to_co("main-1")
    assert res is None  # due to exception
    assert cor.co_exited
    assert cor.co_exception is not None
    assert str(cor.co_exception[1]) == "coroutine exception"
    cor.join(timeout=1)
    assert not cor.is_alive()
    assert cor.co_exited
    assert cor.co_exception is not None


class MainCO:
    """Main test class, creates a cothread and run a loop"""

    def __init__(self, iterations=10000, exception=None):
        self.iterations = iterations
        self.exception = exception
        self.exit_code = None
        self.cor = None

    def _on_exit(self):
        if self.exit_code is None:
            self.exit_code = -1

    def target(self):
        """Co thread"""
        for i in range(self.iterations - 1):
            resp = self.cor.yield_to_main(i)
            logger.debug("CO RESP MAIN %s, i %s", resp, i)
            assert resp == i + 1
            if self.exception and self.exception == i:
                raise EarlyException(f"CO Exception at {i}")
        self.exit_code = 0

    def main(self):
        """Main thread"""
        self.cor = CoThread(on_exit=self._on_exit, target=self.target, daemon=False)
        self.cor.start()

        for i in range(self.iterations):
            resp = self.cor.yield_to_co(i)
            if self.cor.co_exited:
                logger.debug("EXITED early")
                break
            logger.debug("MAIN RESP CO %s, i %s", resp, i)
            assert i == resp
        logger.debug("CO joining")
        self.cor.join()
        return self.exit_code


def test_co_iterations():
    """Test high number of iterations to detect dead locks"""
    iterations = 10000
    if DEBUG:
        iterations = 100
    test = MainCO(iterations=iterations)
    ret = test.main()
    assert ret == 0


def test_co_iterations_exception():
    """Test raise of an exception from the cothread"""
    exception = 3
    test = MainCO(iterations=exception * 2, exception=exception)
    ret = test.main()
    assert test.cor.co_exception is not None
    assert str(test.cor.co_exception[1]) == f"CO Exception at {exception}"
    assert ret == -1
