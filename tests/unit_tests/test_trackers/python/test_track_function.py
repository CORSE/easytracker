# pylint: disable=redefined-outer-name,unused-import

"""Simple tests for track_function"""

from fixtures import tracker, parametrize
from easytracker import PauseReasonType


def sum_of_first_ints(i: int):
    """Computes sum of first i integers."""
    print(f"{i = }")
    if i == 1:
        return 1
    return i + sum_of_first_ints(i - 1)


def main():
    """Entry point of the program."""
    print("dummy line")
    res = sum_of_first_ints(10)
    print(res)


@parametrize("file", [__file__])
def test_track_function_python_pause_reason(tracker):
    """Checks the pause_reason"""

    # Set function tracking
    tracker.track_function("sum_of_first_ints", maxdepth=1)

    # Continue until the function is called and check reason
    tracker.resume()
    stop_reason = tracker.pause_reason
    assert stop_reason.type == PauseReasonType.CALL
    assert stop_reason.line == 9
    assert len(stop_reason.args) == 2
    assert stop_reason.args[0] == 1
    assert stop_reason.args[1] == "sum_of_first_ints"

    # Do a next and check reason
    # Two next are required, it's not a bug
    # it behaves like that in any python debugger
    tracker.next()
    tracker.next()
    stop_reason = tracker.pause_reason
    assert stop_reason.type == PauseReasonType.ENDSTEPPING_RANGE
    assert stop_reason.line == 11
    assert len(stop_reason.args) == 0

    # Do a resume and check reason
    tracker.resume()
    stop_reason = tracker.pause_reason
    assert stop_reason.type == PauseReasonType.RETURN
    assert stop_reason.args[2] == 55
    assert isinstance(stop_reason.line, int)

    # In the python tracker, we still have access to the frame
    # compared to the GDB tracker, hence we can check local
    # variables
    i_var = tracker.get_variable_value("i", as_raw_python_objects=True)
    assert i_var.value == 10

    # Check that resumes go to program's completion
    pause_reason = tracker.resume()
    assert pause_reason.type == PauseReasonType.EXITED
    assert pause_reason.args[0] == 0  # If main return None, it is zero by definition
    assert pause_reason.line is None
    assert tracker.exit_code == 0  # If main return None, it is zero by definition
