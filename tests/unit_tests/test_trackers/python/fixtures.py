"""Fixtures for tracker tests"""

from pytest import fixture
from pytest import mark
from easytracker import init_tracker


parametrize = mark.parametrize


@fixture
def tracker(file):
    """Fixture creating the tracker"""
    track = init_tracker("python")
    track.load_program(file)
    track.start()
    yield track
    track.resume()
    track.terminate()
