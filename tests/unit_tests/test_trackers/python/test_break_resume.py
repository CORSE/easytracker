# pylint: disable=redefined-outer-name,unused-import

"""Test break and resume"""

# pylint: disable=useless-return

from fixtures import tracker, parametrize
from easytracker import PauseReasonType


def add(a_param: int, b_param: int) -> int:
    """Returns the sum of two integers."""
    a_plus_b = a_param + b_param
    return a_plus_b


def main():
    """Entry point of the program."""
    print("dummy line")
    res = add(3, 4)
    print(res)
    return


@parametrize("file", [__file__])
def test_break_before_line_resume(tracker):
    """Checks we correctly break before line and resume."""

    bp1_id = tracker.break_before_line(13)
    assert bp1_id == 1
    bp2_id = tracker.break_before_line(21)
    assert bp2_id == 2

    pause_reason = tracker.resume()
    assert tracker.next_lineno == pause_reason.line == 13
    assert pause_reason.type == PauseReasonType.BREAKPOINT
    assert len(pause_reason.args) == 1
    assert pause_reason.args[0] == bp1_id

    pause_reason = tracker.resume()
    assert tracker.next_lineno == pause_reason.line == 21
    assert pause_reason.type == PauseReasonType.BREAKPOINT
    assert len(pause_reason.args) == 1
    assert pause_reason.args[0] == bp2_id

    pause_reason = tracker.resume()
    assert pause_reason.type == PauseReasonType.EXITED
    assert pause_reason.line is None
    assert len(pause_reason.args) == 1
    assert pause_reason.args[0] == tracker.exit_code
    assert tracker.exit_code == 0


@parametrize("file", [__file__])
def test_break_before_func_resume(tracker):
    """Checks we correctly break before function and resume."""

    bp1_id = tracker.break_before_func("add")
    assert bp1_id == 1

    pause_reason = tracker.resume()
    assert tracker.next_lineno == pause_reason.line == 11

    assert pause_reason.type == PauseReasonType.BREAKPOINT
    assert len(pause_reason.args) == 1
    assert pause_reason.args[0] == bp1_id

    pause_reason = tracker.resume()
    assert pause_reason.type == PauseReasonType.EXITED
    assert pause_reason.line is None
    assert len(pause_reason.args) == 1
    assert pause_reason.args[0] == tracker.exit_code
    assert tracker.exit_code == 0


@parametrize("file", [__file__])
def test_delete_breakpoint(tracker):
    """Checks we correctly delete breakpoint."""

    bp1_id = tracker.break_before_line(13)
    assert bp1_id == 1
    bp2_id = tracker.break_before_line(21)
    assert bp2_id == 2

    tracker.delete_breakpoint(bp1_id)
    pause_reason = tracker.resume()
    assert tracker.next_lineno == pause_reason.line == 21
    assert pause_reason.type == PauseReasonType.BREAKPOINT
    assert len(pause_reason.args) == 1
    assert pause_reason.args[0] == bp2_id


if __name__ == "__main__":
    main()
