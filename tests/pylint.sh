#!/usr/bin/env bash
#
# pylint.sh: simple wrapper for pylint, pass configuration and
# explicit list of folders to check
#
set -euo pipefail

dir="$(dirname "$0")"

include=(
    easytracker/
    easytracker-gdb/
    easytracker-python/
    extra-packages/visualprimitives/
    tools/
    tests/
)

exclude=(
    extra-packages/visualprimitives/pygame/
    easytracker-gdb/easytracker_gdb/pygdbmi/
)

if [ $# -gt 0 ]; then
    set -- "$@"
else
    grep_excl=cat
    excl_p="$(echo "${exclude[@]}"|sed 's/ / -e /g')"
    [ -z "$excl_p" ] || grep_excl="grep -v -e $excl_p"
    files=()
    for i in "${include[@]}"; do
        files=("${files[@]}" $(git ls-files "$dir/../$i"/'*.py' | $grep_excl || true))
    done
    set -- "${files[@]}"
fi

echo "running pylint version $(pylint --version)"
pylint --rcfile="$dir"/../pylintrc --disable=fixme "$@"
