#!/usr/bin/env bash
set -euo pipefail

image="easytracker"

dir="$(dirname "$0")"
cd "$dir"

echo "Building image: $image"
exec docker build -t "$image" .
