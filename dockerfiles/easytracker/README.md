Docker file for easytracker tests
=================================

Install easytracker dependencies.

Build with:

    ./build.sh

Publish to gitlab repository after upgrading the
`VERSION` file and run:

    ./publish.sh

Note that the first time one accesses to the registry,
a login prompt will appear. Login with the `gitlab.inria.fr`
account/password.

The image can be used for CI scripts or locally for instance
with:

    cd easytracker
    docker pull registry.gitlab.inria.fr/corse/easytracker/easytracker:1.0.0
    docker run --rm -it -u $(id -u):$(id -g) -v$PWD:$PWD -w $PWD \
      registry.gitlab.inria.fr/corse/easytracker/easytracker:1.0.0 \
      bash
    ...
    # Then test easytracker from the container for instance
    pip install -r requirements.txt
    pip install -r requirements_dev.txt
    export PYTHONPATH=$PWD
    make -C tests
