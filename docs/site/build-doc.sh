#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

# Get to the site dir and generate doc in site/
cd "$dir"

rm -rf site
mkdocs build --strict --clean
