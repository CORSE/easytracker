Build directory for the Easytracker site
========================================

The Easytracker site is deployed at [https://corse.gitlabpages.inria.fr/easytracker/](https://corse.gitlabpages.inria.fr/easytracker/).

In order to rebuild the site pages, execute from there:

    ./build-site.sh

The `build-site.sh` scripts first generates the documentation for the API, then the rest of the documentation, it is equivalent to:

    ./build-api.sh
    ./build-doc.sh

The generated pages are then available at `site/`.

The top level `make pages` target, in addition to building the site, copies the generated pages to the top level directory `public/`.

While authoring the dicumentation, one may rune the mkdoc server to see live changes with:

    mkdocs serve
