# Installing EasyTracker

## Requirements

EasyTracker requires the following for installation and usage:

- A Linux system or WSL system on Windows
- Python version >= 3.9
- GDB version >= 11.2 for using the GDB tracker

EasyTracker has been tested and used on Linux environment only,
please report an issue if interested in the support for another platform.


## Installation

EasyTracker is hosted on the Python Package repository at:
https://gitlab.inria.fr/CORSE/easytracker/-/packages

In order to install it for controlling Python programs, install the python tracker with:
```bash
pip3 install -U --extra-index-url https://gitlab.inria.fr/api/v4/groups/corse/-/packages/pypi/simple easytracker-python
```

In order to install the GDB tracker, do:
```bash
pip3 install -U --extra-index-url https://gitlab.inria.fr/api/v4/groups/corse/-/packages/pypi/simple easytracker-gdb
```

Optionally, specify the `extra-index-url` in the `~/.config/pip/pip.conf` file:
```
[install]
extra-index-url = https://gitlab.inria.fr/api/v4/groups/corse/-/packages/pypi/simple
```

And install as a standard PyPI package with:
```bash
pip3 install -U easytracker-python easytracker-gdb
```

Note that `-U` option is used for updating to the latest versions
of the packages.


## Testing installation

After installing the python tracker, one may run a simple sanity test
by running controlling the following python program `main-pow2.py`:

```python
--8<-- "docs/snippets/main-pow2.py"
```

With the following controller that will trace the values taken by the `res`
variable. Here is the controller file `install-test.py`:

```python
--8<-- "docs/snippets/install-test.py"
```

Run the controller with the python program as argument:

```bash
python3 install-test.py main-pow2.py
```

It should output:

```text
--8<-- "docs/snippets/install-test-output.txt"
```

In order to test the gdb controller, compile this C program as indicated in the comments:

```c
--8<-- "docs/snippets/main-pow2.c"
```

And execute the controller with the resulting binary:

```bash
python3 install-test.py main-pow2
```

I should output the same trace as the python execution shown above:

```text
--8<-- "docs/snippets/install-test-output.txt"
```
