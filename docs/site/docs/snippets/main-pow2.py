#!/usr/bin/env python3

# The program which will be controlled
def main():
    res = 2
    for _ in range(2):
        res *= 2
    return res

if __name__ == "__main__":
    main()
