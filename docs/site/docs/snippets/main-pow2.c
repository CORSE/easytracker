// Compile with: gcc -O0 -g -o main-pow2 main-pow2.c

// The program which will be controlled
int main() {
  int res = 2;
  for (int i = 0; i <2; i++)
    res *= 2;
  return res;
}
