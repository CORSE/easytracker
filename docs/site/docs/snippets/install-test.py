#!/usr/bin/env python3
import sys
from easytracker import init_tracker

# The controller either take the python or binary version from C
def controller(fname):
    tracker = init_tracker("python") if fname[-3:] == ".py" else init_tracker("gdb")
    tracker.load_program(fname)
    tracker.start()

    while tracker.exit_code is None:
        res = tracker.get_variable_value("res", as_raw_python_objects=True)
        if res is not None and tracker.last_lineno:
            print(f"res == {res.value} at line {tracker.last_lineno}")
        tracker.next()

    tracker.terminate()

if __name__ == "__main__":
    # Run the controller for the given program file
    fname = sys.argv[1] if len(sys.argv) > 1 else "main-pow2.py"
    controller(fname)
