#!/usr/bin/env python3

# Import init_tracker
from easytracker import init_tracker

def control():

    # Initialize a new tracker and load the program 
    tracker = init_tracker("python")
    tracker.load_program("mistery.py")

    # Start the program
    tracker.start()

    # The main tracker loop, process until the program exits
    while tracker.exit_code is None:
        # Resume execution and get reason for the stop
        pause_reason = tracker.resume()
        print(pause_reason)

    tracker.terminate()

if __name__ == "__main__":
    control()
