// Compile with: gcc -O0 -g -o mistery mistery.c

#include <stdio.h>
#include <stdlib.h>

int m(int x, int *elems, int elems_len) {
  int idx = elems_len - 1 - x;
  elems[idx] += 1;
  if (x == 0)
    return 1;
  int m1 = m(x - 1, elems, elems_len);
  int m2 = m(x - 1, elems, elems_len);
  return m1 + m2;
}

int main() {
  int r = 3;
  int *elems;
  elems = calloc(r + 1, sizeof(int));
  m(r, elems, r + 1);
  printf("r = %d -> elems = [", r);
  const char *sep = "";
  for (int i = 0; i < r + 1; i++) {
    printf("%s%d", sep, elems[i]);
    sep = ", ";
  }
  printf("]\n");
  free(elems);
  return 0;
}
