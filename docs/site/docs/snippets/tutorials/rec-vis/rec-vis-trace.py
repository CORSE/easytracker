#!/usr/bin/env python3

# Import init_tracker and pause reason type
from easytracker import init_tracker
from easytracker import PauseReasonType as prt

def control():

    # Initialize a new tracker and load the program 
    tracker = init_tracker("python")
    tracker.load_program("mistery.py")

    # Start the program
    tracker.start()

    # Ask the tracker to stop on entry/exit of m()
    tracker.track_function("m")

    # The main tracker loop, process until the program exits
    while tracker.exit_code is None:
        # Resume execution and get reason for the stop
        pause_reason = tracker.resume()
        if pause_reason.type == prt.CALL:
            # When entering the function, get argument values
            args = {
                arg:
                tracker.get_variable_value(arg, as_raw_python_objects=True).value
                for arg in ["x", "elems"]
            }
            print("CALL", args)
        elif pause_reason.type == prt.RETURN:
            # When exiting the function, get return value
            print("RET", pause_reason.args[2])


    tracker.terminate()

if __name__ == "__main__":
    control()
