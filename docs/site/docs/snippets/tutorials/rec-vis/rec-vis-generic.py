#!/usr/bin/env python3

# Import init_tracker and pause reason type
from easytracker import init_tracker
from easytracker import PauseReasonType as prt

def control(prog, func, args):

    # Initialize a new tracker and load the program 
    tracker = init_tracker("python" if prog.endswith(".py") else "GDB")
    tracker.load_program(prog)

    # Start the program
    tracker.start()

    # Ask the tracker to stop on entry/exit of m()
    tracker.track_function(func)

    # Init managed calls list for the visualization
    calls, current, idx = [], None, 0

    # The main tracker loop, process until the program exits
    while tracker.exit_code is None:
        # Resume execution and get reason for the stop
        pause_reason = tracker.resume()
        if pause_reason.type == prt.CALL:
            # When entering the function, handle argument values
            args = {
                arg:
                tracker.get_variable_value(arg, as_raw_python_objects=True).value
                for arg in args
            }
            current = handle_call(calls, current, args)
        elif pause_reason.type == prt.RETURN:
            # When exiting the function, handle return value
            current = handle_return(calls, current, pause_reason.args[2])
        else:
            continue
        idx += 1
        visualize_calls(f"rec-{idx:03}", calls[0], func)

    tracker.terminate()


def handle_call(calls, current, args):
    from types import SimpleNamespace as ns
    from copy import deepcopy
    current = ns(
        args=[deepcopy(x) for x in args.values()],
        idx=len(calls),
        child=[],
        parent=current,
        active=True,
        retval=None,
    )
    if current.parent is not None:
        current.parent.child.append(current)
    calls.append(current)
    return current

def handle_return(calls, current, retval):
    current.retval = retval
    current.active = False
    return current.parent

def visualize_calls(name, root, func_name):
    from subprocess import run
    with open(f'{name}.dot', "w") as dotf:
        dotf.write('digraph rec {\n')
        dump_call_tree(dotf, root, func_name)
        dotf.write('}\n')
    run(f'dot -Tsvg {name}.dot -o {name}.svg', shell=True)

def dump_call_tree(dot_file, call, func_name):
    args = ', '.join([str(arg) for arg in call.args])
    color = 'chartreuse' if call.active else 'lightgray'
    dot_file.write(
        f'{call.idx} '
        f'[color="{color}" style="filled"'
        f'shape="rect" margin="0" width="1"'
        f'label="{call.idx}.\\n{func_name}({args})"]\n')
    if call.parent:
        ecol = 'black' if call.active else 'lightgray'
        rcol = 'black' if call.parent.active else 'lightgray'
        rsty = 'style="invis"' if call.active else ''
        dot_file.write(
            f'{call.parent.idx} -> {call.idx} '
            f'[color="{ecol}"]\n')
        dot_file.write(
            f'{call.idx} -> {call.parent.idx} '
            f'[label="{call.retval}" '
            f'color="{rcol}" {rsty}]\n')
    for child in call.child:
        dump_call_tree(dot_file, child, func_name)

if __name__ == "__main__":
    import sys
    prog = "mistery.py" if len(sys.argv) <= 1 else sys.argv[1]
    func = "m" if len(sys.argv) <= 2 else sys.argv[2]
    args = ("x", "elems") if len(sys.argv) <= 2 else sys.argv[3:]
    control(prog, func, args)
