Recursive visualization tool
============================

This tutorial shows how to visualize easily the call tree of a self recursive function,
using the API of Easytracker for:

- loading a python or C program;
- registering program stop for a function entry/exit;
- starting the tracker;
- getting some variable values when the program is stopped;
- interpreting the memory model for displaying it. 


# The program to instrument

Lets's start, by an input python program `mystery.py`, which contains a recursive function `m()`:

```python
--8<-- "docs/snippets/tutorials/rec-vis/mistery.py"
```

At first sight, the behavior of the function is not obvious but it may be observed that:

- it has two parameters `x` (an integer) and `elems` (a list of integers);
- it is a double recursive function, meaning that for each call:
    - it either returns immediately or
    - it calls itself twice before returning;
    - the recursion is stopped when `x == 0` and the recursive calls set `x - 1` as the new parameter.

Hence, one can imagine that the actual call graph created by the calls from the function to itself
will be a binary tree of depth the initially passed `x` value.

Let's try to visualize this call graph thanks to easytracker.

Note that one can execute the program and see the resulting `elems` list values, here is the output
of `python3 mistery.py`:

```text
--8<-- "docs/snippets/tutorials/rec-vis/mistery-output.txt"
```


# The no-op controller

In a first step we will create a program controller which simply runs the
program with a generic tracker event loop that does nothing.

The first part of a tracker is to initialize and load the program with:

- `init_tracker("python")` which creates a new tracker for a python program;
- `tracker.load_program("mistery.py")` which loads the python program to inspect.

The second part is the generic event loop for the tracker which:

- starts the program execution with `tracker.start()`, this will run until the `main` program function is reached;
- loops until the tracker has exited, i.e. `while tracker.exit_code is None: ...`;
- resume the program at each loop step with `tracker.resume()`;

Last, when the loop exits, the tracker resources are freed with `tracker.terminate()`.

Here is the controller program `rec-vis-noop.py`:

```python
--8<-- "docs/snippets/tutorials/rec-vis/rec-vis-noop.py"
```

Running this no-nop tracker with `python3 rec-vis-noop.py` gives the following output, one can see that the loop runs once and exits
as the pause reason is actually a program exit event:

```text
--8<-- "docs/snippets/tutorials/rec-vis/rec-vis-noop-output.txt"
```


# The call trace controller

In a second step, we will augment the no-op controller to track the entry and exit of the
function of interest and print arguments and return values.

In order to do this, we will:

- call `tracker.track_function("m")` before the event loop in order to request program stop on
  the `m()` function calls and returns;
- call `tracker.get_variable_value("x")` in the event loop, when the tracker is stopped dues to
  a `CALL` pause reason type. This returns the value of the variable `x` at this point. Note that this
  function returns by default the value in the easytracker generic object model, in order to get the value
  directly into a raw python object, one can add the `as_raw_python_objects=True` argument;
- get the return value in the event loop when the program is stopped due to a `RETURN` pause reason type.
  Note that the return value does not have an explicit name in the program, it is provided with the
  pause reason as `pause.reason.args[2]`.

Here is the controller program `rec-vis-trace.py`:

```python
--8<-- "docs/snippets/tutorials/rec-vis/rec-vis-trace.py"
```

Running this tracker with `python3 rec-vis-trace.py` gives the following output:

```text
--8<-- "docs/snippets/tutorials/rec-vis/rec-vis-trace-output.txt"
```

Note that, as we expected from the first program analysis, the number of times the function is called is `15`, i.e. quadratic in term of the initial value of `x`, as we may expect from a double recursive function.


# The call graph controller

In next step of this tutorial, we will actually generate a call graph
with:

- nodes containing the function name and parameters values;
- edges representing recursive calls to the function;
- backward edges representing return values from the recursive calls.

In order to do this we will basically adapt the trace controller to
generate a `.dot` format graph for each call and return from the function
instead of generating textual trace. Each graph will be converted to `.svg`
with the `dot` tool from the `graphviz` package.


The first step is to modify the textual trace into calls to functions
 `handle_call()` and `handle_ret()` which will iteratively contruct the list
 of call frames and maintain the reference to the current active frame.

The second step is to generate at each loop iteration the current
call graph with the `visualize_calls()` function.

Here is the controller program `rec-vis-graph.py`:

```python
--8<-- "docs/snippets/tutorials/rec-vis/rec-vis-graph.py"
```

Before executing the program, ensure that the `graphviz` package is installed, for instance with:

```bash
sudo apt install graphviz
```

Then execute `python3 rec-vis-graph.py` which wil generate in the current directory a number of `.svg` file, one for each call and return from the function.

One can see all generated graphs with for instance:

```bash
eog rec-*.svg
```

As an example, here is the `rec-019.svg` graph.

![Rec Visu at step 19](rec-019.svg)

As one can see, the `m()` call graph is indeed a binary tree. The
function actually computes the increasing power of 2 values in the `elems`
array.


# The generic controller

We've seen the controlle for the python `mistery.py` program,
though we can use the same controller with minor modifications for
controlling either a C or python file.

For this case we just have to modify:

- the controller to take a `prog` parameter on the command line;
- the tracker initialization to `init_tracker("python" if prog.endsswith(".py") else "GDB")`

Here is the final `rec-vis-generic.py` program:

```python
--8<-- "docs/snippets/tutorials/rec-vis/rec-vis-generic.py"
```

Let's test it with a C version of our program `mistery.c`:

```c
--8<-- "docs/snippets/tutorials/rec-vis/mistery.c"
```

One can then run the same controller for either:

- python with `python3 rec-vis-generic.py mistery.py`, or
- C with `python3 rec-vis-generic.py mistery`, after compiling the `mistery` program as shown in the source file comments.

They both output the same graphs visualisation.
