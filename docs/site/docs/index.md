# Welcome to EasyTracker

## About

EasyTracker is a Python library which provides a simple API for
controlling in a language agnostic way an inferior program written
in Python, or any compiled language supporting debugging through
GDB such as C, C++ or Rust.

The primary objective of EasyTracker is to be a building block
for creating visualization of running programs in particular for
teaching purpose, though it can be used in many contexts such as
for tracing, monitoring or building debug scripts.

The API consists essentially in commands that one may found in a
traditional debugger such as putting breakpoints and watchpoints
and stepping or resuming the program execution. In addition it
provides some higher level actions on control such as tracking
functions entry and exit, on data such as watching future
local variables and on introspection such as getting frame
variables and language agnostic variable values through a
generic memory model consisting of primitive values, references
in stack or heap and string representation of other language
specific object.

## Links

This documentation: [https://corse.gitlabpages.inria.fr/easytracker](https://corse.gitlabpages.inria.fr/easytracker)

Easytracker issues: [https://gitlab.inria.fr/CORSE/easytracker/-/issues](https://gitlab.inria.fr/CORSE/easytracker/-/issues)

Easytracker Python packages: [https://gitlab.inria.fr/CORSE/easytracker/-/packages](https://gitlab.inria.fr/CORSE/easytracker/-/packages)

Easytracker sources: [https://gitlab.inria.fr/CORSE/easytracker](https://gitlab.inria.fr/CORSE/easytracker)

## Versions

This doc was generated for EasyTracker packages versions:

- easytracker-base version {{ package_version('easytracker-base') }}.
- easytracker-python version {{ package_version('easytracker-python') }}.
- easytracker-gdb version {{ package_version('easytracker-gdb') }}.
