import subprocess


def define_env(env):
    def package_version(package):
        tag_base = package.replace("easytracker-", "")
        proc = subprocess.run(
            ["git", "describe", "--tags", "--abbrev=0", f"--match={tag_base}-v*"],
            text=True,
            check=True,
            capture_output=True,
        )
        return proc.stdout.strip()

    env.macro(package_version)
