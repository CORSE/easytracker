#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

# Get to easytracker topdir and generate api.md in docs/site/docs/api.md
cd "$dir"/../..

pydoc-markdown \
    -m easytracker.interface \
    -m easytracker.types.pause_reason \
    -m easytracker.types.frame \
    -m easytracker.types.variable \
    -I . > docs/site/docs/api.md.tmp

mv docs/site/docs/api.md.tmp docs/site/docs/api.md
