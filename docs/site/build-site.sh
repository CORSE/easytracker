#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

# Build Snippets, API and doc
"$dir"/build-snippets.sh
"$dir"/build-api.sh
"$dir"/build-doc.sh
