#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

# Get to snippets dir and verify expected output
cd "$dir"/docs/snippets

set -x
python3 install-test.py main-pow2.py >install-test-output.txt.tmp
diff -u install-test-output.txt.tmp install-test-output.txt

command=$(grep '// Compile with:' main-pow2.c | sed 's/.*://')
eval "$command"
python3 install-test.py main-pow2 >install-test-output.txt.tmp
diff -u install-test-output.txt.tmp install-test-output.txt
