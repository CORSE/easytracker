#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

# Get to snippets dir and verify expected output
cd "$dir"/docs/snippets/tutorials/rec-vis

set -x
out=mistery-output.txt # default program output, tracker does not output anything
python3 mistery.py 2>&1 | tee $out.tmp
diff -u $out.tmp $out

out=rec-vis-noop-output.txt # default program output, tracker does not output anything
python3 rec-vis-noop.py 2>&1 | tee $out.tmp
diff -u $out.tmp $out

out=rec-vis-trace-output.txt # trace and program output
python3 rec-vis-trace.py 2>&1 | tee $out.tmp
diff -u $out.tmp $out

rm -f *.svg *.dot
python3 rec-vis-graph.py
diff -u rec-019.dot rec-019.dot.ref

rm -f *.svg *.dot
python3 rec-vis-generic.py mistery.py m x elems
diff -u rec-019.dot rec-019.dot.ref

command=$(grep '// Compile with:' mistery.c | sed 's/.*://')
eval "$command"
rm -f *.svg *.dot
python3 rec-vis-generic.py mistery m x elems
diff -u rec-019.dot rec-019.dot.ref
