#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

"$dir"/build-snippets-install.sh
"$dir"/build-snippets-tutorials-rec-vis.sh
