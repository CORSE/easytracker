"""Quality of life module to be used by tools"""

from typing import Union, Optional

from easytracker.abstract.abstract_tracker import EasyTrackerItf
from easytracker.types.variable import Struct, Variable, RawVariable
from easytracker.types.frame import Frame, RawFrame
from easytracker.init_tracker import init_tracker_and_start
from easytracker.weak_compare_dict import weak_compare_dict


def step_loop_until_end(prog_name: str):
    """Decorator for a loop body to run it after each step until the end.
    The loop body takes the initalized tracker as arguments"""

    def decorator(loop_body):
        def main_loop():
            tracker = init_tracker_and_start(prog_name)
            while tracker.exit_code is None:
                # call the decorated function
                loop_body(tracker)

                tracker.step()

        return main_loop

    return decorator


def filter_functions(
    variables: dict[str, Union[Variable, RawVariable]]
) -> dict[str, Union[Variable, RawVariable]]:
    """Removes the global functions from the list of globals.
    This is supposed to be called on Python memory graph.
    If this function is called for a C memory graph converted to Python memory it will fail
    """
    new_dict = dict(variables)

    for name, var in new_dict.items():
        func_name = var.name
        value = var.value
        if isinstance(var, Variable):
            value = value.content.content  # type: ignore
        if type(value).__name__ == "function":
            # force change type, this is a placeholder instead of deleting the entry
            new_dict[name] = f"Function {func_name}"  # type: ignore

    return new_dict


def variable_dict_to_struct(
    variables: dict[str, Union[Variable, RawVariable]]
) -> Struct:
    """Converts the given dict to an attribute accessible struct"""
    struct = Struct()
    for var_name, var in variables.items():
        setattr(struct, var_name, var.value)

    return struct


# testing facility
def assert_eq(lhs: object, rhs: object):
    """Asserts that the two given objects are equal"""
    if lhs != rhs:
        raise AssertionError(f"{lhs} and {rhs} should be equal")


def assert_neq(lhs: object, rhs: object):
    """Asserts that the two given objects are different"""
    if lhs == rhs:
        raise AssertionError(f"{lhs} and {rhs} should be different")


def compare_tracker_state(
    tracker: EasyTrackerItf,
    raw: bool,
    stack_ref: Union[Frame, RawFrame],
    glob_vars_ref: Optional[dict[str, Union[Variable, RawVariable]]] = None,
    is_python=False,
) -> bool:
    """Compares the given memory graphs to the current tracker state"""
    # test global variables values
    if glob_vars_ref is not None:
        glob_vars = tracker.get_global_variables(raw)
        if is_python:
            glob_vars = filter_functions(glob_vars)

        if raw:
            failed = glob_vars != glob_vars_ref
        else:
            failed = not weak_compare_dict(glob_vars, glob_vars_ref)  # type: ignore

        if failed:
            print("actual globals")
            print(glob_vars)
            print("ref globals")
            print(glob_vars_ref)
            return False

    # test stack equivalence
    stack = tracker.get_current_frame(raw)
    if raw:
        failed = stack != stack_ref
    else:
        failed = not stack.weak_equality(stack_ref)  # type: ignore

    if failed:
        print("actual frame")
        print(stack)
        print("ref frame")
        print(stack_ref)
        return False

    return True
