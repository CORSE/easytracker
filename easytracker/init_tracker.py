""" A module to setup the requested tracker """

import os
import sys
import importlib
from typing import Optional

from easytracker.interface import EasyTrackerItf

# allow global reference to tracker instance
# pylint: disable=global-statement, invalid-name


class EasyTracker:
    """abstract instance are not meant to be used they are placeholders
    before a real tracker initialization
    """

    # pylint: disable=too-few-public-methods
    def __init__(self):
        self.instance: EasyTrackerItf = None


def init_tracker(tracker_name: str) -> EasyTrackerItf:
    """sets the global tracker reference to the required tracker"""
    legacy_trackers = {
        "GDB": ("easytracker-gdb", "easytracker_gdb.gdb_tracker", "GdbTracker"),
        "python": (
            "easytracker-python",
            "easytracker_python.python_tracker",
            "PythonTracker",
        ),
    }
    from_dir = None
    if tracker_name in legacy_trackers:
        dist, package, cls = legacy_trackers[tracker_name]
        # For legacy trackers, we still support setting PYTHONPATH=<toplevel>
        # if this file is actually in source dir, we add the subpackage path
        legacy_top_path = os.path.realpath(
            os.path.join(os.path.dirname(__file__), "..")
        )
        in_pythonpath = [
            True for p in sys.path if os.path.realpath(p) == legacy_top_path
        ]
        if in_pythonpath:
            legacy_path = os.path.join(legacy_top_path, dist)
            if os.path.exists(legacy_path):
                from_dir = legacy_path
    else:
        # External trackers are expected to be found in a Tracker() class of an easytracker_<tracker> module.
        # It should be installable through an easytracker-<tracker> package.
        dist, package, cls = (
            f"easytracker-{tracker_name}",
            f"easytracker_{tracker_name}",
            "Tracker",
        )
    try:
        if from_dir:
            sys.path.insert(0, from_dir)
        pkg = importlib.import_module(package)
        if from_dir:
            sys.path.remove(from_dir)
    except ModuleNotFoundError as e:
        if e.name != package.split(".")[0]:
            raise
        raise ModuleNotFoundError(
            f"Module {e.name} not found. Tracker {tracker_name} is probably not installed. "
            f"Install with:\n"
            f"  pip install --extra-index-url https://gitlab.inria.fr/api/v4/groups/corse/-/packages/pypi/simple {dist}"
        ).with_traceback(e.__traceback__) from None

    clss = getattr(pkg, cls)
    tracker.instance = clss()

    return tracker.instance


def get_tracker_instance() -> EasyTrackerItf:
    """Returns the global tracker instance"""
    return tracker.instance


def init_tracker_from_prog_name(program_name: str) -> EasyTrackerItf:
    """Reads the program name and creates a tracker depending on the extension"""
    if program_name.endswith(".py"):
        tracker_name = "python"
    else:
        tracker_name = "GDB"
    return init_tracker(tracker_name)


def init_tracker_and_start(program_path: Optional[str] = None) -> EasyTrackerItf:
    """Init a tracker and load the given program and start, if the given name is None, a.out is loaded"""
    if program_path is None:
        program_path = "a.out"

    tracker_ist = init_tracker_from_prog_name(program_path)
    tracker_ist.load_program(program_path)
    tracker_ist.start()

    return tracker_ist


# tracker instance
tracker = EasyTracker()
