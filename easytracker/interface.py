"""
A module defining Easytracker API.

Trackers implementing this API must inherit the EasyTrackerItf abstract class below.
The interface also defines some public types in the types/ module.
The file init_tracker.py includes a function to build a tracker from a list of available trackers.

All the control commands return the pause reason described in the types/pause_reason.py file.
"""

# Standard imports first
from abc import ABC, abstractmethod
from typing import Optional, Union

# Own imports last
from easytracker.types.variable import Variable, RawVariable, NameBinding
from easytracker.types.frame import Frame, RawFrame
from easytracker.types.pause_reason import PauseReason


# pylint: disable=too-many-public-methods
class EasyTrackerItf(ABC):
    """
    The abstract class defining the Easytracker API.
    """

    # === Loading and stopping inferior (not described in paper)
    @abstractmethod
    def load_program(self, prog_file_path: str) -> None:
        """
        Loads a new inferior.

        :param prog_file_path: the file path of the inferior to load
        """

    @abstractmethod
    def force_program_stop(self) -> None:
        """
        Forces a stop of the inferior and resets the internal state.
        """

    # === Program control
    # All the fonctions in program control are blocking
    # and returns only when inferior program is paused
    # or terminated.
    # Resume interface
    @abstractmethod
    def next(self, count: int = 1) -> PauseReason:
        """
        Executes a single line of the inferior.

        Does not step INSIDE function call if the line
        makes such a call.

        :param count: the next command is repeated count times
        :returns: the pause reason
        """

    @abstractmethod
    def step(self, count: int = 1) -> PauseReason:
        """
        Executes a single line of the inferior.

        Step INSIDE function call if the line
        makes such a call.

        :param count: the step command is repeated count times
        :returns: the pause reason
        """

    @abstractmethod
    def start(self, in_file: str = None) -> PauseReason:
        """
        Starts the inferior.

        Starts execution of the inferior and breaks on
        the first line of the main function.

        :params in_file: if not None, this is given to inferior stdin
        :returns: the pause reason
        """

    @abstractmethod
    def run(self, in_file: str = None) -> PauseReason:
        """
        Runs the inferior.

        Executes the inferior from the main function until a pause condition
        (breakpoint, watchpoint, function tracked) is reached or until the main
        function returns.

        :params in_file: if not None, this is given to inferior stdin
        :returns: the pause reason
        """

    @abstractmethod
    def resume(self) -> PauseReason:
        """
        Continues execution until a pause condition
        (breakpoint, watchpoint, function tracked) is reached or until the main
        function returns.

        :returns: the pause reason
        """

    # Pause interface
    @abstractmethod
    def break_before_line(self, lineno: int, maxdepth: Optional[int] = None) -> int:
        """
        The tracker will stop before executing the given line.

        :param lineno: the line number where to put a breakpoint
        :param maxdepth: if not None the tracker won't stop if the frame is deeper than maxdepth
        :returns: the identifier of the newly created breakpoint
        """

    @abstractmethod
    def break_before_func(self, funcname: str, maxdepth: Optional[int] = None) -> int:
        """
        The tracker will stop before executing the first line of the given function body.

        When the breakpoint is hit, the arguments of the functions are already set.

        :param maxdepth: if not None the tracker won't stop if the frame is deeper than maxdepth
        :returns: the identifier of the newly created function breakpoint
        """

    @abstractmethod
    def break_end_of_func(self, funcname: str, maxdepth: Optional[int] = None) -> int:
        """
        The tracker will stop after executing the last line of the given function body.

        When breaking, the BREAKPOINT pause reason contains the value returned by the
        function and :

        - in the Python tracker, the function's frame is still available
        - in the GDB tracker, the function's frame is no more available

        :param maxdepth: if not None the tracker won't stop if the frame is deeper than maxdepth
        :returns: the identifier of the newly created function watchpoint
        """

    @abstractmethod
    def delete_breakpoint(self, bp_num: int) -> None:
        """
        Deletes the breakpoint or watchpoint with the given identifier.

        Does nothing if the breakpoint is already deleted or the identifier corresponds to nothing.

        :param bp_num: the identifier of the breakpoint to delete
        """

    # === Watchpoints management === #
    @abstractmethod
    def watch(self, variable: Union[str, NameBinding]) -> int:
        """
        Adds a new watchpoint on the given variable.

        The execution will stop just after this variable is modified.
        If the same value is written again it won't trigger the watchpoint.

        :param variable: the variable to watch, either as a string or as a string with an optional function's name
        :returns: the identifier of the newly created watchpoint
        """

    @abstractmethod
    def track_function(self, funcname: str, maxdepth: Optional[int] = None) -> int:
        """
        Adds a new breakpoint on the given function entry and return.
        When returning, the RETURN pause reason includes the returned value.

        When breaking because returning a track function :

        - in the Python tracker, the function's frame is still available
        - in the GDB tracker, the function's frame is no more available

        :param maxdepth: if not None the tracker won't stop if the frame is deeper than maxdepth
        :returns: the identifier of the newly created function watchpoint
        """

    # === Inspection of program state === #
    @property
    @abstractmethod
    def exit_code(self) -> Optional[int]:
        """
        Returns the program exit code.

        :returns: the program exit code if the inferior has ended, else returns None
        """

    @property
    @abstractmethod
    def last_lineno(self) -> Optional[int]:
        """
        Returns the number, in self.last_source_file, of the last executed line.

        :returns: the number of the last executed line or None if the inferior
         has not been started or is ended, or if no line has been executed yet
        """

    @property
    @abstractmethod
    def next_lineno(self) -> Optional[int]:
        """
        Returns the number, in self.next_source_file, of the next line to be executed.

        :returns: the number of the next line to be executed if the inferior has
         not been started or is ended returns None
        """

    @property
    @abstractmethod
    def last_source_file(self) -> Optional[str]:
        """
        Returns the absolute path of the last source file this tracker was stopped at.

        :returns: the absolute path of the last source file this tracker was stopped at.
        """

    @property
    @abstractmethod
    def next_source_file(self) -> Optional[str]:
        """
        Returns the absolute path of the source file this tracker is currently stopped at.

        :returns: the absolute path of the source file this tracker is currently stopped at
        """

    @abstractmethod
    def get_current_frame(
        self, as_raw_python_objects: bool = False
    ) -> Optional[Union[Frame, RawFrame]]:
        """
        Returns the current frame.

        The frame contains a list of variables, a name, a depth and eventually
        a parent frame up to the main frame as described in types/frame.py.

        :params as_raw_python_objects: if True returns a RawFrame else returns a Framex
        :returns: the current frame or None if the main frame exited or no program is loaded
        """

    @abstractmethod
    def get_global_variables(
        self, as_raw_python_objects: bool = False
    ) -> dict[str, Union[Variable, RawVariable]]:
        """
        Returns the list of all global variables and their values.

        :params as_raw_python_objects: if True returns a dict containing RawVariable else
         returns a dict containing Variable
        :returns: a dict from variable names to Variable or RawVariable
        """

    @abstractmethod
    def get_variable_value(
        self, name: str, as_raw_python_objects: bool = False
    ) -> Union[Variable, RawVariable, None]:
        """
        Returns the value of the given variable identifier.

        :params as_raw_python_objects: if True returns a RawFrame else returns a Framex
        :returns: the value of the variale or None If the variable does not exist in current function
        """

    @property
    @abstractmethod
    def pause_reason(self) -> PauseReason:
        """
        Returns why the inferior has been paused.

        When using step/next AND watchpoints, several reasons may cause the inferior
        to pause. However we only return the reason with the highest priority as
        described in types/pause_reason.py.

        :returns: why the inferior has been paused
        """

    # === Tracker specific stuff === #
    @abstractmethod
    def send_direct_command(self, command: str) -> PauseReason:
        """
        Sends a non empty command to the tracker.

        :param command: the tracker dependant command to be sent
        :returns: the pause reason that can be empty (UNKNOWN) if the program was not run
        """

    # === Tracker life control === #
    def terminate(self) -> None:
        """
        Terminates the tracker.

        This function MUST be called when the tracker is not used anymore since it
        performs any required cleanup.
        """
