"""public re-exports"""

try:
    from importlib.metadata import version

    __version__ = version("easytracker-base")
except:  # pylint: disable=bare-except
    # only useful in no install mode (with PYTHONPATH)
    # change only for major rev M to: M.0.1+dYYYMMDD
    __version__ = "1.0.1+d20230704"

from .init_tracker import get_tracker_instance as tracker_interface
from .init_tracker import init_tracker, init_tracker_from_prog_name

# types export
from .types.variable import (
    Variable,
    RawVariable,
    Value,
    Struct,
    Location,
    InvalidMemory,
    AbstractType,
    NameBinding,
)
from .types.frame import Frame
from .types.pause_reason import PauseReasonType, PauseReason, DepthChange
