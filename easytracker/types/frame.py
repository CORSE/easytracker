"""
Module defining frames.
"""

from __future__ import annotations
from dataclasses import dataclass
from typing import Optional

from easytracker.types.variable import Variable, RawVariable
from easytracker.weak_compare_dict import weak_compare_dict


@dataclass
class _BaseFrame:
    """
    Base abstract dataclass for both RawFrame and Frame classes.
    """

    name: str
    depth: int  # the depth of the current frame in the stack frame
    next_source_file: str  # the source file associated to this frame
    next_lineno: int  # the next line to be executed (IN next_source_file)


@dataclass
class RawFrame(_BaseFrame):
    """
    A Frame which variables are RawVariable.
    """

    variables: dict[str, RawVariable]
    parent: Optional[RawFrame]


@dataclass
class Frame(_BaseFrame):
    """
    A Frame which variables are Variable.
    """

    variables: dict[str, Variable]
    parent: Optional[Frame]

    def weak_equality(self, other: Frame) -> bool:
        """
        Override equality for testing
        """
        if not (
            self.name == other.name
            and self.depth == other.depth
            and self.next_source_file == other.next_source_file
            and self.next_lineno == other.next_lineno
        ):
            return False
        if self.parent is None:
            if other.parent is not None:
                return False
        else:
            if other.parent is None or not self.parent.weak_equality(other.parent):
                return False

        return weak_compare_dict(self.variables, other.variables)
