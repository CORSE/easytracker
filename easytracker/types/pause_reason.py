"""
Module defining the list of pause reasons and their priority.
"""
from dataclasses import dataclass, field
import typing
from enum import Enum, IntEnum


class PauseReasonType(IntEnum):
    """
    Enum type for the pause reason.
    The value of the enum represents the priority.
    """

    EXITED = 1
    """The end of the main function has been reached"""

    SIGNAL = 5
    """A signal has been sent"""

    CALL = 10
    """The entry of a tracked function has been reached"""

    RETURN = 15
    """The exit of a tracked function has been reached"""

    WATCHPOINT = 20
    """A watchpoint has been reached"""

    BREAKPOINT = 25
    """A break point (line or function) has been reached"""

    FUNCTION_FINISHED = 30
    """We got out a function following a call to the tracker finish method"""

    ENDSTEPPING_RANGE = 50
    """"""

    START = 77
    """The tracker start method has been called."""

    UNKNOWN = 99
    """Unknown pause reason."""


@dataclass(frozen=True)
class PauseReason:
    """
    Contains the pause reason type and depending on this type some additional information in
    the `args` list as following :

    - EXITED has the return code (`args` list of size 1)
    - SIGNAL has the exit code, the signal name and the signal meaning
      (`args` list of size 3)
    - CALL has the (internal) breakpoint id and the function name
      (`args` list of size 2)
    - RETURN has the (internal) breakpoint id, the function name, and
      the return value of the function (`args` list of size 3)
    - WATCHPOINT has the new variable value as info and some other stuff
      depending on the tracker
      (`args` list of size X)
    - BREAKPOINT has no info (`args` list of size 0)
    - FUNCTION_FINISHED has no info (`args` list of size 0)
    - ENDSTEPPING_RANGE has no info (`args` list of size 0)
    - START has no info (`args` list of size 0)
    - UNKNOWN has no info (`args` list of size 0)

    Moreover, all pause reason types but EXIT, SIGNAL and START have `line` set to
    the next line number to be executed.
    """

    type: PauseReasonType
    line: typing.Optional[int] = None
    args: list[object] = field(default_factory=list)


class DepthChange(Enum):
    """
    Enum type for function call/return/nothing.
    """

    NONE = 0
    STEP = 1
    CALL = 2
    RETURN = 3
