"""
Module defining variables.
"""

from __future__ import annotations
from dataclasses import dataclass
from typing import Optional, Union
from enum import Enum


# ==== Variable, heap objects and struct definition
class Location(Enum):
    """
    An enum to define possible location in memory of a value.
    """

    STACK = 0
    HEAP = 1
    GLOBAL = 2
    CODE = 3
    UNKNOWN = 4


class AbstractType(Enum):
    """
    An enum to interpret the content of a value.
    """

    PRIMITIVE = 0
    """
    - C: int, float and string literals 
    - Python: int, float, str
    - Content type: int | float | str
    - Raw: maps to int | float | str
    """

    REF = 1
    """
    - C: pointers
    - Python: variables, attributes
    - Content type: Value
    - Raw: maps to Any (depending on pointed type)
    """

    LIST = 2
    """
    - C: malloc of size > 1, static arrays, variable lengths arrays
    - Python: list, tuples
    - Content type: tuple[Value]
    - Raw: maps to list[Any]
    """

    DICT = 3
    """
    - C: N/A
    - Python: dict
    - Content type: dict[Value, Value]
    - Raw: maps to dict[Any, Any]
    """

    STRUCT = 4
    """
    # C: struct
    # Python: class instances
    # Content type: dict[str, Value]
    # Raw: maps to dict[str, Any]
    """

    NONE = 5
    """
    # C: N/A
    # Python: NONE
    # Content type: None
    # Raw: None
    """

    INVALID = 6
    """
    - C: value pointed by invalid pointer
    - Python: N/A
    - Content type: None
    - Raw: maps to InvalidMemory
    """

    FUNCTION = 7
    """
    - C: function pointers
    - Python: functions
    - Content type: str (function's name)
    - Raw: maps to str (function's name)
    """

    UNKNOWN = 8
    """
    - C: N/A
    - Python:
       + slots only classes
       + modules
    - Content type: str representation
    - Raw: maps to str (representation)
    """


@dataclass(frozen=True)
class Value:
    """
    A class to represent a Value, that is a block of memory.
    """

    # the location of the memory block on the stack, heap or global variable
    location: Location

    # the address of the memory block, this is None only for Python REF values
    address: Optional[int]

    # the name of the type as expressed in the language of the inferior
    language_type: str

    # the abstract type to interpret content
    abstract_type: AbstractType

    # content is interpreted according to the abstract_type
    content: Union[
        int,
        float,
        str,
        "Value",
        tuple["Value"],
        dict[Value, Value],
        dict[str, Value],
        None,
    ]

    def __repr__(self):
        address_str = hex(self.address) if self.address else "N/A"
        return (
            f"Value: abstract type={self.abstract_type}, "
            f"address={address_str}, location={self.location}, content=({str(self.content)})"
        )

    def to_human_str(
        self, inline_ref: bool = False, seen: dict[int, str] = None
    ) -> str:
        """
        Converts the value to a human-readable string.

        If inline_ref is True, references are inlined in their parent.
        For example, a STRUCT with one PRIMITIVE field and a REFERENCE
        to a PRIMITIVE field is displayed as :
          (f_one=1, f_two=ref to 0x7fb6e334) if inline_ref is false
          (f_one=1, f_two="two") if inline_ref is true
        """
        if seen is None:
            seen = {}
        res = seen.get(id(self), None)
        if res is None:
            if self.abstract_type == AbstractType.PRIMITIVE:
                delimiter = '"' if isinstance(self.content, str) else ""
                res = f"{delimiter}{str(self.content)}{delimiter}"
            elif self.abstract_type == AbstractType.NONE:
                res = "None"
            elif self.abstract_type == AbstractType.REF:
                seen[id(self)] = "..."
                pointed_value: Value = self.content  # type: ignore
                if inline_ref:
                    res = pointed_value.to_human_str(inline_ref=inline_ref, seen=seen)
                else:
                    res = f"ref to 0x{pointed_value.address:0x}"
            elif self.abstract_type == AbstractType.LIST:
                seen[id(self)] = "[...]"
                list_value: list[Value] = self.content  # type: ignore
                res = f"[{', '.join(c.to_human_str(inline_ref=inline_ref, seen=seen) for c in list_value)}]"
            elif self.abstract_type == AbstractType.STRUCT:
                seen[id(self)] = "..."
                struct_value: dict[str, Value] = self.content  # type: ignore
                res = f"""({', '.join(f'{attr_name}='
                                       f'{attr_value.to_human_str(inline_ref=inline_ref, seen=seen)}'
                                       for attr_name, attr_value in struct_value.items())}"""
                res += ")"
            elif self.abstract_type == AbstractType.DICT:
                seen[id(self)] = "{...}"
                dict_value: dict[Value, Value] = self.content  # type: ignore
                res = f"""{{{', '.join(f'{attr_key_value.to_human_str(inline_ref=inline_ref, seen=seen)}: '
                                       f'{attr_value_value.to_human_str(inline_ref=inline_ref, seen=seen)}'
                                       for attr_key_value, attr_value_value in dict_value.items())}"""
                res += "}"
            elif self.abstract_type == AbstractType.INVALID:
                res = "invalid value"
            else:
                raise NotImplementedError(
                    "abstract type not implemented in to_human_str"
                )
            seen[id(self)] = res
        return res

    def weak_equality(self, other: Value) -> bool:
        """Override equality for testing"""
        if not (
            self.location == other.location
            and self.language_type == other.language_type
            and self.abstract_type == other.abstract_type
        ):
            return False

        if self.abstract_type in [AbstractType.PRIMITIVE, AbstractType.FUNCTION]:
            return self.content == other.content
        if self.abstract_type == AbstractType.REF:
            # dont check destination address equality
            assert isinstance(other.content, Value), other.content
            return self.content.weak_equality(other.content)  # type: ignore
        if self.abstract_type == AbstractType.INVALID:
            return True
        if self.abstract_type == AbstractType.LIST:
            if len(self.content) != len(other.content):  # type: ignore
                return False
            return all(
                map(
                    lambda e: e[0].weak_equality(e[1]),  # type: ignore
                    zip(self.content, other.content),  # type: ignore
                )
            )
        raise NotImplementedError


class InvalidMemory(Value):
    """
    A class to represent invalid memory.
    """

    def __init__(self, address: int):
        super().__init__(
            Location.UNKNOWN, address, type(None).__name__, AbstractType.INVALID, None
        )


@dataclass(frozen=True)
class NameBinding:
    """
    A dataclass to hold a name binding without value.
    """

    name: str

    function_name: Optional[
        str
    ] = None  # if the variable is global or static the function_name is None
    # for now, we support only function level function_names

    depth: Optional[int] = None  # the depth of the function_name in the stack frame
    filename: Optional[str] = None  # the name binding can be specific to a source file


@dataclass(frozen=True)
class Variable(NameBinding):
    """
    A class to represent a variable in the stack.
    """

    value: Value = InvalidMemory(0)

    def weak_equality(self, other: Variable) -> bool:
        """Override equality for testing"""
        if not (
            self.name == other.name
            and self.function_name == other.function_name
            and self.depth == other.depth
            and self.filename == other.filename
        ):
            return False
        return self.value.weak_equality(other.value)


@dataclass(frozen=True)
class RawVariable(NameBinding):
    """
    A class tor represent a variable in the stack.

    Compared to Variable holding a Value, RawVariable holds directly
    a Python object representing the variable's value.
    """

    value: object = None


class Struct:
    """
    A wrapper class because we cannot add attributes to object instances.
    This is used only in the GDB tracker.
    """

    def __repr__(self):
        return str(self.__dict__.items())
