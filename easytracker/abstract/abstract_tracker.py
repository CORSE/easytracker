"""Abstract easy tracker.

This module provides an abstract tracker implementing common behavior between
GDB and python trackers.
"""
import typing
from typing import Optional, Union

# Own imports
from easytracker.interface import EasyTrackerItf
from easytracker.types.frame import Frame, RawFrame
from easytracker.types.variable import (
    NameBinding,
    Variable,
    RawVariable,
    Value,
    AbstractType,
)
from easytracker.types.pause_reason import PauseReason, PauseReasonType


# pylint: disable=too-many-instance-attributes
class AbstractEasyTracker(EasyTrackerItf):
    """An abstract tracker.

    Implements some methods of the tracker interface
    in a generic way.
    """

    def __init__(self):
        super().__init__()

        self._line_breakpoints: dict[tuple[int, str], tuple[int, Optional[int]]] = {}
        """Mapping from (lineno, filename), the value is (bp_num, maxdepth)"""

        self._before_function_breakpoints: dict[
            tuple[str, str], tuple[int, Optional[int]]
        ] = {}
        """Mapping from (funcname, filename) to (bp_num, maxdepth)"""

        self._before_endof_function_breakpoints: dict[
            tuple[str, str], tuple[int, Optional[int]]
        ] = {}
        """Mapping from (funcname, filename) to (bp_num, maxdepth)"""

        self._tracked_functions: dict[str, tuple[int, Optional[int]]] = {}
        """Mapping from function name to (bp_num, maxdepth)"""

        self._watchpoints: dict[NameBinding, int] = {}
        """Mapping from name binding to wp_num"""

        self._next_bp_num = 1

        self._running: bool = False

        self._next_source_file: Optional[
            str
        ] = None  # The source file where this tracker is stopped
        self._last_source_file: Optional[
            str
        ] = None  # The source file where this tracker was stopped last time

        self._exit_code: Optional[int] = None

        self._last_lineno: Optional[int] = None
        self._next_lineno: Optional[int] = None

        self._pause_reason: PauseReason = PauseReason(PauseReasonType.UNKNOWN)

    def _add_function_breakpoint(self, breakpoints, funcname, maxdepth):
        breakpoints[
            (
                funcname,
                self.next_source_file if self.next_source_file else "",
            )
        ] = (
            self._next_bp_num,
            maxdepth,
        )
        self._next_bp_num += 1
        return self._next_bp_num - 1

    @staticmethod
    def _delete_breakpoint(
        bps: dict[typing.Any, tuple[int, typing.Any]], bp_id: int
    ) -> Optional[object]:
        """
        Tries to delete the given bp from the given bps dict.
        Returns the bp information if found
        """
        for bp_key, (bp_num, _) in bps.items():
            if bp_num == bp_id:
                break
        else:
            return None

        del bps[bp_key]
        return bp_key

    def _delete_watchpoint(self, wp_id: int):
        """Tries to delete the given watchpoint"""
        for name_binding, other_wp_id in self._watchpoints.items():
            if other_wp_id == wp_id:
                del self._watchpoints[name_binding]
                return

    # ===== property declaration =====
    @property
    def last_source_file(self) -> Optional[str]:
        return self._last_source_file

    @property
    def next_source_file(self) -> Optional[str]:
        return self._next_source_file

    @property
    def exit_code(self) -> Optional[int]:
        return self._exit_code

    @property
    def last_lineno(self) -> Optional[int]:
        if self.exit_code is not None:
            return None
        return self._last_lineno

    @property
    def next_lineno(self) -> Optional[int]:
        if self.exit_code is not None:
            return None
        return self._next_lineno

    @property
    def pause_reason(self) -> PauseReason:
        return self._pause_reason

    # common abstract tracker implementation
    def run(self, in_file: str = None) -> PauseReason:
        self.start(in_file)
        return self.resume()

    def break_before_func(self, funcname: str, maxdepth: Optional[int] = None) -> int:
        return self._add_function_breakpoint(
            self._before_function_breakpoints, funcname, maxdepth
        )

    def break_end_of_func(self, funcname: str, maxdepth: Optional[int] = None) -> int:
        return self._add_function_breakpoint(
            self._before_endof_function_breakpoints, funcname, maxdepth
        )

    def break_before_line(self, lineno: int, maxdepth: Optional[int] = None) -> int:
        self._line_breakpoints[
            (
                lineno,
                self.next_source_file if self.next_source_file else "",
            )
        ] = (
            self._next_bp_num,
            maxdepth,
        )
        self._next_bp_num += 1

        return self._next_bp_num - 1

    def watch(self, variable: Union[NameBinding, str]) -> int:
        if isinstance(variable, str):
            variable = NameBinding(variable)
        self._watchpoints[variable] = self._next_bp_num
        self._next_bp_num += 1

        return self._next_bp_num - 1

    def track_function(self, funcname: str, maxdepth: Optional[int] = None) -> int:
        self._tracked_functions[funcname] = (self._next_bp_num, maxdepth)
        self._next_bp_num += 1

        return self._next_bp_num - 1

    def delete_breakpoint(self, bp_num: int):
        # line bp
        info = AbstractEasyTracker._delete_breakpoint(
            bps=self._line_breakpoints, bp_id=bp_num
        )
        if info is not None:
            return
        # function bp
        info = AbstractEasyTracker._delete_breakpoint(
            bps=self._before_function_breakpoints, bp_id=bp_num
        )
        if info is not None:
            return
        # track function
        info = AbstractEasyTracker._delete_breakpoint(
            bps=self._tracked_functions, bp_id=bp_num
        )
        if info is not None:
            return
        # watchpoint
        self._delete_watchpoint(bp_num)

    @staticmethod
    def _as_raw_frame(frame: Frame, seen: dict[int, typing.Any]) -> RawFrame:
        """Recursively converts the frame and its parents to raw frames"""
        current_frame: Optional[Frame] = frame
        raw_frame = None
        last_raw_frame = None
        while True:
            assert current_frame is not None
            raw_variables = {
                vn: AbstractEasyTracker._as_raw_variable(v, seen)
                for vn, v in frame.variables.items()
            }
            current_raw_frame = RawFrame(
                current_frame.name,
                depth=current_frame.depth,
                next_source_file=current_frame.next_source_file,
                next_lineno=current_frame.next_lineno,
                variables=raw_variables,
                parent=None,
            )
            if last_raw_frame is not None:
                last_raw_frame.parent = current_raw_frame
            last_raw_frame = current_raw_frame
            if raw_frame is None:
                raw_frame = current_raw_frame
            current_frame = current_frame.parent
            if current_frame is None:
                break
        return raw_frame

    # pylint: disable=too-many-return-statements
    @staticmethod
    def _as_raw_variable(
        variable: Variable, seen: dict[int, typing.Any]
    ) -> RawVariable:
        """Converts the variable to a raw variable"""

        @typing.no_type_check
        def _converts_value(value: Value) -> typing.Any:
            prev_value = seen.get(id(value), None)
            if prev_value is not None:
                return prev_value
            if value.abstract_type == AbstractType.PRIMITIVE:
                seen[id(value)] = value.content
                return value.content
            if value.abstract_type == AbstractType.REF:
                res = _converts_value(value.content)
                seen[id(value)] = res
                return res
            if value.abstract_type == AbstractType.LIST:
                res = []
                seen[id(value)] = res
                for child_value in value.content:
                    res.append(_converts_value(child_value))
                return res
            if value.abstract_type == AbstractType.NONE:
                seen[id(value)] = None
                return None
            if value.abstract_type == AbstractType.UNKNOWN:
                seen[id(value)] = value.content
                return value.content
            if value.abstract_type == AbstractType.STRUCT:
                res = {}
                seen[id(value)] = res
                for str_key, child_value in value.content.items():
                    res[str_key] = _converts_value(child_value)
                return res
            if value.abstract_type == AbstractType.DICT:
                res = {}
                seen[id(value)] = res
                for str_value, child_value in value.content.items():
                    res[_converts_value(str_value)] = _converts_value(child_value)
                return res
            raise NotImplementedError(f"_convert_value of type {value.abstract_type}")

        return RawVariable(
            name=variable.name,
            function_name=variable.function_name,
            depth=variable.depth,
            filename=variable.filename,
            value=_converts_value(variable.value),
        )
