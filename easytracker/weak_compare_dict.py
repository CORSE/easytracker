"""Tiny module to hold a weak compare function to avoid cyclic dependencies"""

from easytracker.types.variable import Variable


def weak_compare_dict(actual: dict[str, Variable], ref: dict[str, Variable]) -> bool:
    """Compares two variable dictionaries using Value weak_equality"""
    if actual.keys() != ref.keys():
        return False
    for self_var, ref_var in zip(actual, ref):
        if not actual[self_var].weak_equality(ref[ref_var]):
            return False
    return True
